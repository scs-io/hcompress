/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
 * <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of HCompress
 * 
 * HCompress is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
/*-------------------------------------------------------------------------
*
* Created: h5_buffer_write.cpp
* June 5 2018
* Hariharan Devarajan <hdevarajan@hdfgroup.org>
*
* Purpose:to test the Write to Buffer on DMSH
*
*-------------------------------------------------------------------------
*/

#include <malloc.h>
#include <memory.h>
#include <hdf5.h>
#include <assert.h>
#include <math.h>
#include <hermes_vol.h>
#include <dataset_generator.h>
#include "util.h"
#include "timer.h"

#ifdef __cplusplus
extern "C" {
#endif

int main(int argc, char **argv) {
    MPI_Init(&argc, &argv);
    struct InputArgs args = parse_opts(argc, argv);
    signal(SIGSEGV, handler_c);
    signal(SIGABRT, handler_c);
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    if(args.debug && rank==0){
        printf("ready for attach\n");
        fflush(stdout);
        getchar();
    }
    MPI_Barrier(MPI_COMM_WORLD);

    char file_name[256];
    char *homepath = getenv("RUN_DIR");
    if (args.pfs_path != NULL) {
        sprintf(file_name, "%s/test_%d.h5", args.pfs_path, rank);
    } else {
        sprintf(file_name, "%s/pfs/test_%d.h5", homepath, rank);
    }
    hid_t fapl = H5Pcreate(H5P_FILE_ACCESS);
    //if (args.direct_io_) H5Pset_fapl_direct(fapl, 4 * 1024, 4 * 1024, 4 * 1024);
    int rank_ = 2;
    int64_t type_ = H5T_NATIVE_INT;
    size_t num_elements = 10;
    if (args.io_size_ > 0) {
        double total_number_of_ints = args.io_size_ * MB / (H5Tget_size(type_) * 1.0);
        num_elements = (size_t) ceil(sqrt(total_number_of_ints));
    }

    hsize_t dims[2] = {num_elements, num_elements};
    hsize_t max_dims[2] = {num_elements, num_elements};
    usleep(100000*rank);
    timer_on (0);
    hid_t file_id = H5Fcreate(file_name, H5F_ACC_TRUNC, H5P_DEFAULT, fapl);
    timer_off (0);
    hid_t dataspaceId = H5Screate_simple(rank_, dims, max_dims);
    MPI_Barrier(MPI_COMM_WORLD);
    if(rank==0) printf("num_elements %ld\n",num_elements);
    for(int i=0;i<args.iteration_;i++) {
        usleep(100000*rank);
	char dataset_name[256];
        sprintf(dataset_name,"/dset_%d",i);
        timer_on (0);
        hid_t datasetId = H5Dcreate2(file_id, dataset_name, H5T_NATIVE_INT, dataspaceId, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
        timer_off (0);
        int32_t* array=RandomDistribution1d(args.probability_type, num_elements*num_elements, args.probability_args[0], args.probability_args[1]);
        int32_t* array2=malloc(num_elements*num_elements*sizeof(int));
	MPI_Barrier(MPI_COMM_WORLD);
	timer_on (0);
        timer_on (1);
        if(H5Dwrite(datasetId, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT, array) < 0) {
            printf("Write failed\n");
            exit(0);
        }
        timer_off (1);
        timer_off (0);
	MPI_Barrier(MPI_COMM_WORLD);
	timer_on (0);
        timer_on (2);
        if(H5Dread(datasetId, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT, array2) < 0) {
            printf("Read Failed\n");
        }
        timer_off (2);
        timer_off (0);
	MPI_Barrier(MPI_COMM_WORLD);
        if(strncmp((char*)array, (char*)array2, num_elements*num_elements*sizeof(int)) != 0)
            printf("Failed to restore array from Hermes. Data corrupted.\n");
        free(array);
        free(array2);
	MPI_Barrier(MPI_COMM_WORLD);
        timer_on (0);
        H5Dclose(datasetId);
        timer_off (0);
	MPI_Barrier(MPI_COMM_WORLD);
    }
    MPI_Barrier(MPI_COMM_WORLD);
    timer_on (0);
    H5Sclose(dataspaceId);
    H5Fclose(file_id);
    timer_off (0);
    if(rank==0){
        printf("\nTiming results\n");
        timer_msg (2, "just reading data");
        timer_msg (1, "just writing data");
        timer_msg (0, "opening, writing, closing file");
        printf("\n");
    }
    H5Pclose(fapl);
    MPI_Finalize();
    return 1;
}

#ifdef __cplusplus
}
#endif
