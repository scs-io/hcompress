/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
 * <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of HCompress
 * 
 * HCompress is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
/*-------------------------------------------------------------------------
*
* Created: h5_buffer_read_fpp_mpi.cpp
* June 5 2018
* Hariharan Devarajan <hdevarajan@hdfgroup.org>
*
* Purpose:to test the MPI processes  Read existing data from the Buffers of DMSH
*
*-------------------------------------------------------------------------
*/

#include <malloc.h>
#include <memory.h>
#include <hdf5.h>
#include <assert.h>
#include <math.h>
#include <zconf.h>
#include "../../include/hermes_vol.h"
#include "util.h"

#ifdef __cplusplus
extern "C" {
#endif

int main(int argc, char **argv) {
    struct InputArgs args = parse_opts(argc, argv);
    setup_env(args);
    MPI_Init(&argc, &argv);
    int rank, comm_size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &comm_size);
    char *homepath = getenv("RUN_DIR");
    if (rank == 0) {
        printf("ready for attach %d processes\n", comm_size);
        fflush(stdout);
        getchar();
    }
    MPI_Barrier(MPI_COMM_WORLD);
    char file_name[256];
    if (args.pfs_path != NULL) {
        sprintf(file_name, "%s/test_%d.h5", args.pfs_path, rank);
    } else {
        sprintf(file_name, "%s/pfs/test_%d.h5", homepath, rank);
    }
    const char dataset_name[] = "/dset";
    hid_t vol_fapl = H5Pcreate(H5P_FILE_ACCESS);
    if (args.direct_io_) H5Pset_fapl_direct(vol_fapl, 4 * 1024, 4 * 1024, 4 * 1024);
    hid_t vol_id;
    if (args.layer_count_ == 0) {
        LayerInfo layers[4];
        sprintf(layers[0].mount_point_, "%s/ramfs/", homepath);
        layers[0].capacity_mb_ = 1024;
        layers[0].bandwidth = 80000;
        layers[0].is_memory = true;
        sprintf(layers[1].mount_point_, "%s/nvme/", homepath);
        layers[1].capacity_mb_ = 2 * 1024;
        layers[1].bandwidth = 2000;
        layers[1].is_memory = false;
        sprintf(layers[2].mount_point_, "%s/bb/", homepath);
        layers[2].capacity_mb_ = 4 * 1024;
        layers[2].bandwidth = 400;
        layers[2].is_memory = false;
        sprintf(layers[3].mount_point_, "%s/pfs/", homepath);
        layers[3].capacity_mb_ = 8 * 1024;
        layers[3].bandwidth = 100;
        layers[3].is_memory = false;
        vol_id = H5Pset_fapl_hermes_vol(vol_fapl, layers, 4);
    } else {
        vol_id = H5Pset_fapl_hermes_vol(vol_fapl, args.layers, args.layer_count_);
    }
    /**
     * Write data to the final destination
     */
    hid_t file_id = H5Fcreate(file_name, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
    int rank_ = 2;
    int64_t type_ = H5T_NATIVE_INT;
    size_t num_elements = 1024;
    if (args.io_size_ > 0) {
        double total_number_of_ints = args.io_size_ * MB / (H5Tget_size(type_) * 1.0);
        num_elements = (size_t) ceil(sqrt(total_number_of_ints));
    }
    hsize_t dims[2] = {num_elements, num_elements};
    hsize_t max_dims[2] = {num_elements, num_elements};
    hid_t dataspaceId = H5Screate_simple(rank_, dims, max_dims);
    hid_t datasetId = H5Dcreate2(file_id, dataset_name, H5T_NATIVE_INT, dataspaceId, H5P_DEFAULT, H5P_DEFAULT,H5P_DEFAULT);
    int *array = malloc(num_elements * num_elements * sizeof(int));
    for (int i = 0; i < num_elements; i++) {
        for (int j = 0; j < num_elements; j++) {
            array[i * num_elements + j] = i + j;
        }
    }
    H5Dwrite(datasetId, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT, array);
    free(array);
    H5Sclose(dataspaceId);
    H5Dclose(datasetId);
    H5Fclose(file_id);

    file_id = H5Fopen(file_name, H5F_ACC_RDONLY, vol_fapl);
    datasetId = H5Dopen(file_id, dataset_name, H5P_DEFAULT);
    sleep(5);
    int *array2 = malloc(num_elements * num_elements * sizeof(int));
    for (int i = 0; i < num_elements; i++) {
        for (int j = 0; j < num_elements; j++) {
            array2[i * num_elements + j] = i + j;
        }
    }
    H5Dread(datasetId, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT, array2);
    sleep(5);
    H5Dread(datasetId, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT, array2);
    free(array2);
    H5Dclose(datasetId);
    H5Fclose(file_id);
    H5Pclose(vol_fapl);
    clean_env(args);
    if (rank == 0) {
        printf("SUCCESS\n");
    }
    MPI_Finalize();
    return 1;
}

#ifdef __cplusplus
}
#endif
