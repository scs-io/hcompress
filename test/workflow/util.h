/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
 * <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of HCompress
 * 
 * HCompress is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
/*-------------------------------------------------------------------------
 *
 * Created: util.h
 * June 11 2018
 * Hariharan Devarajan <hdevarajan@hdfgroup.org>
 *
 * Purpose:This contains common utility functions for the test cases
 *
 *
 * INPUTS:
 * -l [layer_count]
 * -l (i)_capacity_mb
 * -l (i)_bandwidth
 * -l (i)_is_memory
 * -l (i)_mount_point
 * -l (i)_io_size
 * -i [io_size_]
 * -n [iteration_]
 * -f [pfs_path]
 * -d [direct_io]
*-------------------------------------------------------------------------
*/

#ifndef HERMES_PROJECT_UTIL_H
#define HERMES_PROJECT_UTIL_H

#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>

const uint32_t MB=1024*1024;

int run_command(char* cmd){
    int ret;
    ret=system(cmd);
    return ret>>8;
}

typedef struct InputArgs{
    size_t io_size_;
    char* pfs_path;
    int layer_count_;
    LayerInfo* layers;
    size_t iteration_;
    size_t direct_io_;

} InputArgs;

static char** str_split(char* a_str, const char a_delim)
{
    char** result    = 0;
    size_t count     = 0;
    char* tmp        = a_str;
    char* last_comma = 0;
    char delim[2];
    delim[0] = a_delim;
    delim[1] = 0;

    /* Count how many elements will be extracted. */
    while (*tmp)
    {
        if (a_delim == *tmp)
        {
            count++;
            last_comma = tmp;
        }
        tmp++;
    }

    /* Add space for trailing token. */
    count += last_comma < (a_str + strlen(a_str) - 1);

    /* Add space for terminating null string so caller
       knows where the list of returned strings ends. */
    count++;

    result = (char**)malloc(sizeof(char*) * count);

    if (result)
    {
        size_t idx  = 0;
        char* token = strtok(a_str, delim);

        while (token)
        {
            assert(idx < count);
            *(result + idx++) = strdup(token);
            token = strtok(0, delim);
        }
        assert(idx == count - 1);
        *(result + idx) = 0;
    }

    return result;
}


static struct InputArgs parse_opts(int argc, char *argv[]){
    int flags, opt;
    int nsecs, tfnd;

    nsecs = 0;
    tfnd = 0;
    flags = 0;
    struct InputArgs args;
    args.io_size_=0;
    args.layer_count_=0;
    args.pfs_path=NULL;
    args.iteration_=1;
    args.direct_io_=true;
    while ((opt = getopt (argc, argv, "l:i:f:n:d:")) != -1)
    {
        switch (opt)
        {
            case 'l':{
                char** layers=str_split(optarg,'#');
                int layer_count=atoi(layers[0]);
                LayerInfo* layerInfos=(LayerInfo*)malloc(sizeof(LayerInfo)*layer_count);
                int i;
		        for(i=0;i<layer_count;++i){
                    char** layerInfoStr=str_split(layers[i+1],'_');
                    layerInfos[i].capacity_mb_= (float) atof(layerInfoStr[0]);
                    layerInfos[i].bandwidth= (float) atof(layerInfoStr[1]);
                    layerInfos[i].is_memory= (bool) atoi(layerInfoStr[2]);
                    strcpy(layerInfos[i].mount_point_, layerInfoStr[3]);
                    layerInfos[i].direct_io= (bool) atoi(layerInfoStr[4]);
                }
                args.layer_count_=layer_count;
                args.layers=layerInfos;
                break;
            }
            case 'i':{
                args.io_size_= (size_t) atoi(optarg);
                break;
            }
            case 'n':{
                args.iteration_= (size_t) atoi(optarg);
                break;
            }
            case 'f':{
                args.pfs_path= optarg;
                break;
            }
            case 'd':{
                args.direct_io_= atoi(optarg);
                break;
            }
            default:               /* '?' */
                fprintf (stderr, "Usage: %s [-l layer_count;l(i)_capacity_mb-l(i)_bandwidth-l(i)_is_memory-l(i)_mount_point] [-i io_size_per_request]  [-f pfs_path] [-d direct io true/false] [-n request repetition]\n", argv[0]);
                exit (EXIT_FAILURE);
        }
    }
    return args;
}

static void setup_env(struct InputArgs args){
    if(args.layer_count_==0){
        char* homepath = getenv("RUN_DIR");
        ssize_t len = snprintf(NULL, 0,"mkdir -p %s/pfs %s/nvme %s/bb %s/ramfs", homepath,homepath,homepath,homepath);
        char* command=(char*)malloc(len+1);
        snprintf(command,len+1, "mkdir -p %s/pfs %s/nvme %s/bb %s/ramfs", homepath,homepath,homepath,homepath);
        run_command(command);
        ssize_t len2 = snprintf(NULL, 0, "rm -rf %s/pfs/* %s/nvme/* %s/bb/* %s/ramfs/*", homepath,homepath,homepath,homepath);
        char* command2=(char*)malloc(len2+1);
        snprintf(command2,len2+1, "rm -rf %s/pfs/* %s/nvme/* %s/bb/* %s/ramfs/*", homepath,homepath,homepath,homepath);
        run_command(command2);
    }else{
        int i;
	for(i=0;i<args.layer_count_;++i){
            char cmd1[256];
            sprintf(cmd1,"rm -rf %s*",args.layers[i].mount_point_);
            run_command(cmd1);
            char cmd2[256];
            sprintf(cmd2,"mkdir -p %s",args.layers[i].mount_point_);
            run_command(cmd2);
        }
    }
}

static void clean_env(struct InputArgs args){
    if(args.layer_count_==0){
        char* homepath = getenv("RUN_DIR");
        ssize_t len2 = snprintf(NULL, 0, "rm -rf %s/pfs/* %s/nvme/* %s/bb/* %s/ramfs/*", homepath,homepath,homepath,homepath);
        char* command2=(char*)malloc(len2+1);
        snprintf(command2,len2+1, "rm -rf %s/pfs/* %s/nvme/* %s/bb/* %s/ramfs/*", homepath,homepath,homepath,homepath);
        run_command(command2);
    }else{
        int i;
	for(i=0;i<args.layer_count_;++i){
            char cmd[256];
            sprintf(cmd,"rm -rf %s*",args.layers[i].mount_point_);
            run_command(cmd);
        }
    }
}
#endif //HERMES_PROJECT_UTIL_H
