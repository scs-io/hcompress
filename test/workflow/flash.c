/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
 * <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of HCompress
 * 
 * HCompress is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
/*-------------------------------------------------------------------------
*
* Created: h5_buffer_mwrite_fpp_mpi.cpp
* June 5 2018
* Hariharan Devarajan <hdevarajan@hdfgroup.org>
*
* Purpose:to test the MPI Processes perform Multiple writes in the Buffers of DMSH
*
*-------------------------------------------------------------------------
*/



#include <malloc.h>
#include <memory.h>
#include <mpi.h>
#include <hdf5.h>
#include <assert.h>
#include <math.h>
#include "../../include/hermes_vol.h"
#include "util.h"

#ifdef __cplusplus
extern "C" {
#endif

int main(int argc, char** argv) {
    struct InputArgs args=parse_opts(argc,argv);
  setup_env(args);
    MPI_Init(&argc,&argv);
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    char* homepath = getenv("RUN_DIR");
    char file_name[256];
    if(args.pfs_path!=NULL){
        sprintf(file_name,"%s/test.h5",args.pfs_path);
    }else{
        sprintf(file_name,"%s/test.h5", homepath);
    }
    char p1[256],p2[256],p3[256],p4[256],p5[256],p6[256],p7[256],p8[256];
    sprintf(p1,"/p1_%d",rank);
    sprintf(p2,"/p2_%d",rank);
    sprintf(p3,"/p3_%d",rank);
    sprintf(p4,"/p4_%d",rank);
    sprintf(p5,"/p5_%d",rank);
    sprintf(p6,"/p6_%d",rank);
    sprintf(p7,"/p7_%d",rank);
    sprintf(p8,"/p8_%d",rank);
    hid_t vol_fapl = H5Pcreate(H5P_FILE_ACCESS);
    MPI_Info info ;
    MPI_Info_create(&info) ;
    if(args.direct_io_) {
        MPI_Info_set(info, "direct_read", "true");
        MPI_Info_set(info, "direct_write", "true");
    }
    H5Pset_fapl_mpio(vol_fapl, MPI_COMM_WORLD, info);
    H5Pset_all_coll_metadata_ops(vol_fapl, true);
    H5Pset_coll_metadata_write(vol_fapl, true);
    hid_t vol_id;
    if(args.layer_count_ == 0){
        LayerInfo layers[4];
        sprintf(layers[0].mount_point_,"%s/ramfs/",homepath);
        layers[0].capacity_mb_=1024;
        layers[0].bandwidth=8000;
        layers[0].is_memory=false;
        sprintf(layers[1].mount_point_,"%s/nvme/",homepath);
        layers[1].capacity_mb_=2*1024;
        layers[1].bandwidth=2000;
        layers[1].is_memory=false;
        sprintf(layers[2].mount_point_,"%s/bb/",homepath);
        layers[2].capacity_mb_=4*1024;
        layers[2].bandwidth=400;
        layers[2].is_memory=false;
        sprintf(layers[3].mount_point_,"%s/pfs/",homepath);
        layers[3].capacity_mb_=8*1024;
        layers[3].bandwidth=100;
        layers[3].is_memory=false;
        vol_id=H5Pset_fapl_hermes_vol(vol_fapl,layers,4);
    }else{
        vol_id=H5Pset_fapl_hermes_vol(vol_fapl,args.layers,args.layer_count_);
    }
    hid_t file_id = H5Fcreate(file_name, H5F_ACC_TRUNC, H5P_DEFAULT, vol_fapl);
    int rank_ = 1;
    int64_t type_1 = H5T_NATIVE_CHAR;
    int64_t type_2 = H5T_NATIVE_CHAR;
    size_t num_elements=10;
    if(args.io_size_>0){
        double total_number_of_ints=args.io_size_*MB/(H5Tget_size(type_1)*1.0);
        num_elements= (size_t) ceil(total_number_of_ints/7);
    }
    hsize_t dims[1]={num_elements};
    hsize_t max_dims[1]={num_elements};
    hid_t dataspaceId = H5Screate_simple(rank_, dims, max_dims);

    char* x1=malloc(num_elements*sizeof(char));
    char* x2=malloc(num_elements*sizeof(char));
    char* x3=malloc(num_elements*sizeof(char));
    char* x4=malloc(num_elements*sizeof(char));
    char* x5=malloc(num_elements*sizeof(char));
    char* x6=malloc(num_elements*sizeof(char));
    char* y1=malloc(num_elements*sizeof(char));
    char* y2=malloc(num_elements*sizeof(char));
    int i;
    for(i=0;i<num_elements;i++) {
        x1[i]=rand();
        x2[i]=rand();
        x3[i]=rand();
        x4[i]=rand();
        x5[i]=rand();
        x6[i]=rand();
        y1[i]=rand();
        y2[i]=rand();
    }
    hid_t p1_id = H5Dcreate2(file_id, p1, type_1, dataspaceId, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    H5Dwrite(p1_id,type_1 , H5S_ALL, H5S_ALL, H5P_DEFAULT, x1);
    hid_t p2_id = H5Dcreate2(file_id, p2, type_1, dataspaceId, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    H5Dwrite(p2_id,type_1 , H5S_ALL, H5S_ALL, H5P_DEFAULT, x2);
    hid_t p3_id = H5Dcreate2(file_id, p3, type_1, dataspaceId, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    H5Dwrite(p3_id,type_1 , H5S_ALL, H5S_ALL, H5P_DEFAULT, x3);
    hid_t p4_id = H5Dcreate2(file_id, p4, type_1, dataspaceId, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    H5Dwrite(p4_id,type_1 , H5S_ALL, H5S_ALL, H5P_DEFAULT, x4);
    hid_t p5_id = H5Dcreate2(file_id, p5, type_1, dataspaceId, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    H5Dwrite(p5_id,type_1 , H5S_ALL, H5S_ALL, H5P_DEFAULT, x5);
    hid_t p6_id = H5Dcreate2(file_id, p6, type_1, dataspaceId, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    H5Dwrite(p6_id,type_1 , H5S_ALL, H5S_ALL, H5P_DEFAULT, x6);
    hid_t p7_id = H5Dcreate2(file_id, p7, type_1, dataspaceId, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    H5Dwrite(p7_id,type_2 , H5S_ALL, H5S_ALL, H5P_DEFAULT, y1);
    hid_t p8_id = H5Dcreate2(file_id, p8, type_1, dataspaceId, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    H5Dwrite(p8_id,type_2 , H5S_ALL, H5S_ALL, H5P_DEFAULT, y2);
    free(x1);
    free(x2);
    free(x3);
    free(x4);
    free(x5);
    free(x6);
    free(y1);
    free(y2);
    H5Sclose(dataspaceId);
    H5Dclose(p1_id);
    H5Dclose(p2_id);
    H5Dclose(p3_id);
    H5Dclose(p4_id);
    H5Dclose(p5_id);
    H5Dclose(p6_id);
    H5Dclose(p7_id);
    H5Dclose(p8_id);
    H5Fclose(file_id);
    H5Pclose(vol_fapl);
 clean_env(args);
    printf("SUCCESS\n");
    MPI_Finalize();
    return 1;
}

#ifdef __cplusplus
}
#endif
