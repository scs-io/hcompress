/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
 * <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of HCompress
 * 
 * HCompress is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
/*-------------------------------------------------------------------------
*
* Created: h5_buffer_mwrite_fpp_mpi.cpp
* June 5 2018
* Hariharan Devarajan <hdevarajan@hdfgroup.org>
*
* Purpose:to test the MPI Processes perform Multiple writes in the Buffers of DMSH
*
*-------------------------------------------------------------------------
*/



#include <malloc.h>
#include <memory.h>
#include <mpi.h>
#include <hdf5.h>
#include <assert.h>
#include <math.h>
#include "../../include/hermes_vol.h"
#include "util.h"

#ifdef __cplusplus
extern "C" {
#endif

int main(int argc, char** argv) {
    struct InputArgs args=parse_opts(argc,argv);
  setup_env(args);
    MPI_Init(&argc,&argv);
    int rank,comm_size;
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    MPI_Comm_size(MPI_COMM_WORLD,&comm_size);
    if(rank==0){
        printf("ready for attach %d processes\n", comm_size);
        fflush(stdout);
        getchar();
    }
    MPI_Barrier(MPI_COMM_WORLD);
    char file_name[256];

    char* homepath = getenv("RUN_DIR");
    if(args.pfs_path!=NULL){
        sprintf(file_name,"%s/test.h5",args.pfs_path);
    }else{
        sprintf(file_name,"%s/test.h5",homepath);
    }
    char dataset_name[256];
    sprintf(dataset_name,"/dset_%d",rank);
    hid_t vol_fapl = H5Pcreate(H5P_FILE_ACCESS);
    MPI_Info info ;
    MPI_Info_create(&info) ;
    if(args.direct_io_) {
        MPI_Info_set(info, "direct_read", "true");
        MPI_Info_set(info, "direct_write", "true");
    }
    H5Pset_fapl_mpio(vol_fapl, MPI_COMM_WORLD, info);
    H5Pset_all_coll_metadata_ops(vol_fapl, true);
    H5Pset_coll_metadata_write(vol_fapl, true);
    hid_t vol_id;
    if(args.layer_count_ == 0){
        LayerInfo layers[4];
        sprintf(layers[0].mount_point_,"%s/ramfs/",homepath);
        layers[0].capacity_mb_=1024;
        layers[0].bandwidth=8000;
        layers[0].is_memory=true;
        sprintf(layers[1].mount_point_,"%s/nvme/",homepath);
        layers[1].capacity_mb_=2*1024;
        layers[1].bandwidth=2000;
        layers[1].is_memory=false;
        sprintf(layers[2].mount_point_,"%s/bb/",homepath);
        layers[2].capacity_mb_=4*1024;
        layers[2].bandwidth=400;
        layers[2].is_memory=false;
        sprintf(layers[3].mount_point_,"%s/pfs/",homepath);
        layers[3].capacity_mb_=8*1024;
        layers[3].bandwidth=100;
        layers[3].is_memory=false;
        vol_id=H5Pset_fapl_hermes_vol(vol_fapl,layers,4);
    }else{
        vol_id=H5Pset_fapl_hermes_vol(vol_fapl,args.layers,args.layer_count_);
    }
    hid_t file_id = H5Fcreate(file_name, H5F_ACC_TRUNC, H5P_DEFAULT, vol_fapl);
    int rank_ = 2;
    int64_t type_ = H5T_NATIVE_INT;
    size_t num_elements=10;
    if(args.io_size_>0){
        double total_number_of_ints=args.io_size_*MB/(H5Tget_size(type_)*1.0);
        num_elements= (size_t) ceil(sqrt(total_number_of_ints));
    }
    hsize_t dims[2]={num_elements,num_elements};
    hsize_t max_dims[2]={num_elements,num_elements};
    hid_t dataspaceId = H5Screate_simple(rank_, dims, max_dims);
    hid_t datasetId = H5Dcreate2(file_id, dataset_name, H5T_NATIVE_INT, dataspaceId, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    int* write_data=malloc(num_elements*num_elements*sizeof(int));
    int* read_data=malloc(num_elements*num_elements*sizeof(int));
    for(int i=0;i<num_elements;i++) {
        for (int j = 0; j < num_elements; j++) {
            write_data[i*num_elements+j]=i+j;
            read_data[i*num_elements+j]=-1;
        }
    }
    size_t fifth_element= (size_t) floor(num_elements / 5.0);
    hsize_t start[2]={fifth_element,fifth_element};
    hsize_t count[2]={fifth_element,fifth_element};
    hid_t dataspace = H5Dget_space (datasetId);
    H5Sselect_hyperslab (dataspace, H5S_SELECT_SET, start, NULL,count, NULL);
    hsize_t mem_dims[2]={num_elements,num_elements};
    hsize_t mem_max_dims[2]={num_elements,num_elements};
    hid_t memspace = H5Screate_simple (rank_, mem_dims, mem_max_dims);
    hsize_t mem_start[2]={fifth_element,fifth_element};
    hsize_t mem_count[2]={fifth_element,fifth_element};
    H5Sselect_hyperslab (memspace, H5S_SELECT_SET, mem_start, NULL, mem_count, NULL);
    H5Dwrite (datasetId,H5T_NATIVE_INT, memspace, dataspace, H5P_DEFAULT, write_data);

    hsize_t start_2[2]={2*fifth_element,2*fifth_element};
    hsize_t count_2[2]={fifth_element,fifth_element};
    hid_t dataspace_2 = H5Dget_space (datasetId);
    H5Sselect_hyperslab (dataspace_2, H5S_SELECT_SET, start_2, NULL,count_2, NULL);
    hsize_t mem_dims_2[2]={num_elements,num_elements};
    hsize_t mem_max_dims_2[2]={num_elements,num_elements};
    hid_t memspace_2 = H5Screate_simple (rank_, mem_dims_2, mem_max_dims_2);
    hsize_t mem_start_2[2]={2*fifth_element,2*fifth_element};
    hsize_t mem_count_2[2]={fifth_element,fifth_element};
    H5Sselect_hyperslab (memspace_2, H5S_SELECT_SET, mem_start_2, NULL, mem_count_2, NULL);
    H5Dwrite (datasetId,H5T_NATIVE_INT, memspace_2, dataspace_2, H5P_DEFAULT, write_data);

    hsize_t start_3[2]={3*fifth_element,3*fifth_element};
    hsize_t count_3[2]={fifth_element,fifth_element};
    hid_t dataspace_3 = H5Dget_space (datasetId);
    H5Sselect_hyperslab (dataspace_3, H5S_SELECT_SET, start_3, NULL,count_3, NULL);
    hsize_t mem_dims_3[2]={num_elements,num_elements};
    hsize_t mem_max_dims_3[2]={num_elements,num_elements};
    hid_t memspace_3 = H5Screate_simple (rank_, mem_dims_3, mem_max_dims_3);
    hsize_t mem_start_3[2]={3*fifth_element,3*fifth_element};
    hsize_t mem_count_3[2]={fifth_element,fifth_element};
    H5Sselect_hyperslab (memspace_3, H5S_SELECT_SET, mem_start_3, NULL, mem_count_3, NULL);
    H5Dwrite (datasetId,H5T_NATIVE_INT, memspace_3, dataspace_3, H5P_DEFAULT, write_data);
    H5Dread(datasetId,H5T_NATIVE_INT , H5S_ALL, H5S_ALL, H5P_DEFAULT, read_data);
    free(read_data);
    free(write_data);
    H5Sclose(dataspaceId);
    H5Sclose(memspace);
    H5Sclose(dataspace);
    H5Sclose(memspace_2);
    H5Sclose(dataspace_2);
    H5Sclose(memspace_3);
    H5Sclose(dataspace_3);
    H5Dclose(datasetId);
    H5Fclose(file_id);
    H5Pclose(vol_fapl);
 clean_env(args);
    printf("SUCCESS\n");
    MPI_Finalize();
    return 1;
}

#ifdef __cplusplus
}
#endif
