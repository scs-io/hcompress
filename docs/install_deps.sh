# Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
# Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
# <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
#
# This file is part of HCompress
# 
# HCompress is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

#####DISCLAIMER####
#RUN set_environment.sh first!!!!!
#Make sure to modify the necessary variables
#This script is NOT ENTIRELY AUTOMATIC!!!
#This file assumes the dependencies are located in a directory that contains a directory named "source". 

######SET DEFAULT VARIABLES#####

BLD=$HCOMPRESS_DEP_BLD
SRC=$HCOMPRESS_DEP_SRC

CMAKE_COMP_NAME=cmake-3.15.2.tar.gz
CMAKE_UNCOMP_NAME=cmake-3.15.2

MPICH_COMP_NAME=mpich-3.3.1.tar.gz
MPICH_UNCOMP_NAME=mpich-3.3.1

BOOST_COMP_NAME=boost_1_70_0.tar.gz
BOOST_UNCOMP_NAME=boost_1_70_0

RAPIDJSON_COMP_NAME=rapidjson-master.zip
RAPIDJSON_UNCOMP_NAME=rapidjson

DLIB_COMP_NAME=dlib-19.17.tar.bz2
DLIB_UNCOMP_NAME=dlib-19.17

########CMAKE#########
cd $SRC
wget https://github.com/Kitware/CMake/releases/download/v3.15.2/cmake-3.15.2.tar.gz
tar -xvzf $CMAKE_COMP_NAME -C $SRC
cd $CMAKE_UNCOMP_NAME
./bootstrap --prefix=$BLD
make && make install

########RPCLIB#########
cd $SRC
git clone https://github.com/rpclib/rpclib.git
cd rpclib
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=$BLD ../ -DBUILD_SHARED_LIBS=1
make -j3 && make install

########MPICH##########

cd $SRC
wget http://www.mpich.org/static/downloads/3.3.1/mpich-3.3.1.tar.gz
tar -xvzf $MPICH_COMP_NAME -C $SRC
cd $MPICH_UNCOMP_NAME
./configure --prefix=$BLD --enable-fast=03 --enable-shared --enable-romio --enable-threads --disable-fortran --disable-fc
make && make install

########HDF5##########

cd $SRC
git clone -b develop --single-branch https://bitbucket.hdfgroup.org/scm/hdffv/hdf5.git 
cd hdf5 
nano hl/src/H5LT.c 
	#line 891: hid_t vol_id = H5VLget_connector_id(H5VL_NATIVE_NAME); H5Pset_vol(fapl, vol_id, NULL); 
mkdir build 
cd build
cmake -DCMAKE_INSTALL_PREFIX=$BLD -DHDF5_ENABLE_DIRECT_VFD=ON -DHDF5_BUILD_CPP_LIB=OFF -DHDF5_ENABLE_PARALLEL=ON ../
make -j8 && make install

########H5HUT##########

cd $SRC
git clone -b 1.6 --single-branch https://gitlab.psi.ch/H5hut/src.git H5hut 
cd H5hut 
./autogen.sh 
./configure --prefix=$BLD --with-hdf5=$BLD --with-mpi=$BLD --enable-shared --enable-parallel
nano src/H5Part.c 
	#line 2138: else if ( H5Oget_info ( obj_id, &objinfo,0 ) < 0 ) {
	#line 2088: if( H5Oget_info_by_name( group_id, member_name, &objinfo,0, H5P_DEFAULT ) < 0 ) { 
	#line 261: MPI_Info_create(&info) ; MPI_Info_set(info, "direct_read", "true"); MPI_Info_set(info, "direct_write", "true");
	#line 249: if (H5Pset_fapl_mpio ( f->access_prop, comm, 0 ) < 0) {
nano test/testframe.c 
	#line 670: H5Oget_info(list[i], &info,0);
make && make install

########BOOST##########
cd $SRC
wget https://dl.bintray.com/boostorg/release/1.70.0/source/boost_1_70_0.tar.gz
tar -xvzf $BOOST_COMP_NAME -C $SRC
cd $BOOST_UNCOMP_NAME
./bootstrap.sh --prefix=$BLD
./b2 install

#######RAPIDJSON#######
cd $SRC
git clone https://github.com/Tencent/rapidjson.git
cd $RAPIDJSON_UNCOMP_NAME
cmake -DCMAKE_INSTALL_PREFIX=$BLD .
make && make install

########DLIB##########
cd $SRC
wget http://dlib.net/files/dlib-19.17.tar.bz2
tar -xvjf $DLIB_COMP_NAME -C $SRC
cd $DLIB_UNCOMP_NAME
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=$BLD ../ -DBUILD_SHARED_LIBS=1 -DCMAKE_INSTALL_PREFIX=$BLD
make && make install

########OpenBLAS##########
cd $SRC
git clone https://github.com/xianyi/OpenBLAS
cd OpenBLAS
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=$BLD ../
make -j3 && make install

