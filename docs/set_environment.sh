# Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
# Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
# <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
#
# This file is part of HCompress
# 
# HCompress is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

######YOU MAY WANT TO CHANGE THE FOLLOWING VARIABLES#######
#RESARCH_DIR: The directory where hcompress and its external dependencies are located
#HCOMPRESS_SRC: The directory containing the hcompress github
#HCOMPRESS_BLD: The directory where you build the hcompress github with cmake/make
#HCOMPRESS_DEP_SRC: The directory where external dependencies are extracted
#HCOMPRESS_DEP_BLD: The directory where external dependencies are stored
#INSTALL_DIR: The directory where hcompress libraries are installed using make install
#RUN_DIR: The directory where test cases will buffer files by default


#######INTENDED USAGE########
#cd /path/to/RESEARCH_DIR
#git clone hcompress
#./set_environment.sh
#./install_libs.sh
#./install_deps.sh

########SET PROJECT DIRECTORY#########

#RESEARCH_DIR="/media/lukemartinlogan/Mirror1/Documents/School/S19/Ares/Programming"
#RESEARCH_DIR="/media/lukemartinlogan/Mirror2/Documents/School/S19/Ares/Programming"
RESEARCH_DIR="/home/llogan"
echo "HCOMPRESS_BLD=$RESEARCH_DIR/hcompress/build" >> ~/.bashrc
echo "HCOMPRESS_SRC=$RESEARCH_DIR/hcompress" >> ~/.bashrc
echo "HCOMPRESS_DEP_BLD=$RESEARCH_DIR/dependencies" >> ~/.bashrc
echo "HCOMPRESS_DEP_SRC=$RESEARCH_DIR/dependencies/source" >> ~/.bashrc
source ~/.bashrc

#########SETUP DEPENDENCIES DIRECTORY

mkdir $HCOMPRESS_DEP_BLD
mkdir $HCOMPRESS_DEP_SRC
mkdir $HCOMPRESS_DEP_SRC/comp_libs

########EXTERNAL DEPENDENCIES##########

echo "export PATH=\$HCOMPRESS_DEP_BLD/bin:\$HCOMPRESS_DEP_BLD/lib:\$HCOMPRESS_DEP_BLD/include:\$PATH" >> ~/.bashrc
echo "export LD_LIBRARY_PATH=\$HCOMPRESS_DEP_BLD/lib:\$LD_LIBRARY_PATH" >> ~/.bashrc
echo "export LIBRARY_PATH=\$HCOMPRESS_DEP_BLD/lib:\$LIBRARY_PATH" >> ~/.bashrc
echo "export INCLUDE=\$HCOMPRESS_DEP_BLD/include:\$INCLUDE" >> ~/.bashrc
echo "export CPATH=\$HCOMPRESS_DEP_BLD/include:\$CPATH" >> ~/.bashrc

#######HCOMPRESS########

#libhermes*.so
echo "export LD_LIBRARY_PATH=\$HCOMPRESS_BLD:\$LD_LIBRARY_PATH" >> ~/.bashrc
echo "export LIBRARY_PATH=\$HCOMPRESS_BLD:\$LIBRARY_PATH" >> ~/.bashrc
#libares.so
echo "export LD_LIBRARY_PATH=\$HCOMPRESS_BLD/ares:\$LD_LIBRARY_PATH" >> ~/.bashrc
echo "export LIBRARY_PATH=\$HCOMPRESS_BLD/ares:\$LIBRARY_PATH" >> ~/.bashrc
#libdatasets.so
echo "export LD_LIBRARY_PATH=\$HCOMPRESS_BLD/ares/datasets:\$LD_LIBRARY_PATH" >> ~/.bashrc
echo "export LIBRARY_PATH=\$HCOMPRESS_BLD/ares/datasets:\$LIBRARY_PATH" >> ~/.bashrc
#libcommon.so
echo "export LD_LIBRARY_PATH=\$HCOMPRESS_BLD/common:\$LD_LIBRARY_PATH" >> ~/.bashrc
echo "export LIBRARY_PATH=\$HCOMPRESS_BLD/common:\$LIBRARY_PATH" >> ~/.bashrc
#HDF5 HERMES VOL
echo "export HDF5_PLUGIN_PATH=\$HCOMPRESS_SRC/build/" >> ~/.bashrc
echo "export HDF5_VOL_CONNECTOR=hermes" >> ~/.bashrc
#make install
echo "export INSTALL_DIR=\$HCOMPRESS_BLD" >> ~/.bashrc
#Used for test cases
echo "export RUN_DIR=\$HOME/test"  >> ~/.bashrc
