/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
 * <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of HCompress
 * 
 * HCompress is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
/*-------------------------------------------------------------------------
*
* Created: hdf5_data_organizer.cpp
* June 5 2018
* Hariharan Devarajan <hdevarajan@hdfgroup.org>
*
* Purpose: It is the definitions of data_organizer on hdf5 file format.
*
*-------------------------------------------------------------------------
*/

#include <iostream>
#include <stdint-gcc.h>
#include <core/metadata_manager_factory.h>
#include <core/hdf5_impl/hdf5_data_organizer.h>
#include <core/hdf5_impl/hdf5_metadata_manager.h>
#include <common/constants.h>
#include <debug.h>
#include <boost/filesystem.hpp>



HDF5Output HDF5DataOrganizer::Move(HDF5Input source, HDF5Input destination) {
    AutoTrace trace = AutoTrace("HDF5DataOrganizer::Move",source,destination);
   /* CharStruct s_buffered_file = source.HDF5Input::layer.layer_loc + FILE_PATH_SEPARATOR + source.filename;
    CharStruct d_buffered_file = destination.HDF5Input::layer.layer_loc + FILE_PATH_SEPARATOR + destination.filename;
    boost::system::error_code ec;
    boost::filesystem::copy_file(s_buffered_file.c_str(),d_buffered_file.c_str(),boost::filesystem::copy_option::overwrite_if_exists,ec);
    if(ec.failed()) {
        printf("couldn't rename file %s\n",ec.message().data());
        printf("couldn't rename source %s dest %s\n",s_buffered_file,d_buffered_file);
    }*/
    /*if(source.HDF5Input::layer.io_client_type==IOClientType::HDF5_FILE && destination.HDF5Input::layer.io_client_type==IOClientType::HDF5_FILE ){
        CharStruct s_buffered_file = source.HDF5Input::layer.layer_loc + FILE_PATH_SEPARATOR + source.filename;
        CharStruct d_buffered_file = destination.HDF5Input::layer.layer_loc + FILE_PATH_SEPARATOR + destination.filename;
        rename(s_buffered_file.c_str(),d_buffered_file.c_str());
    }else{*/
    CharStruct s_buffered_file = source.HDF5Input::layer.layer_loc + FILE_PATH_SEPARATOR + source.filename;
    CharStruct d_buffered_file = destination.HDF5Input::layer.layer_loc + FILE_PATH_SEPARATOR + destination.filename;
    rename(s_buffered_file.c_str(),d_buffered_file.c_str());
    uint64_t size_of_io=source.GetSize();
    source.buffer=malloc(size_of_io);
    DBGMSG("size of io : "<< size_of_io);
    HDF5Output read_output=Singleton<HDF5IOFactory>::GetInstance()->GetClient(source.layer.io_client_type)->Read(source,source);
    if(!read_output.CheckSuccess()) return read_output;
    HDF5Output write_output=Singleton<HDF5IOFactory>::GetInstance()->GetClient(destination.layer.io_client_type)->Write(source,destination);
    if(source.buffer) free(source.buffer);
    if(!write_output.CheckSuccess()) return write_output;
    /*}*/
    Singleton<HDF5IOFactory>::GetInstance()->GetClient(source.layer.io_client_type)->Delete(source);
    HDF5Output output;
    return output;
}

bool HDF5DataOrganizer::HasCapacity(HDF5Input source, Layer layer) {
    AutoTrace trace = AutoTrace("HDF5DataOrganizer::HasCapacity",source,layer);
    /* calculate remaining capacity and calculate if it has enough capacity. */
    auto current_capacity=Singleton<HDF5IOFactory>::GetInstance()->GetClient(layer.io_client_type)->GetCurrentCapacity(layer);
    float remaining_capacity=layer.capacity_mb_ * MB - current_capacity;
    return remaining_capacity >= source.GetSize();
}

HDF5Output HDF5DataOrganizer::MakeCapacity(HDF5Input source, Layer layer) {
    AutoTrace trace = AutoTrace("HDF5DataOrganizer::MakeCapacity",source,layer);
    /* if cant fit return error*/
    if(!CanFit(source,layer))
        return HDF5Output(-1);
    HDF5Output output;
    if(layer.next == nullptr) return output;
    /* if layer doesnt have capacity move data to next layer to make capacity*/
    if(!HasCapacity(source,*layer.next)){
        MakeCapacity(source, *layer.next);
    }
    /* calculate remaining capacity. */
    auto current_capacity=Singleton<HDF5IOFactory>::GetInstance()->GetClient(layer.io_client_type)->GetCurrentCapacity(layer);
    float remaining_capacity=layer.capacity_mb_ * MB - current_capacity;
    /* get data to evict from MDM*/
    auto datas_to_evict=Singleton<MetadataManagerFactory<HDF5Input>>::GetInstance()->GetMDM(MDM_TYPE)->GetBufferedDataToEvict(source,layer,remaining_capacity);
    for(auto data_source:datas_to_evict){
        HDF5WriteInput destination(data_source);
        destination.HDF5Input::layer=*layer.next;
        /* move data to next layer*/
        HDF5Output output = Move(data_source,destination);
        if(output.CheckSuccess()){
            Singleton<MetadataManagerFactory<HDF5Input>>::GetInstance()->GetMDM(MDM_TYPE)->UpdateLayer(destination,*layer.next);
        }else return output;
    }
    return output;
}

bool HDF5DataOrganizer::CanFit(HDF5Input source, Layer layer) {
    AutoTrace trace = AutoTrace("HDF5DataOrganizer::CanFit",source,layer);
    return layer.capacity_mb_*MB>=source.GetSize();
}



HDF5Output HDF5DataOrganizer::BufferInit(HDF5Input source) {
    AutoTrace trace = AutoTrace("HDF5DataOrganizer::BufferInit",source);
    HDF5Output output;
    return output;
}

HDF5Output HDF5DataOrganizer::BufferFlush(HDF5Input source) {
    AutoTrace trace = AutoTrace("HDF5DataOrganizer::BufferFlush",source);
    HDF5Output output;
    /* Get all Buffered Data from MDM*/
    vector<pair<HDF5Input,HDF5Input>> buffered_data = Singleton<MetadataManagerFactory<HDF5Input>>::GetInstance()->GetMDM(MDM_TYPE)->GetAllBufferedData(source);
    /* move all buffered data to PFS*/
    for(std::pair<HDF5Input,HDF5Input> pair:buffered_data){
        Move(pair.first,pair.second);
    }
    /* delete buffered data from buffering layers. */
    for(std::pair<HDF5Input,HDF5Input> pair:buffered_data){
        HDF5Input source_buf=pair.first;
        if(source_buf.layer!=*Layer::LAST){
            Singleton<HDF5IOFactory>::GetInstance()->GetClient(source_buf.layer.io_client_type)->Delete(source_buf);
        }
    }
    /* Update metadata on sync. */
    Singleton<MetadataManagerFactory<HDF5Input>>::GetInstance()->GetMDM(MDM_TYPE)->UpdateOnSync(source);
    return output;
}
