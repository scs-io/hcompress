/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
 * <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of HCompress
 * 
 * HCompress is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
//
// Created by HariharanDevarajan on 2/7/2019.
//

#include <src/core/interfaces/prefetcher/trigger_manager.h>
#include <core/hdf5_impl/prefetcher/hdf5_event_builder.h>
vector<HDF5Event> HDF5EventBuilder::build(HDF5Input input, EventType type,uint64_t sequenceId=0, bool is_end=false){
    vector<HDF5Event> events=vector<HDF5Event>();
    HDF5Event event;
    event.processId=rank;
    event.sequenceId= sequenceId == 0 ? globalSequence.GetNextSequenceServer(0):sequenceId;
//    event.time_stamp= Singleton<basket::global_clock>::GetInstance()->GetTime();
    event.input=input;
    event.type=type;
    event.is_end=is_end;
    events.push_back(event);
    return events;
}
