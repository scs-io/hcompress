/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
 * <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of HCompress
 * 
 * HCompress is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
//
// Created by HariharanDevarajan on 2/15/2019.
//

#include <core/hdf5_impl/prefetcher/hdf5_history_based_prediction_engine.h>

void HDF5HistoryBasedPredictionEngine::enhance(vector <HDF5Event> events) {
    // Will get start and end versions of events

    for (auto event : events) {
        // Find deltas on current events

        // Get at internal deltas
        auto file_set = pattern[event.processId];
        std::hash<std::string> hash_str;
        std::string val = std::string(event.input.filename.c_str())+std::string(FILE_PATH_SEPARATOR.c_str())+std::string(event.input.dataset_name.c_str());
            auto file_set_iter = file_set.find(hash_str(val));
        if( file_set_iter != file_set.end()){
            //found file
            auto deltas = file_set_iter->second;
            // OPEN event, do nothing because deltas are already initialized
            // READ event, start and end
            // - start: check whether current position of delta matches read event
            // - end: verify time for each event to finish
            // - Once current delta fixed, propagate change in knowledge to other deltas
            // times need to be shifted for failed timespan

            // deltas
            // ------
            // if the offsets are correct (i.e. u started from zero) but size was wrong fix the sequential access for the new size
            // if offset was wrong but size is correct -> (convert deltas from sequential to strided)
            // if offset was wrong and size was also wrong -> guess strided pattern with new offset and size
            // if this happens too much eventually u will see the pattern based on history

            // TODO We need a constant for prediction lookahead
            // Initially will be assumed, we'll test for different numbers in different test cases
        }else{
            //no found
            // Add file
            // Initial prediction
            // - See new file, prefetch sequentially (fixed size) from start to end with timespan delays
            // - Put current_pointers to first part of pattern
        }

        // Store those deltas
    }
}

void HDF5HistoryBasedPredictionEngine::store() {
    // Invalidate predictions that weren't actually seen.
}

void HDF5HistoryBasedPredictionEngine::load() {
}

vector<pair<t_mili,HDF5Event>> HDF5HistoryBasedPredictionEngine::getNextTriggers() {
    // EventType will be READ_DATASET
    return vector<pair<t_mili,HDF5Event>>();
}
