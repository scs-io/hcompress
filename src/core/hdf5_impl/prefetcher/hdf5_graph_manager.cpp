/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
 * <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of HCompress
 * 
 * HCompress is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
//
// Created by HariharanDevarajan on 2/15/2019.
//
#include <core/hdf5_impl/prefetcher/hdf5_graph_manager.h>

RootGraphNode HDF5GraphManager::load() {
    FILE *fh = std::fopen(graph_file.c_str(), "r");
    if (fh != NULL) {
        std::fseek(fh, 0L, SEEK_END);
        long size_to_read = std::ftell(fh);
        std::rewind(fh);
        msgpk::unpacker m_pac;
        m_pac.reserve_buffer(size_to_read);
        size_t count = std::fread(m_pac.buffer(), sizeof(char), size_to_read, fh);
        if (count != size_to_read) {
            //TODO:didnt read
        }
        std::fclose(fh);
        m_pac.buffer_consumed(count);
        msgpk::object_handle oh;
        if (m_pac.next(oh)) {
            msgpk::object obj_node = oh.get();
            tuple<RootGraphNode, vector<HDF5GraphNode>> tuple_nodes;
            obj_node.convert(tuple_nodes);
            root_node = std::get<0>(tuple_nodes);
            vector<HDF5GraphNode> nodes = std::get<1>(tuple_nodes);
            for (auto node:nodes)
                this->nodes.insert(std::pair<size_t, std::shared_ptr<HDF5GraphNode>>(node.sequenceId,
                                                                                     std::make_shared<HDF5GraphNode>(
                                                                                             node)));
        }
        root_node.num_nodes = nodes.size();
        is_initialized = false;
    }
    return root_node;
}

vector<std::shared_ptr<HDF5GraphNode>> HDF5GraphManager::enhance(vector<HDF5Event> events) {
    if (this->nodes.size() == 0) {
        for(int i=0;i<HERMES_CONF->RANKS_PER_SERVER;++i){
            root_node.links.push_back(0);
        }
    }
    //Test while graph is loaded.
    for (auto event : events) {
        auto iter = nodes.find(event.sequenceId);
        std::shared_ptr<HDF5GraphNode> node;
        if (iter != nodes.end()) {
            //found it
            if (event.is_end) iter->second->end_time = event.time_stamp;
            else iter->second->start_time = event.time_stamp;
            iter->second->input = event.input;
            iter->second->type = event.type;
            node = iter->second;
        } else {
            //oops not found
            node = std::make_shared<HDF5GraphNode>();
            node->sequenceId = event.sequenceId;
            node->processId = event.processId;
            node->start_time = event.time_stamp;
            node->input = event.input;
            node->type = event.type;
            node->links = std::vector<size_t>();
            nodes.insert(std::pair<size_t, std::shared_ptr<HDF5GraphNode>>(event.sequenceId, node));
            auto current_node_index_iter = current_nodes_index.begin();
            while(current_node_index_iter != current_nodes_index.end()){
                if (current_node_index_iter->first == node->processId && current_node_index_iter->second < node->sequenceId) {
                    auto current_node = nodes.find(current_node_index_iter->second)->second;
                    current_node->links.push_back(node->sequenceId);
                    current_node_index_iter = current_nodes_index.erase(current_node_index_iter);
                }else current_node_index_iter++;
            }
            if (root_node.links[node->processId%HERMES_CONF->RANKS_PER_SERVER]==0) {
                root_node.links[node->processId%HERMES_CONF->RANKS_PER_SERVER]=node->sequenceId;
            }
        }
        current_nodes_index.erase(node->processId);
        current_nodes_index.insert(std::pair<size_t,size_t>(node->processId,node->sequenceId));
    }
    root_node.num_nodes = this->nodes.size();
    vector<std::shared_ptr<HDF5GraphNode>> current_nodes=vector<std::shared_ptr<HDF5GraphNode>>();
    for(auto current_node_index:current_nodes_index){
        auto current_node = nodes.find(current_node_index.second)->second;
        current_nodes.push_back(current_node);
    }
    return current_nodes;
}

void HDF5GraphManager::store() {
    vector<HDF5GraphNode> vec_nodes = vector<HDF5GraphNode>();
    for (auto node:nodes) {
        vec_nodes.push_back(*node.second.get());
    }
    tuple<RootGraphNode, vector<HDF5GraphNode>> tuple_nodes(root_node, vec_nodes);
    msgpk::v1::sbuffer buffer;
    msgpk::v1::pack(buffer, tuple_nodes);
    FILE *fh = std::fopen(graph_file.c_str(), "w+");
    size_t count = std::fwrite(buffer.data(), sizeof(char), buffer.size(), fh);
    if (count != buffer.size()) {
        //TODO:didnt write
    }
    std::fclose(fh);
}

RootGraphNode HDF5GraphManager::fetch() {
    return root_node;
}

vector<pair<t_mili, HDF5Event>> HDF5GraphManager::getNextTriggers() {
    vector<pair<t_mili, HDF5Event>> triggers = vector<pair<t_mili, HDF5Event>>();
    for (auto current_node_index:current_nodes_index) {
        auto current_node = nodes.find(current_node_index.second)->second;
        for (auto link : current_node->links) {
            vector<size_t> io_links = findReadLink(link);
            for (auto io_link:io_links) {
                auto iter = scheduled_events.find(io_link);
                if (iter == scheduled_events.end()) {
                    auto node_iter = nodes.find(io_link);
                    if (node_iter != nodes.end()) {
                        auto node = node_iter->second;
                        HDF5Event event;
                        event.sequenceId = node->sequenceId;
                        event.processId = node->processId;
                        event.time_stamp = node->start_time;
                        event.input = node->input;
                        event.type = node->type;
                        t_mili t = node->start_time - (node->end_time - node->start_time);
                        t -= PREFETCHING_ALGO_DELAY_US;
                        //printf("delay read event by %d\n", t);
                        triggers.push_back(std::pair<t_mili, HDF5Event>(t, event));
                        scheduled_events.insert(node->sequenceId);
                    }
                }
            }
        }
    }
    return triggers;
}

vector<size_t> HDF5GraphManager::findReadLink(size_t link) {
    vector<size_t> return_link = vector<size_t>();
    auto node_iter = nodes.find(link);
    if (node_iter != nodes.end()) {
        bool found=false;
        if (node_iter->second->type == EventType::READ_DATASET){
            auto iter = scheduled_events.find(node_iter->second->sequenceId);
            if(iter == scheduled_events.end()){
                return_link.push_back(node_iter->second->sequenceId);
                found=true;
            }

        }
        if(!found){
            for (auto newlink:node_iter->second->links) {
                auto new_return_link = findReadLink(newlink);
                return_link.insert(return_link.end(), new_return_link.begin(), new_return_link.end());
            }
        }
    }
    return return_link;
}

