/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
 * <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of HCompress
 * 
 * HCompress is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Copyright by The HDF Group.                                               *
 * Copyright by the Board of Trustees of the University of Illinois.         *
 * All rights reserved.                                                      *
 *                                                                           *
 * This file is part of HDF5. The full HDF5 copyright notice, including      *
 * terms governing use, modification, and redistribution, is contained in    *
 * the files COPYING and Copyright.html. COPYING can be found at the root    *
 * of the source code distribution tree; Copyright.html can be found at the  *
 * root level of an installed copy of the electronic HDF5 document set and   *
 * is linked from the top-level documents page. It can also be found at      *
 * http://hdfgroup.org/HDF5/doc/Copyright.html. If you do not have           *
 * access to either file, you may request a copy from help@hdfgroup.org.     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/*-------------------------------------------------------------------------
*
* Created: hdf5_max_performance_dpe.cpp
* June 5 2018
* Hariharan Devarajan <hdevarajan@hdfgroup.org>
*
* Purpose: It is the definitions of data_placement_engine on hdf5 file format.
*
*-------------------------------------------------------------------------
*/

#include <cmath>
#include <core/metadata_manager_factory.h>
#ifdef ENABLE_COMPRESSION
#include <ares_filter/configuration_manager.h>
#include <ares_filter/compression_metrics_manager.h>
#endif
#include <core/hdf5_impl/hdf5_max_performance_dpe.h>
#include <common/constants.h>
#include <common/configuration_manager.h>


std::vector<Placement<HDF5Input,HDF5Output>>
HDF5MaxPerformanceDataPlacementEngine::PlaceReadData(HDF5ReadInput input) {
    AutoTrace trace = AutoTrace("HDF5MaxPerformanceDataPlacementEngine::PlaceReadData",input);
    /* Get buffered data from MDM*/
    std::vector<std::pair<HDF5Input,HDF5Input>> buffered_data = Singleton<MetadataManagerFactory<HDF5Input>>::GetInstance()->GetMDM(MDM_TYPE)->FindBufferedData(input);
    /* Construct and return placements */
    std::vector<Placement<HDF5Input,HDF5Output>> placements=std::vector<Placement<HDF5Input,HDF5Output>>();
    if(buffered_data.size()!=0){
        for(std::pair<HDF5Input,HDF5Input> pair:buffered_data){
            Placement<HDF5Input, HDF5Output> placement;
            placement.move_required_=false;
            placement.output=HDF5Output();
            placement.source_info=pair.first;
            placement.destination_info=pair.second;
            placements.push_back(placement);
        }
    }
    return placements;
}

std::vector<Placement<HDF5Input,HDF5Output>>
HDF5MaxPerformanceDataPlacementEngine::PlaceWriteData(HDF5WriteInput input) {
    AutoTrace trace = AutoTrace("HDF5MaxPerformanceDataPlacementEngine::PlaceWriteData",input);
    Matrix bufferDim;
    bufferDim.GetDefault(input.rank_);
    input.dimentions_=input.GetCount();
    input.max_dimentions_=input.GetCount();
    for(int i=0;i<input.rank_;i++){
        bufferDim.start_[i]=input.memory_start_[i];
        bufferDim.end_[i]=input.memory_dim_[i];
    }
    /* Place data optimally into first layer */
    std::vector<Placement<HDF5Input,HDF5Output>> placements=CalculatePlacementOptimally(input,*Layer::FIRST,bufferDim);
    return placements;
}


std::vector<Placement<HDF5Input, HDF5Output>>
HDF5MaxPerformanceDataPlacementEngine::CalculatePlacementOptimally(HDF5Input input,Layer layer,
                                                                   Matrix &buffer_dims)
{
    AutoTrace trace = AutoTrace("HDF5MaxPerformanceDataPlacementEngine::CalculatePlacementOptimally",input,layer,buffer_dims);

    //if layer has capacity to fit data or is the last layer.
    if(HDF5DataOrganizer::GetInstance()->HasCapacity(input,layer) || layer.next == nullptr)
    {
        auto optimal_compression=FindOptimalCompression(input,layer);
        float place_score=optimal_compression.first;
        std::vector<Placement<HDF5Input, HDF5Output>> placements = std::vector<Placement<HDF5Input, HDF5Output>>();
        Placement<HDF5Input, HDF5Output> placement=BuildPlacement(input,layer,false,optimal_compression.second,true,buffer_dims);
        placement.score_ = place_score;
        placements.push_back(placement);
        return placements;
    }

    //if layer doesnt have enough capacity but can fit data if data is moved.
    else if(HDF5DataOrganizer::GetInstance()->CanFit(input,layer)){
        auto original_buffer=buffer_dims;
        //Score for making space + placing
        auto optimal_compression=FindOptimalCompression(input,layer);
        float movement_cost = CalculateMovingScore(input,layer,*layer.next);
        float move_and_place_score= movement_cost + optimal_compression.first;

        //Score for skipping this layer
        auto placments_sub = CalculatePlacementOptimally(input,*layer.next,buffer_dims);
        float next_layer_score=0;
        std::vector<Placement<HDF5Input,HDF5Output>> placements=std::vector<Placement<HDF5Input,HDF5Output>>();
        for(auto placement:placments_sub){
            placements.push_back(placement);
            next_layer_score+=placement.score_;
        }
        if(next_layer_score < move_and_place_score)
            return placements;
        placements.clear();
        auto placement=BuildPlacement(input,layer,movement_cost!=0,optimal_compression.second,true,original_buffer);
        placement.score_=move_and_place_score;
        placements.push_back(placement);
        return placements;
    }

    //if complete data cannot be fit in layer
    else{

        //split input into smaller piece that can fit in layer and then fit all other pieces in other layer.
        std::vector<Placement<HDF5Input,HDF5Output>> placements_1=std::vector<Placement<HDF5Input,HDF5Output>>();
        std::vector<Placement<HDF5Input,HDF5Output>> placements_2=std::vector<Placement<HDF5Input,HDF5Output>>();
        std::vector<HDF5Input> inputs=SplitInput(input,layer.capacity_mb_);

        //Score for splitting data and sending rest to next layer
        auto optimal_compression=FindOptimalCompression(inputs[0],layer);
        float movement_cost=CalculateMovingScore(inputs[0],layer,*layer.next);
        float move_and_place_score= movement_cost + optimal_compression.first;
        auto placement=BuildPlacement(inputs[0],layer,movement_cost!=0,optimal_compression.second,true,buffer_dims);
        placement.score_=move_and_place_score;
        placements_1.push_back(placement);
        auto placments_sub=std::vector<Placement<HDF5Input,HDF5Output>>();
        for(int i=1;i<inputs.size();i++){
            auto temp_placement=CalculatePlacementOptimally(inputs[i],*layer.next,buffer_dims);
            placments_sub.insert(placments_sub.end(),temp_placement.begin(),temp_placement.end());
        }
        for(auto placement:placments_sub){
            placements_1.push_back(placement);
            move_and_place_score+=placement.score_;
        }

        //Score for skipping this layer
        float next_layer_score=0;
        placments_sub=CalculatePlacementOptimally(input,*layer.next,buffer_dims);
        for(auto placement:placments_sub){
            placements_2.push_back(placement);
            next_layer_score+=placement.score_;
        }

        //if moving is more costly than placing in that layer then place data in next layer.
        if(move_and_place_score < next_layer_score)
            return placements_1;
        else
            return placements_2;
    }
}


std::pair<float,CompressionLibrary>
HDF5MaxPerformanceDataPlacementEngine::FindOptimalCompression(HDF5Input input, Layer layer){
    AutoTrace trace = AutoTrace("HDF5MaxPerformanceDataPlacementEngine::FindOptimalCompression",input,layer);
    CompressionLibrary comp_lib=CompressionLibrary::DUMMY;
#ifndef ENABLE_COMPRESSION
    return make_pair<float,CompressionLibrary>(CalculatePlacementScore(input,layer,CompressionLibrary::DUMMY),CompressionLibrary::DUMMY);
#else
    if(ARES_FILTER_CONF->compressionLibrary != CompressionLibrary::DYNAMIC){
        return make_pair(CalculatePlacementScore(input,layer,ARES_FILTER_CONF->compressionLibrary),ARES_FILTER_CONF->compressionLibrary);
    }else{
        int best_lib = 0;
        float min_time = std::numeric_limits<float>::infinity();
        for(int comp_lib_id = 0; comp_lib_id < NUM_ARES_LIBRARIES; comp_lib_id++) {
            //Selection
            auto time = CalculatePlacementScore(input,layer,static_cast<CompressionLibrary>(comp_lib_id));
            if(time < min_time)
            {
                min_time = time;
                best_lib = comp_lib_id;
            }
        }
        //std::cout << "HDF5MaxPerformanceDataPlacementEngine::FindOptimalCompression-{BEST_LIB: " << static_cast<int>(best_lib) << "}" << std::endl;
        return make_pair(min_time,static_cast<CompressionLibrary>(best_lib));
    }
#endif
}

float HDF5MaxPerformanceDataPlacementEngine::CalculatePlacementScore(HDF5Input input, Layer layer, CompressionLibrary comp_lib)
{
    AutoTrace trace = AutoTrace("HDF5MaxPerformanceDataPlacementEngine::CalculatePlacementScore",input,layer);
    //Get file and layer information

    float S = input.GetSize();
    float Bi = layer.bandwidth_mbps_;

#ifndef ENABLE_COMPRESSION
    return S/Bi;
#else
    auto cm_manager = Singleton<CompressionMetricsManager>::GetInstance();
    //Get constants
    float c1, c2, c3;
    int comp_lib_id = static_cast<int>(comp_lib);
    int type_id = cm_manager->GetDatasetType(input.type_);
    cm_manager->GetCompressionLibraryFit(type_id, comp_lib_id, c1, c2, c3);
    //Estimates
    float tc = c1*S;
    float r = c2;
    float td = c3*S;
    //Score
    float time = ARES_FILTER_CONF->metric_weight[0]*tc + S/Bi - ARES_FILTER_CONF->metric_weight[1]*(S/Bi)*((r-1)/r) + ARES_FILTER_CONF->metric_weight[2]*td;
    //std::cout << "HDF5MaxPerformanceDataPlacementEngine::CalculatePlacementScore-{" << tc << "}-{" << r << "}-{" << td << "}-{" << S << "}-{" << time << "}\n";
    return time;
#endif
}

float
HDF5MaxPerformanceDataPlacementEngine::CalculateMovingScore(HDF5Input input, Layer source_layer,
                                                                     Layer destination_layer) {
    AutoTrace trace = AutoTrace("HDF5MaxPerformanceDataPlacementEngine::CalculateMovingScore",input,source_layer,destination_layer);
    if(HDF5DataOrganizer::GetInstance()->HasCapacity(input,source_layer))
        return 0;
    uint64_t size_of_input=input.GetSize();
    float size_of_input_mb=size_of_input/MB;
    /*
     * time = time to read from source layer + time to write to destination layer.
     */
    float time=size_of_input_mb/source_layer.bandwidth_mbps_ + size_of_input_mb/destination_layer.bandwidth_mbps_;
    return time;
}

Placement<HDF5Input, HDF5Output>
HDF5MaxPerformanceDataPlacementEngine::BuildPlacement(HDF5Input input,
                                                      Layer layer,
                                                      bool move_required,
                                                      CompressionLibrary comp_lib,
                                                      bool generate_name,
                                                      Matrix &buffer_dims) {
    AutoTrace trace = AutoTrace("HDF5MaxPerformanceDataPlacementEngine::BuildPlacement",input,layer,move_required,generate_name,buffer_dims);
    Placement<HDF5Input, HDF5Output> placement;
    placement.move_required_=move_required;
    placement.output=HDF5Output();
    placement.destination_info=input;
    placement.user_info=input;
    placement.source_info=input;
    if(layer==*Layer::LAST){
        for(int rank=0;rank<input.rank_;rank++){
            placement.source_info.file_start_[rank] = input.file_start_[rank]-buffer_dims.start_[rank];
            placement.source_info.file_end_[rank] = placement.source_info.file_start_[rank] + placement.destination_info.GetCount()[rank]-1;
        }
    }else{
        if(generate_name)
            placement.destination_info.filename=bip::string(Singleton<MetadataManagerFactory<HDF5Input>>::GetInstance()->GetMDM(MDM_TYPE)->GenerateBufferFilename().c_str());

        placement.source_info = placement.destination_info;
        for(int rank=0;rank<input.rank_;rank++){
            placement.destination_info.file_end_[rank] = placement.destination_info.file_end_[rank]-placement.destination_info.file_start_[rank];
            placement.destination_info.file_start_[rank]=0;
            placement.destination_info.dimentions_[rank]=placement.destination_info.file_end_[rank]-placement.destination_info.file_start_[rank]+1;
            placement.destination_info.max_dimentions_[rank]=placement.destination_info.file_end_[rank]-placement.destination_info.file_start_[rank]+1;
            placement.source_info.file_start_[rank] = buffer_dims.start_[rank];
            placement.source_info.file_end_[rank] = placement.source_info.file_start_[rank] + placement.destination_info.GetCount()[rank]-1;
            buffer_dims.start_[rank] = placement.source_info.file_end_[rank]+1;
            placement.source_info.dimentions_[rank]=buffer_dims.end_[rank] > placement.source_info.file_end_[rank]?buffer_dims.end_[rank]:placement.source_info.file_end_[rank]+1;
            placement.source_info.max_dimentions_[rank]=placement.source_info.dimentions_[rank]+1;
        }
        placement.destination_info.layer=layer;
    }
#ifdef ENABLE_COMPRESSION
    placement.source_info.comp_lib = comp_lib;
#endif
    return placement;
}

std::vector<HDF5Input>
HDF5MaxPerformanceDataPlacementEngine::SplitInput(HDF5Input input, uint64_t capacity_) {
    AutoTrace trace = AutoTrace("HDF5MaxPerformanceDataPlacementEngine::SplitInput",input,capacity_);
    HDF5WriteInput input_1(input);
    uint64_t total_elements=1;
    for(int i=0;i<input.rank_;i++){
        total_elements*=input.file_end_[i]-input.file_start_[i];
    }
    uint64_t total_size=input.GetSize();
    /* calculate number of elements per dim allowed. */
    uint32_t num_elements_to_keep_per_dim = static_cast<uint32_t>(floor(pow(capacity_ * MB / H5Tget_size(input.type_),1./input.rank_)));

    std::vector<HDF5Input> inputs=std::vector<HDF5Input>();

    std::vector<HDF5WriteInput> pieces=std::vector<HDF5WriteInput>();
    Matrix left_matrix=Matrix(input.rank_,input.file_start_,input.file_end_);
    std::vector<HDF5Input> others=std::vector<HDF5Input>();
    /* split orginal input into smaller chunks. */
    for(int i=0;i<input.rank_;i++){
        if(num_elements_to_keep_per_dim < input_1.file_end_[i]-input_1.file_start_[i]+1){
            input_1.file_end_[i] = input_1.file_start_[i] + num_elements_to_keep_per_dim - 1;
            if(input_1.file_end_[i]<input.file_end_[i]){
                HDF5WriteInput input_2(input);
                input_2.file_start_[i]=input_1.file_end_[i]+1;
                left_matrix.end_[i]=input_2.file_start_[i]-1;
                others.push_back(input_2);
            }
        }
    }
    inputs.push_back(input_1);
    inputs.insert(inputs.end(),others.begin(),others.end());
    return inputs;
}
