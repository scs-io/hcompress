/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
 * <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of HCompress
 * 
 * HCompress is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
/*-------------------------------------------------------------------------
*
* Created: hdf5_metdata_manager.cpp
* June 5 2018
* Hariharan Devarajan <hdevarajan@hdfgroup.org>
*
* Purpose: It is the definition of metadata manager on hdf5 file format.
*
*-------------------------------------------------------------------------
*/

#include <common/city.h>
#include <core/hdf5_impl/hdf5_metadata_manager_opt.h>
#include <common/constants.h>
#include <debug.h>

int HDF5MetadataManagerOpt::UpdateOnRead(HDF5Input source, HDF5Input destination) {
    AutoTrace trace = AutoTrace("HDF5MetadataManagerOpt::UpdateOnRead", source, destination);
    return 0;
}

int HDF5MetadataManagerOpt::UpdateOnWrite(HDF5Input source, HDF5Input destination) {
    AutoTrace trace = AutoTrace("HDF5MetadataManagerOpt::UpdateOnWrite", source, destination);

    CharStruct complete_path = source.filename + FILE_PATH_SEPARATOR + source.dataset_name;
    auto file_iterator = valid_buffered_dataset.Get(complete_path);
    if (!file_iterator.first) {
        /*New file and dataset*/
        UpdateOnInit(source);

    }
    file_iterator = valid_buffered_dataset.Get(complete_path);
    {
        /*Existing file and dataset*/
        auto meta_iter = file_meta_map.Get(complete_path);
        HDF5Input file_meta = meta_iter.second;
        std::shared_ptr<basket::map<Matrix,HDF5Input,MapCompare>> offsets = offsetMaps[file_iterator.second];
        Matrix current_matrix = Matrix(source.rank_, source.file_start_, source.file_end_);
        auto matched_keys = offsets->Contains(current_matrix);
        if (matched_keys.size() > 0) {
            std::vector<std::pair<Matrix, HDF5Input>> left_offsets = std::vector<std::pair<Matrix, HDF5Input>>();
            for (auto matched_key : matched_keys) {
                bip::vector<Matrix> left_overs = matched_key.first.Substract(current_matrix);
                for (Matrix left_over : left_overs) {
                    HDF5Input left_over_file = matched_key.second;
                    /*for (int rank = 0; rank < matched_key.second.rank_; rank++) {
                        left_over_file.file_start_[rank] = left_over.start_[rank] - matched_key.first.start_[rank];
                        left_over_file.file_end_[rank] = left_over.end_[rank] - matched_key.first.start_[rank];
                    }*/
                    left_offsets.push_back(make_pair(left_over, left_over_file));
                }
                CharStruct buffered_file_name =
                        matched_key.second.filename + FILE_PATH_SEPARATOR + matched_key.second.dataset_name;
                bool is_pfs_file = matched_key.second.layer == *Layer::LAST;
                if (left_overs.size() > 0) {
                    Singleton<CacheManager<HDF5Input, Matrix>>::GetInstance()->Delete(matched_key.first);
                    auto iter = buffer_file_actual_map.Get(buffered_file_name);
                    if (!iter.first && !is_pfs_file)
                        buffer_file_actual_map.Erase(buffered_file_name);
                    offsets->Erase(matched_key.first);
                }
            }
            for (std::pair<Matrix, HDF5Input> pair:left_offsets) {
                if (pair.second.layer != *Layer::LAST)
                    buffer_file_actual_map.Put(pair.second.filename + FILE_PATH_SEPARATOR + pair.second.dataset_name,
                                               complete_path);
                auto result = offsets->Put(pair.first, pair.second);
                Singleton<CacheManager<HDF5Input, Matrix>>::GetInstance()->Insert(pair.first, pair.second);
                if (!result) {
                    DBGMSG("Unsuccessful insertion");
                }
            }
        }
        /* insert actual data. */
        buffer_file_actual_map.Put(destination.filename + FILE_PATH_SEPARATOR + destination.dataset_name,
                                   complete_path);
        auto result = offsets->Put(current_matrix, destination);
        Singleton<CacheManager<HDF5Input, Matrix>>::GetInstance()->Insert(current_matrix, destination);
        if (!result) {
            DBGMSG("Unsuccessful insertion");
        }
    }
    return 0;
}

std::vector<std::pair<HDF5Input, HDF5Input>> HDF5MetadataManagerOpt::FindBufferedData(HDF5Input source) {
    AutoTrace trace = AutoTrace("HDF5MetadataManagerOpt::FindBufferedData", source);
    std::vector<std::pair<HDF5Input, HDF5Input>> buffered_data = std::vector<std::pair<HDF5Input, HDF5Input>>();
    CharStruct complete_path = source.filename + FILE_PATH_SEPARATOR + source.dataset_name;
    auto file_iterator = valid_buffered_dataset.Get(complete_path);
    if (file_iterator.first) {
        auto meta_iter = file_meta_map.Get(complete_path);
        std::shared_ptr<basket::map<Matrix,HDF5Input,MapCompare>> offsets = offsetMaps[file_iterator.second];
        Matrix current_matrix = Matrix(source.rank_, source.file_start_, source.file_end_);
        auto matched_keys = offsets->Contains(current_matrix);
        for (auto matched_key : matched_keys) {
            Matrix intersection = matched_key.first.Intersect(current_matrix);
            HDF5Input destination_file = matched_key.second;
            destination_file.buffer = source.buffer;
            HDF5Input source_file = matched_key.second;
            destination_file.dimentions_ = source.dimentions_;
            destination_file.max_dimentions_ = source.max_dimentions_;
            for (int rank = 0; rank < matched_key.second.rank_; rank++) {
                destination_file.file_start_[rank] = intersection.start_[rank] - current_matrix.start_[rank];
                destination_file.file_end_[rank] = intersection.end_[rank] - current_matrix.start_[rank];
                if (matched_key.first.original_start_[rank] > 0) {
                    source_file.file_start_[rank] = intersection.start_[rank] - matched_key.first.original_start_[rank];
                    source_file.file_end_[rank] = intersection.end_[rank] - matched_key.first.original_start_[rank];
                } else {
                    source_file.file_start_[rank] = intersection.start_[rank] - matched_key.first.start_[rank];
                    source_file.file_end_[rank] = intersection.end_[rank] - matched_key.first.start_[rank];
                }

            }
            buffered_data.push_back(std::make_pair(source_file, destination_file));
        }
    }
    return buffered_data;
}

vector<HDF5Input> HDF5MetadataManagerOpt::GetBufferedDataToEvict(HDF5Input source, Layer layer, float remaining_capacity) {
    AutoTrace trace = AutoTrace("HDF5MetadataManagerOpt::GetBufferedDataToEvict", source, layer, remaining_capacity);
    HDF5Input temp = source;
    temp.layer = layer;
    /* Based on required size to evict start evicting data from cacheline of layer. */
    int size_to_evict = source.GetSize() - remaining_capacity;
    vector<HDF5Input> evicted_data = vector<HDF5Input>();
    while (size_to_evict > 0) {
        std::pair<Matrix, HDF5Input> data = Singleton<CacheManager<HDF5Input, Matrix>>::GetInstance()->Evict(layer);
        if (data.first.num_dimension > 0) {
            size_to_evict -= data.second.GetSize();
            evicted_data.push_back(data.second);
        } else break;
    }
    return evicted_data;
}

string HDF5MetadataManagerOpt::GenerateBufferFilename() {
    AutoTrace trace = AutoTrace("HDF5MetadataManagerOpt::GenerateBufferFilename", "void");
    /* use timestamp to generate unique file names. */
    struct timeval tp;
    gettimeofday(&tp, NULL);
    long int us = tp.tv_sec * 1000000 + tp.tv_usec;
    return std::to_string(us)+"_"+ std::to_string(HERMES_CONF->rank) + ".h5";
}


int HDF5MetadataManagerOpt::UpdateOnInit(HDF5Input source) {
    AutoTrace trace = AutoTrace("HDF5MetadataManagerOpt::UpdateOnInit", source);
    /* On init create a record of file and dataset and put first entry into the map of whole dataset being in PFS. */
    CharStruct complete_path = source.filename + FILE_PATH_SEPARATOR + source.dataset_name;
    auto iter = valid_buffered_dataset.Get(complete_path);
    uint64_t sequence;
    if(iter.first) sequence = iter.second;
    else sequence = file_seq.GetNextSequenceServer(0)% MAX_NUM_FILES;
    std::shared_ptr<basket::map<Matrix,HDF5Input,MapCompare>> offsets = offsetMaps[sequence];
    HDF5Input destination = source;
    destination.file_start_ = std::array<hsize_t,MAX_DIMS>();
    for (int i = 0; i < source.rank_; i++) {
        destination.file_start_[i]=0;
        destination.file_end_[i]=source.dimentions_[i] - 1;
    }
    destination.layer = *Layer::LAST;
    offsets->Put(Matrix(destination.rank_, destination.file_start_, destination.file_end_), destination);
    valid_buffered_dataset.Put(complete_path, sequence);
    file_meta_map.Put(complete_path, source);
    return 0;
}

int HDF5MetadataManagerOpt::UpdateOnSync(HDF5Input source) {
    AutoTrace trace = AutoTrace("HDF5MetadataManagerOpt::UpdateOnSync", source);
    /* On successful Sync. remove all buffered metadata and show all data in PFS. */
    CharStruct complete_path = source.filename + FILE_PATH_SEPARATOR + source.dataset_name;
    auto file_iterator = valid_buffered_dataset.Get(complete_path);
    if (file_iterator.first) {
        auto iter = file_meta_map.Get(complete_path);
        std::shared_ptr<basket::map<Matrix,HDF5Input,MapCompare>> offsets = offsetMaps[file_iterator.second];
        HDF5Input destination = iter.second;
        destination.file_start_ = std::array<hsize_t,MAX_DIMS>();
        destination.file_end_ = std::array<hsize_t,MAX_DIMS>();
        for (int i = 0; i < source.rank_; i++) {
            destination.file_start_[i]=0;
            destination.file_end_[i]=destination.dimentions_[i];
        }
        destination.layer = *Layer::LAST;
        offsets->Put(Matrix(destination.rank_, destination.file_start_, destination.file_end_), destination);
    }
    return 0;
}

vector<pair<HDF5Input, HDF5Input>> HDF5MetadataManagerOpt::GetAllBufferedData(HDF5Input source) {
    AutoTrace trace = AutoTrace("HDF5MetadataManagerOpt::GetAllBufferedData", source);
    /* iterate through all pieces of buffered data and return*/
    std::vector<std::pair<HDF5Input, HDF5Input>> buffered_data = std::vector<std::pair<HDF5Input, HDF5Input>>();
    CharStruct complete_path = source.filename + FILE_PATH_SEPARATOR + source.dataset_name;
    auto file_iterator = valid_buffered_dataset.Get(complete_path);
    if (file_iterator.first) {
        auto meta_iter = file_meta_map.Get(complete_path);
        std::shared_ptr<basket::map<Matrix,HDF5Input,MapCompare>> offsets = offsetMaps[file_iterator.second];
        auto mapped_data = offsets->GetAllData();
        for (auto data : mapped_data) {
            HDF5Input destination_file = meta_iter.second;
            HDF5Input source_file = data.second;
            source_file.operation_type = OperationType::READ;
            destination_file.operation_type = OperationType::WRITE;
            for (int i = 0; i < destination_file.rank_; i++) {
                destination_file.memory_dim_[i]=(0);
                destination_file.memory_start_[i]=(0);
            }
            if (source_file.layer != *Layer::LAST) {
                destination_file.file_end_ = data.first.end_;
                destination_file.file_start_ = data.first.start_;
                destination_file.layer = *Layer::LAST;
                destination_file.dimentions_ = meta_iter.second.dimentions_;
                destination_file.max_dimentions_ = meta_iter.second.max_dimentions_;
                buffered_data.emplace_back(source_file, destination_file);
            }
        }
    }
    return buffered_data;
}

int HDF5MetadataManagerOpt::UpdateLayer(HDF5Input buf_source, Layer layer) {
    AutoTrace trace = AutoTrace("HDF5MetadataManagerOpt::UpdateLayer", buf_source, layer);
    /* search for the data which was earlier present and update its layer info. */
    CharStruct complete_path = buf_source.filename + FILE_PATH_SEPARATOR + buf_source.dataset_name;
    auto actual_file_path = buffer_file_actual_map.Get(CharStruct(complete_path)).second;
    auto file_iterator = valid_buffered_dataset.Get(CharStruct(complete_path));
    if (!file_iterator.first) {
        file_iterator = valid_buffered_dataset.Get(actual_file_path);
    }
    if (file_iterator.first) {
        std::shared_ptr<basket::map<Matrix,HDF5Input,MapCompare>> offsets = offsetMaps[file_iterator.second];
        Matrix current_matrix = Matrix(buf_source.rank_, buf_source.file_start_, buf_source.file_end_);
        auto matched_keys = offsets->Contains(current_matrix);
        for(auto matched_key:matched_keys){
            if(matched_key.second.filename == buf_source.filename){
                matched_key.second.layer=layer;
                offsets->Put(matched_key.first, matched_key.second);
                Singleton<CacheManager<HDF5Input, Matrix>>::GetInstance()->Insert(matched_key.first, matched_key.second);
            }
        }
    }
    return 0;
}

bool HDF5MetadataManagerOpt::HasChunk(HDF5Input buf_source, Layer layer) {
    AutoTrace trace = AutoTrace("HDF5MetadataManagerOpt::HasChunk", buf_source, layer);
    CharStruct complete_path = buf_source.filename + FILE_PATH_SEPARATOR + buf_source.dataset_name;
    auto actual_file_path = buffer_file_actual_map.Get(CharStruct(complete_path)).second;
    auto file_iterator = valid_buffered_dataset.Get(CharStruct(complete_path));
    if (file_iterator.first) {
        std::shared_ptr<basket::map<Matrix,HDF5Input,MapCompare>> offsets = offsetMaps[file_iterator.second];
        auto mapped_data = offsets->GetAllData();
        for (auto data : mapped_data) {
            if (data.second.layer == layer) {
                return true;
            }
        }
    }
    return false;
}
