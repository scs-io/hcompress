/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
 * <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of HCompress
 * 
 * HCompress is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
/*-------------------------------------------------------------------------
*
* Created: hdf5_client.cpp
* June 5 2018
* Hariharan Devarajan <hdevarajan@hdfgroup.org>
*
* Purpose: It is the definition of io_client on hdf5 file format.
*
*-------------------------------------------------------------------------
*/

#include <array>
#include <memory>
#include <core/hdf5_impl/hdf5_client.h>


#ifdef ENABLE_COMPRESSION
#include <ares_filter/compression_metrics_manager.h>
#include <ares/c++/ares.h>
#include <ares_filter/hdf5_ares_filter.h>
#endif

#include <common/constants.h>
#include <debug.h>
#include <H5FDdirect.h>

HDF5Output HDF5Client::Read(HDF5Input source, HDF5Input destination) {
    AutoTrace trace = AutoTrace("HDF5Client::Read", source, destination);
    HDF5ReadOutput read_output;
    DBGVAR(source.filename);
    CharStruct buffered_file = source.HDF5Input::layer.layer_loc + FILE_PATH_SEPARATOR + source.filename;
    hid_t file;
    hid_t dataset;
    hid_t dataspace, memspace;
    //Open HDF5 dataset
    if (source.HDF5Input::layer == *Layer::LAST) {
        DBGMSG("Using PFS file");
        dataset_get_wrapper(source.dataset_object, source.vol_id, H5VL_DATASET_GET_SPACE, H5P_DATASET_XFER_DEFAULT, NULL, &dataspace);
        memspace = H5Screate_simple(destination.rank_, destination.dimentions_.data(), destination.max_dimentions_.data());
        read_output.error += H5Sselect_hyperslab(dataspace, H5S_SELECT_SET, source.file_start_.data(), NULL, source.GetCount().data(), NULL);
        read_output.error += H5Sselect_hyperslab(memspace, H5S_SELECT_SET, destination.file_start_.data(), NULL, destination.GetCount().data(), NULL);
        read_output.error += H5VLdataset_read(source.dataset_object, source.vol_id, destination.type_, memspace, dataspace, H5P_DEFAULT, destination.HDF5Input::buffer, NULL);
    } else {
        hid_t fapl = H5Pcreate(H5P_FILE_ACCESS);
        hid_t native_id = H5VLget_connector_id(H5VL_NATIVE_NAME);
        herr_t e = H5Pset_vol(fapl, native_id, NULL);
        file = H5Fopen(buffered_file.c_str(), H5F_ACC_RDWR, fapl);
        dataset = H5Dopen2(file, source.dataset_name.c_str(), H5P_DEFAULT);
        //Read uncompressed dataset into destination
        dataspace = H5Dget_space(dataset);
        memspace = H5Screate_simple(destination.rank_, destination.dimentions_.data(), destination.max_dimentions_.data());
        read_output.error += H5Sselect_hyperslab(dataspace, H5S_SELECT_SET, source.file_start_.data(), NULL, source.GetCount().data(), NULL);
        read_output.error += H5Sselect_hyperslab(memspace, H5S_SELECT_SET, destination.file_start_.data(), NULL, destination.GetCount().data(), NULL);
        read_output.error += H5Dread(dataset, destination.type_, memspace, dataspace, H5P_DEFAULT, destination.HDF5Input::buffer);
    }
    for (int r = 0; r < destination.rank_; r++) {
        DBGVAR(r);
        DBGVAR2(source.file_start_[r], source.GetCount()[r]);
        DBGVAR2(destination.file_start_[r], destination.GetCount()[r]);
    }
    //Close pointers
    read_output.error += H5Sclose(dataspace);
    read_output.error += H5Sclose(memspace);
    if (source.HDF5Input::layer != *Layer::LAST) {
        read_output.error += H5Dclose(dataset);
        read_output.error += H5Fclose(file);
    }
    return read_output;
}

HDF5Output HDF5Client::Write(HDF5Input source, HDF5Input destination) {
    AutoTrace trace = AutoTrace("HDF5Client::Write", source, destination);
    DBGVAR2(destination.filename, destination.HDF5Input::layer.layer_loc);
    HDF5WriteOutput write_output;
    CharStruct buffered_file = destination.HDF5Input::layer.layer_loc + FILE_PATH_SEPARATOR + destination.filename;
    /*write buffered data*/
    hid_t buf_file;
    hid_t buf_dset;
    hid_t file_space, mem_space;
    auto reduced_size = 0;
    //Open/create source dataset
    if (destination.HDF5Input::layer == *Layer::LAST) {
        DBGMSG("Using PFS file");

        dataset_get_wrapper(source.dataset_object, source.vol_id, H5VL_DATASET_GET_SPACE, H5P_DATASET_XFER_DEFAULT, NULL, &file_space);
        mem_space = H5Screate_simple(source.rank_, source.dimentions_.data(), source.max_dimentions_.data());
        write_output.error += H5Sselect_hyperslab(mem_space, H5S_SELECT_SET, source.file_start_.data(), NULL, source.GetCount().data(), NULL);
        write_output.error += H5Sselect_hyperslab(file_space, H5S_SELECT_SET, destination.file_start_.data(), NULL, destination.GetCount().data(), NULL);
        write_output.error += H5VLdataset_write(source.dataset_object, source.vol_id, destination.type_, mem_space, file_space, H5P_DEFAULT, destination.HDF5Input::buffer, NULL);

    } else {
        //Find buffer file if it exists
        if (FileExists(buffered_file.c_str())) {
            DBGMSG("Opened existing file");
            hid_t fapl = H5Pcreate(H5P_FILE_ACCESS);
            hid_t native_id = H5VLget_connector_id(H5VL_NATIVE_NAME);
            herr_t e = H5Pset_vol(fapl, native_id, NULL);
            if (destination.HDF5Input::layer.direct_io) write_output.error += H5Pset_fapl_direct(fapl, 4 * 1024, 4 * 1024, 4 * 1024);
            buf_file = H5Fopen(buffered_file.c_str(), H5F_ACC_RDWR, fapl);
        }
        //Create buffer file otherwise
        else {
            DBGMSG("Opened new file");
            hid_t fapl = H5Pcreate(H5P_FILE_ACCESS);
            hid_t native_id = H5VLget_connector_id(H5VL_NATIVE_NAME);
            herr_t e = H5Pset_vol(fapl, native_id, NULL);
            if (destination.HDF5Input::layer.direct_io)
                write_output.error += H5Pset_fapl_direct(fapl, 4 * 1024, 4 * 1024, 4 * 1024);
            buf_file = H5Fcreate(buffered_file.c_str(), H5F_ACC_EXCL, H5P_DEFAULT, fapl);
        }
        //Open dataset if it exists
        if (H5Lexists(buf_file, destination.dataset_name.c_str(), H5P_DEFAULT) != 0) {
            if (source.comp_lib == CompressionLibrary::DUMMY)
                buf_dset = H5Dopen(buf_file, destination.dataset_name.c_str(), H5P_DEFAULT);
                //The dataset will be overwritten if it's compressed
            else {
                H5Ldelete(buf_file, destination.dataset_name.c_str(), H5P_DEFAULT);
            }
            file_space = H5Dget_space(buf_dset);
        }
            //Create dataset otherwise
        else {
            //Create property list
            hid_t dcpl = H5Pcreate(H5P_DATASET_CREATE);
#ifdef ENABLE_COMPRESSION
            if(source.comp_lib != CompressionLibrary::DUMMY && destination.HDF5Input::layer!=*Layer::LAST){
                    auto cm_manager = Singleton<CompressionMetricsManager>::GetInstance();
                    std::hash<std::string> stringHash;
                    unsigned hash_value=stringHash(std::string(buffered_file.data()));
                    const unsigned cd_values[2] = {(unsigned)cm_manager->GetDatasetType(source.type_),hash_value};
                    hsize_t chunk_dims[source.rank_];

                    if(source.rank_ == 1) { chunk_dims[0] = CHUNK_SIZE <=source.GetCount()[0]  ? CHUNK_SIZE:source.GetCount()[0]; /* We want page size to be the chunk size.*/  }
                    else {
                        size_t val=sqrt(CHUNK_SIZE);
                        chunk_dims[0] = val<= source.GetCount()[0] ? val:source.GetCount()[0];
                        chunk_dims[1] = val<= source.GetCount()[1] ? val:source.GetCount()[1];
                        for(int i = 2; i < source.rank_; i++) chunk_dims[i] = 1;
                    }
                    int comp_lib = static_cast<int>(source.comp_lib);
                    hid_t stat = H5Pset_filter (dcpl, (H5Z_filter_t) H5Z_ARES_LIB_FILTER[comp_lib], H5Z_FLAG_MANDATORY, (size_t)2, cd_values);
                    stat = H5Pset_chunk (dcpl, source.rank_, chunk_dims);
                }
#endif
            file_space = H5Screate_simple(destination.rank_, destination.dimentions_.data(),
                                          destination.max_dimentions_.data());
            buf_dset = H5Dcreate2(buf_file, destination.dataset_name.c_str(), destination.type_, file_space,
                                  H5P_DEFAULT, dcpl, H5P_DEFAULT);
        }
        mem_space = H5Screate_simple(source.rank_, source.dimentions_.data(), source.max_dimentions_.data());
        write_output.error += H5Sselect_hyperslab(mem_space, H5S_SELECT_SET, source.file_start_.data(), NULL,
                                                  source.GetCount().data(), NULL);
        write_output.error += H5Sselect_hyperslab(file_space, H5S_SELECT_SET, destination.file_start_.data(), NULL,
                                                  destination.GetCount().data(), NULL);
        write_output.error += H5Dwrite(buf_dset, destination.type_, mem_space, file_space, H5P_DEFAULT,
                                       source.HDF5Input::buffer);
    }
    for (int r = 0; r < destination.rank_; r++) {
        DBGVAR(r);
        DBGVAR2(source.file_start_[r], source.GetCount()[r]);
        DBGVAR2(destination.file_start_[r], destination.GetCount()[r]);
    }

    //Free space
/*#ifndef ENABLE_COMPRESSION
    auto chaed_iter = is_changed.Get((std::to_string(HERMES_CONF->MY_SERVER)+"_"+std::to_string(destination.layer.id_)));
    auto size = destination.GetSize();
    if (chaed_iter.first) is_changed.Add((std::to_string(HERMES_CONF->MY_SERVER)+"_"+std::to_string(destination.layer.id_)), size);
    else is_changed.Put((std::to_string(HERMES_CONF->MY_SERVER)+"_"+std::to_string(destination.layer.id_)), size);
#else
    std::hash<std::string> stringHash;
    unsigned hash_value=stringHash(std::string(buffered_file.data()));
    auto cm_manager = Singleton<CompressionMetricsManager>::GetInstance();
    auto size=cm_manager->GetSize(hash_value);
    auto chaed_iter = is_changed.Get((std::to_string(HERMES_CONF->MY_SERVER)+"_"+std::to_string(destination.layer.id_)));
    if (chaed_iter.first) is_changed.Add((std::to_string(HERMES_CONF->MY_SERVER)+"_"+std::to_string(destination.layer.id_)), size);
    else is_changed.Put((std::to_string(HERMES_CONF->MY_SERVER)+"_"+std::to_string(destination.layer.id_)), size);
#endif*/
    write_output.error += H5Sclose(mem_space);
    write_output.error += H5Sclose(file_space);
    if (destination.HDF5Input::layer != *Layer::LAST) {
        write_output.error += H5Dclose(buf_dset);
        write_output.error += H5Fclose(buf_file);
    }
    return write_output;
}

HDF5Output HDF5Client::Delete(HDF5Input input) {
    AutoTrace trace = AutoTrace("HDF5Client::Delete", input);
    CharStruct buffered_file = input.HDF5Input::layer.layer_loc + FILE_PATH_SEPARATOR + input.filename;
    remove(buffered_file.c_str());
    return HDF5Output();
}

HDF5Output HDF5Client::Create(HDF5Input input) {
    AutoTrace trace = AutoTrace("HDF5Client::Create", input);
    HDF5Output output;
    CharStruct actual_file = input.HDF5Input::layer.layer_loc + FILE_PATH_SEPARATOR + input.filename;
    if (!FileExists(actual_file.c_str())) {
        /*create buffered data*/
        DBGMSG("file not created: " << actual_file);
    } else {
        DBGMSG("file: " << actual_file);

    }
    return output;
}

uint64_t HDF5Client::GetCurrentCapacity(Layer layer) {
    AutoTrace trace = AutoTrace("HDF5Client::GetCurrentCapacity", layer);
    /*auto was_changed = is_changed.Get((std::to_string(HERMES_CONF->MY_SERVER)+"_"+std::to_string(layer.id_)));
    if (!was_changed.first || was_changed.second == 0) {*/
        /* run system command du -s to calculate size of director. */
        std::string cmd = "du -s -B1 " + std::string(layer.layer_loc.c_str()) + " | awk {'print$1'}";
        FILE *fp;
        std::array<char, 128> buffer;
        std::string result;
        std::shared_ptr<FILE> pipe(popen(cmd.c_str(), "r"), pclose);
        if (!pipe) throw std::runtime_error("popen() failed!");
        while (!feof(pipe.get())) {
            if (fgets(buffer.data(), 128, pipe.get()) != nullptr)
                result += buffer.data();
        }
        auto val = std::stoll(result);
       /* is_changed.Put((std::to_string(HERMES_CONF->MY_SERVER)+"_"+std::to_string(layer.id_)), val);*/
        return val;
   /* } else return was_changed.second;*/
}
