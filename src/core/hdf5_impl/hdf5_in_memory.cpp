/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
 * <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of HCompress
 * 
 * HCompress is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
//
// Created by hariharan on 6/7/18.
//

#include <common/constants.h>
#include <debug.h>
#include <core/hdf5_impl/hdf5_in_memory.h>
#ifdef ENABLE_COMPRESSION
#include <ares_filter/compression_metrics_manager.h>
#include <ares_filter/hdf5_ares_filter.h>
#endif

HDF5Output HDF5InMemory::Create(HDF5Input input) {
    AutoTrace trace = AutoTrace("HDF5InMemory::Create",input);
    return HDF5Output();
}

HDF5Output HDF5InMemory::Read(HDF5Input source, HDF5Input destination) {
    AutoTrace trace = AutoTrace("HDF5InMemory::Read",source,destination);
    HDF5ReadOutput read_output;
    DBGVAR(source.filename);
    CharStruct buffered_file=source.HDF5Input::layer.layer_loc+FILE_PATH_SEPARATOR+source.filename;
    hid_t file = load(CharStruct(source.filename));
    if(file != -1){
        DBGMSG("Opened existing file");
        hid_t dataset = H5Dopen2(file, source.dataset_name.c_str(),H5P_DEFAULT);
        hid_t dataspace = H5Dget_space (dataset);
        read_output.error= H5Sselect_hyperslab (dataspace, H5S_SELECT_SET, source.file_start_.data(), NULL,
                                                source.GetCount().data(), NULL);
        hid_t memspace = H5Screate_simple (destination.rank_, destination.dimentions_.data(), destination.max_dimentions_.data());
        for(int r=0;r<destination.rank_;r++){
            DBGVAR(r);
            DBGVAR2(source.file_start_[r],source.GetCount()[r]);
            DBGVAR2(destination.file_start_[r],destination.GetCount()[r]);
        }
        read_output.error = H5Sselect_hyperslab (memspace, H5S_SELECT_SET, destination.file_start_.data(), NULL,
                                                 destination.GetCount().data(), NULL);
        read_output.error = H5Dread (dataset, destination.type_, memspace, dataspace,
                                     H5P_DEFAULT, destination.HDF5Input::buffer);
        read_output.error = H5Sclose (dataspace);
        read_output.error = H5Sclose (memspace);
        read_output.error = H5Dclose(dataset);
    }else{
        read_output.error=-1;
    }

    return read_output;
    return HDF5ReadOutput();
}

HDF5Output HDF5InMemory::Write(HDF5Input source, HDF5Input destination) {
    AutoTrace trace = AutoTrace("HDF5InMemory::Write",source,destination);
    DBGVAR2(destination.filename,destination.HDF5Input::layer.layer_loc);
    HDF5WriteOutput write_output;
    CharStruct buffered_file=destination.HDF5Input::layer.layer_loc+FILE_PATH_SEPARATOR+destination.filename;

    /*write buffered data*/
    hid_t buf_file;

    if(FileExists(CharStruct(buffered_file))){
        DBGMSG("Opened existing file");
        buf_file = load(buffered_file);
    }else{
        DBGMSG("Opened new file");
        hid_t fapl = H5Pcreate(H5P_FILE_ACCESS);
        hid_t native_id=H5VLget_connector_id(H5VL_NATIVE_NAME);
        herr_t  e = H5Pset_vol(fapl, native_id, NULL);
        write_output.error = H5Pset_fapl_core(fapl,source.GetSize(),false);
        buf_file = H5Fcreate(buffered_file.c_str(),  H5F_ACC_TRUNC, H5P_DEFAULT, fapl);
    }
    hid_t buf_dset;
    hid_t file_space;
    if(H5Lexists(buf_file, destination.dataset_name.c_str(), H5P_DEFAULT) != 0){
        buf_dset = H5Dopen(buf_file, destination.dataset_name.c_str(), H5P_DEFAULT);
    }else{
        hid_t dcpl = H5Pcreate (H5P_DATASET_CREATE);
#ifdef ENABLE_COMPRESSION
        if(source.comp_lib != CompressionLibrary::DUMMY && destination.HDF5Input::layer!=*Layer::LAST){
            auto cm_manager = Singleton<CompressionMetricsManager>::GetInstance();
            const unsigned cd_values[1] = {(unsigned)cm_manager->GetDatasetType(source.type_)};
            hsize_t chunk_dims[source.rank_];

            if(source.rank_ == 1) { chunk_dims[0] = CHUNK_SIZE <=source.GetCount()[0]  ? CHUNK_SIZE:source.GetCount()[0]; /* We want page size to be the chunk size.*/  }
            else {
                size_t val=sqrt(CHUNK_SIZE);
                chunk_dims[0] = val<= source.GetCount()[0] ? val:source.GetCount()[0];
                chunk_dims[1] = val<= source.GetCount()[1] ? val:source.GetCount()[1];
                for(int i = 2; i < source.rank_; i++) chunk_dims[i] = 1;
            }
            int comp_lib = static_cast<int>(source.comp_lib);
            hid_t stat = H5Pset_filter (dcpl, (H5Z_filter_t) H5Z_ARES_LIB_FILTER[comp_lib], H5Z_FLAG_MANDATORY, (size_t)1, cd_values);
            stat = H5Pset_chunk (dcpl, source.rank_, chunk_dims);
        }
#endif
        file_space = H5Screate_simple (destination.rank_, destination.dimentions_.data(), destination.max_dimentions_.data());
        buf_dset = H5Dcreate2 (buf_file, destination.dataset_name.c_str(), destination.type_, file_space, H5P_DEFAULT,
                               dcpl, H5P_DEFAULT);
    }
    file_space = H5Dget_space(buf_dset);
    hid_t mem_space = H5Screate_simple (source.rank_, source.dimentions_.data(), source.max_dimentions_.data());
    for(int r=0;r<source.rank_;r++){
        DBGVAR(r);
        DBGVAR2(source.file_start_[r],source.GetCount()[r]);
        DBGVAR2(destination.file_start_[r],destination.GetCount()[r]);
    }
    write_output.error = H5Sselect_hyperslab(mem_space, H5S_SELECT_SET, source.file_start_.data(), NULL,
                                             source.GetCount().data(), NULL);
    write_output.error = H5Sselect_hyperslab(file_space, H5S_SELECT_SET, destination.file_start_.data(), NULL,
                                             destination.GetCount().data(), NULL);
    write_output.error = H5Dwrite (buf_dset, destination.type_, mem_space, file_space, H5P_DEFAULT,
                                   source.HDF5Input::buffer);
    write_output.error = H5Sclose (mem_space);
    write_output.error = H5Sclose (file_space);
    write_output.error = H5Dclose (buf_dset);
    if(write_output.error >= 0){
        store(CharStruct(destination.filename),buf_file,source.GetSize());
    }
    return write_output;
}

HDF5Output HDF5InMemory::Delete(HDF5Input input) {
    AutoTrace trace = AutoTrace("HDF5InMemory::Delete",input);
    HDF5Output output;
    if(FileExists(CharStruct(input.filename))){
        hid_t fileId=load(input.filename);
        output.error = H5Fclose(fileId);
        existing_file.Erase(input.filename);
        bip::shared_memory_object::remove(input.filename.c_str());
    }
    return output;
}

uint64_t HDF5InMemory::GetCurrentCapacity(Layer layer) {
    AutoTrace trace = AutoTrace("HDF5InMemory::GetCurrentCapacity",layer);
    int64_t used_capacity=0;
    auto entries=existing_file.GetAllData();
    for (auto entry:entries){
        used_capacity+=entry.second.size;
    }
    return used_capacity;
}
