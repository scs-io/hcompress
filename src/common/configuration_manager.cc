/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
 * <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of HCompress
 * 
 * HCompress is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef ENABLE_COMPRESSION
#include <ares_filter/configuration_manager.h>
#include <ares_filter/compression_metrics_manager.h>
#endif
#include <common/configuration_manager.h>

extern "C" {

void SetRanksPerServer(int RANKS_PER_SERVER){
    HERMES_CONF->RANKS_PER_SERVER = RANKS_PER_SERVER;
    HERMES_CONF->calculateServerValues();
}

void SetHermesConfiguration(char *config){
    HERMES_CONF->configuration_file = config;
}

void SetMetricsFile(char *metrics) {
#ifdef ENABLE_COMPRESSION
    ARES_FILTER_CONF->metrics_file = metrics;
#endif
}

void SetCompressionLibrary(int comp_lib) {
#ifdef ENABLE_COMPRESSION
    ARES_FILTER_CONF->compressionLibrary = static_cast<CompressionLibrary>(comp_lib);
#endif
}

void PrintCompressionMetrics(void) {
#ifdef ENABLE_COMPRESSION
    Singleton<CompressionMetricsManager>::GetInstance()->PrintCompressionMetrics();
#endif
}

void SetMetricWeights(float w1, float w2, float w3) {
#ifdef ENABLE_COMPRESSION
    ARES_FILTER_CONF->SetMetricWeights(w1, w2, w3);
#endif
}

}
