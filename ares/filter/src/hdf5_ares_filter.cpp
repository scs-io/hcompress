/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
 * <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of HCompress
 * 
 * HCompress is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
//
// Created by lukemartinlogan on 7/25/19.
//

#include <hdf5.h>
#include <ares/c++/ares.h>
#include <ares_filter/hdf5_ares_filter.h>
#include <ares_filter/compression_metrics_manager.h>
#include <common/singleton.h>
#include <ares/c++/common/enumerations.h>
#include <ares/c++/common/data_structure.h>
#include <debug.h>
#include <ares_filter/configuration_manager.h>

//DUMMY, BZ2, ZLIB, HUFF, SF, RICE, RLE, TRLE, LZO, PITHY, SNAPPY, QLZ,

inline void H5_register_filter(const H5Z_class2_t *filter)
{
    if(H5Zregister(filter) < 0) {
        std::cout << "AresFilter::Failed to load filter " << filter->name << std::endl;
        exit(1);
    }
}

int H5_init_ares_filters(void)
{
    //AutoTrace trace = AutoTrace("AresFilter::H5_init_ares_filters", 0);
    H5_register_filter(H5Z_BZIP2);
    H5_register_filter(H5Z_ZLIB);
    H5_register_filter(H5Z_HUFF);
    H5_register_filter(H5Z_SF);
    H5_register_filter(H5Z_RICE);
    H5_register_filter(H5Z_RLE);
    H5_register_filter(H5Z_TRLE);
    H5_register_filter(H5Z_LZO);
    H5_register_filter(H5Z_PITHY);
    H5_register_filter(H5Z_SNAPPY);
    H5_register_filter(H5Z_QLZ);
	return 0;
}

inline size_t H5Z_ares_filter(unsigned int flags, size_t cd_nelmts,
                              const unsigned int cd_values[], size_t nbytes,
                              size_t *buf_size, void **buf,
                              CompressionLibrary comp_lib)
{
    AutoTrace trace = AutoTrace("AresFilter::H5Z_ares_filter");
    AresData data;
    void *new_buf;
    size_t new_size;
    if(ARES_FILTER_CONF->compressionLibrary!=CompressionLibrary::DYNAMIC){
        data.setLib(ARES_FILTER_CONF->compressionLibrary);
    } else {
        data.setLib(comp_lib);
    }
	auto cm_manager = Singleton<CompressionMetricsManager>::GetInstance();

    //Decompress
    if (flags & H5Z_FLAG_REVERSE){
        if(!ares_decompress(*buf, *buf_size, new_buf, new_size, &data))
            return 0;
        cm_manager->AddDecompressionMetrics(cd_values[0], comp_lib, new_size, data.decomp_time);
    }
    //Compress
    else {
        if(!ares_compress(*buf, *buf_size, new_buf, new_size, &data))
            return 0;
        cm_manager->AddCompressionMetrics(cd_values[0], comp_lib, *buf_size, (double)(*buf_size)/new_size, data.comp_time);
        cm_manager->UpdateSize(cd_values[1],new_size);
    }
    *buf = new_buf;
    *buf_size = new_size;

    //Feedback
    /*static uint64_t count = 0;
    if(count % 100 == 0)
        cm_manager->FitData();
    count++;*/
    return new_size;
}

size_t H5Z_filter_bzip2(unsigned int flags, size_t cd_nelmts,
                        const unsigned int cd_values[], size_t nbytes,
                        size_t *buf_size, void **buf)
{
    H5Z_ares_filter(flags, cd_nelmts, cd_values, nbytes, buf_size, buf, CompressionLibrary::BZ2);
}

size_t H5Z_filter_zlib(unsigned int flags, size_t cd_nelmts,
                        const unsigned int cd_values[], size_t nbytes,
                        size_t *buf_size, void **buf)
{
    H5Z_ares_filter(flags, cd_nelmts, cd_values, nbytes, buf_size, buf, CompressionLibrary::ZLIB);
}

size_t H5Z_filter_huff(unsigned int flags, size_t cd_nelmts,
                       const unsigned int cd_values[], size_t nbytes,
                       size_t *buf_size, void **buf)
{
    H5Z_ares_filter(flags, cd_nelmts, cd_values, nbytes, buf_size, buf, CompressionLibrary::HUFF);
}

size_t H5Z_filter_sf(unsigned int flags, size_t cd_nelmts,
                       const unsigned int cd_values[], size_t nbytes,
                       size_t *buf_size, void **buf)
{
    H5Z_ares_filter(flags, cd_nelmts, cd_values, nbytes, buf_size, buf, CompressionLibrary::SF);
}

size_t H5Z_filter_rice(unsigned int flags, size_t cd_nelmts,
                       const unsigned int cd_values[], size_t nbytes,
                       size_t *buf_size, void **buf)
{
    H5Z_ares_filter(flags, cd_nelmts, cd_values, nbytes, buf_size, buf, CompressionLibrary::RICE);
}

size_t H5Z_filter_rle(unsigned int flags, size_t cd_nelmts,
                       const unsigned int cd_values[], size_t nbytes,
                       size_t *buf_size, void **buf)
{
    H5Z_ares_filter(flags, cd_nelmts, cd_values, nbytes, buf_size, buf, CompressionLibrary::RLE);
}

size_t H5Z_filter_trle(unsigned int flags, size_t cd_nelmts,
                       const unsigned int cd_values[], size_t nbytes,
                       size_t *buf_size, void **buf)
{
    H5Z_ares_filter(flags, cd_nelmts, cd_values, nbytes, buf_size, buf, CompressionLibrary::TRLE);
}

size_t H5Z_filter_lzo(unsigned int flags, size_t cd_nelmts,
                       const unsigned int cd_values[], size_t nbytes,
                       size_t *buf_size, void **buf)
{
    H5Z_ares_filter(flags, cd_nelmts, cd_values, nbytes, buf_size, buf, CompressionLibrary::LZO);
}

size_t H5Z_filter_pithy(unsigned int flags, size_t cd_nelmts,
                       const unsigned int cd_values[], size_t nbytes,
                       size_t *buf_size, void **buf)
{
    H5Z_ares_filter(flags, cd_nelmts, cd_values, nbytes, buf_size, buf, CompressionLibrary::PITHY);
}

size_t H5Z_filter_snappy(unsigned int flags, size_t cd_nelmts,
                       const unsigned int cd_values[], size_t nbytes,
                       size_t *buf_size, void **buf)
{
    H5Z_ares_filter(flags, cd_nelmts, cd_values, nbytes, buf_size, buf, CompressionLibrary::SNAPPY);
}

size_t H5Z_filter_qlz(unsigned int flags, size_t cd_nelmts,
                       const unsigned int cd_values[], size_t nbytes,
                       size_t *buf_size, void **buf)
{
    H5Z_ares_filter(flags, cd_nelmts, cd_values, nbytes, buf_size, buf, CompressionLibrary::QLZ);
}
