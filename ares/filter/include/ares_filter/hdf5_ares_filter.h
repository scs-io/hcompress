/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
 * <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of HCompress
 * 
 * HCompress is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
//
// Created by lukemartinlogan on 7/25/19.
//

#ifndef HERMES_PROJECT_HDF5_ARES_FILTER_H
#define HERMES_PROJECT_HDF5_ARES_FILTER_H

//Filter ID
#define H5Z_FILTER_BZIP2 257
#define H5Z_FILTER_ZLIB 258
#define H5Z_FILTER_HUFF 259
#define H5Z_FILTER_SF 260
#define H5Z_FILTER_RICE 261
#define H5Z_FILTER_RLE 262
#define H5Z_FILTER_TRLE 263
#define H5Z_FILTER_LZO 264
#define H5Z_FILTER_PITHY 265
#define H5Z_FILTER_SNAPPY 266
#define H5Z_FILTER_QLZ 267

//Filter vector
const H5Z_filter_t H5Z_ARES_LIB_FILTER[NUM_ARES_LIBRARIES] = {
	0,
	H5Z_FILTER_BZIP2,
	H5Z_FILTER_ZLIB,
	H5Z_FILTER_HUFF,
	H5Z_FILTER_SF,
	H5Z_FILTER_RICE,
	H5Z_FILTER_RLE,
	H5Z_FILTER_TRLE,
	H5Z_FILTER_LZO,
	H5Z_FILTER_PITHY,
	H5Z_FILTER_SNAPPY,
	H5Z_FILTER_QLZ
};

//Prototypes

int H5_init_ares_filters(void);
size_t H5Z_filter_bzip2(unsigned int flags, size_t cd_nelmts,
                        const unsigned int cd_values[], size_t nbytes,
                        size_t *buf_size, void **buf);
size_t H5Z_filter_zlib(unsigned int flags, size_t cd_nelmts,
                       const unsigned int cd_values[], size_t nbytes,
                       size_t *buf_size, void **buf);
size_t H5Z_filter_huff(unsigned int flags, size_t cd_nelmts,
                       const unsigned int cd_values[], size_t nbytes,
                       size_t *buf_size, void **buf);
size_t H5Z_filter_sf(unsigned int flags, size_t cd_nelmts,
                     const unsigned int cd_values[], size_t nbytes,
                     size_t *buf_size, void **buf);
size_t H5Z_filter_rice(unsigned int flags, size_t cd_nelmts,
                       const unsigned int cd_values[], size_t nbytes,
                       size_t *buf_size, void **buf);
size_t H5Z_filter_rle(unsigned int flags, size_t cd_nelmts,
                      const unsigned int cd_values[], size_t nbytes,
                      size_t *buf_size, void **buf);
size_t H5Z_filter_trle(unsigned int flags, size_t cd_nelmts,
                       const unsigned int cd_values[], size_t nbytes,
                       size_t *buf_size, void **buf);
size_t H5Z_filter_lzo(unsigned int flags, size_t cd_nelmts,
                      const unsigned int cd_values[], size_t nbytes,
                      size_t *buf_size, void **buf);
size_t H5Z_filter_pithy(unsigned int flags, size_t cd_nelmts,
                        const unsigned int cd_values[], size_t nbytes,
                        size_t *buf_size, void **buf);
size_t H5Z_filter_snappy(unsigned int flags, size_t cd_nelmts,
                         const unsigned int cd_values[], size_t nbytes,
                         size_t *buf_size, void **buf);
size_t H5Z_filter_qlz(unsigned int flags, size_t cd_nelmts,
                      const unsigned int cd_values[], size_t nbytes,
                      size_t *buf_size, void **buf);

//Filter classes
const H5Z_class2_t H5Z_BZIP2[1] =
        {{
                 H5Z_CLASS_T_VERS,       /* H5Z_class_t version */
                 (H5Z_filter_t)H5Z_FILTER_BZIP2,         /* Filter id number             */
                 1,              /* encoder_present flag (set to true) */
                 1,              /* decoder_present flag (set to true) */
                 "bzip2",                  /* Filter name for debugging    */
                 NULL,                       /* The "can apply" callback     */
                 NULL,                       /* The "set local" callback     */
                 (H5Z_func_t)H5Z_filter_bzip2,         /* The actual filter function   */
         }};;
const H5Z_class2_t H5Z_ZLIB[1] =
        {{
                 H5Z_CLASS_T_VERS,       /* H5Z_class_t version */
                 (H5Z_filter_t)H5Z_FILTER_ZLIB,         /* Filter id number             */
                 1,              /* encoder_present flag (set to true) */
                 1,              /* decoder_present flag (set to true) */
                 "zlib",                  /* Filter name for debugging    */
                 NULL,                       /* The "can apply" callback     */
                 NULL,                       /* The "set local" callback     */
                 (H5Z_func_t)H5Z_filter_zlib,         /* The actual filter function   */
         }};;
const H5Z_class2_t H5Z_HUFF[1] =
        {{
                 H5Z_CLASS_T_VERS,       /* H5Z_class_t version */
                 (H5Z_filter_t)H5Z_FILTER_HUFF,         /* Filter id number             */
                 1,              /* encoder_present flag (set to true) */
                 1,              /* decoder_present flag (set to true) */
                 "huff",                  /* Filter name for debugging    */
                 NULL,                       /* The "can apply" callback     */
                 NULL,                       /* The "set local" callback     */
                 (H5Z_func_t)H5Z_filter_huff,         /* The actual filter function   */
         }};;
const H5Z_class2_t H5Z_SF[1] =
        {{
                 H5Z_CLASS_T_VERS,       /* H5Z_class_t version */
                 (H5Z_filter_t)H5Z_FILTER_SF,         /* Filter id number             */
                 1,              /* encoder_present flag (set to true) */
                 1,              /* decoder_present flag (set to true) */
                 "sf",                  /* Filter name for debugging    */
                 NULL,                       /* The "can apply" callback     */
                 NULL,                       /* The "set local" callback     */
                 (H5Z_func_t)H5Z_filter_sf,         /* The actual filter function   */
         }};;
const H5Z_class2_t H5Z_RICE[1] =
        {{
                 H5Z_CLASS_T_VERS,       /* H5Z_class_t version */
                 (H5Z_filter_t)H5Z_FILTER_RICE,         /* Filter id number             */
                 1,              /* encoder_present flag (set to true) */
                 1,              /* decoder_present flag (set to true) */
                 "rice",                  /* Filter name for debugging    */
                 NULL,                       /* The "can apply" callback     */
                 NULL,                       /* The "set local" callback     */
                 (H5Z_func_t)H5Z_filter_rice,         /* The actual filter function   */
         }};;
const H5Z_class2_t H5Z_RLE[1] =
        {{
                 H5Z_CLASS_T_VERS,       /* H5Z_class_t version */
                 (H5Z_filter_t)H5Z_FILTER_RLE,         /* Filter id number             */
                 1,              /* encoder_present flag (set to true) */
                 1,              /* decoder_present flag (set to true) */
                 "rle",                  /* Filter name for debugging    */
                 NULL,                       /* The "can apply" callback     */
                 NULL,                       /* The "set local" callback     */
                 (H5Z_func_t)H5Z_filter_rle,         /* The actual filter function   */
         }};;
const H5Z_class2_t H5Z_TRLE[1] =
        {{
                 H5Z_CLASS_T_VERS,       /* H5Z_class_t version */
                 (H5Z_filter_t)H5Z_FILTER_TRLE,         /* Filter id number             */
                 1,              /* encoder_present flag (set to true) */
                 1,              /* decoder_present flag (set to true) */
                 "trle",                  /* Filter name for debugging    */
                 NULL,                       /* The "can apply" callback     */
                 NULL,                       /* The "set local" callback     */
                 (H5Z_func_t)H5Z_filter_trle,         /* The actual filter function   */
         }};;
const H5Z_class2_t H5Z_LZO[1] =
        {{
                 H5Z_CLASS_T_VERS,       /* H5Z_class_t version */
                 (H5Z_filter_t)H5Z_FILTER_LZO,         /* Filter id number             */
                 1,              /* encoder_present flag (set to true) */
                 1,              /* decoder_present flag (set to true) */
                 "lzo",                  /* Filter name for debugging    */
                 NULL,                       /* The "can apply" callback     */
                 NULL,                       /* The "set local" callback     */
                 (H5Z_func_t)H5Z_filter_lzo,         /* The actual filter function   */
         }};;
const H5Z_class2_t H5Z_PITHY[1] =
        {{
                 H5Z_CLASS_T_VERS,       /* H5Z_class_t version */
                 (H5Z_filter_t)H5Z_FILTER_PITHY,         /* Filter id number             */
                 1,              /* encoder_present flag (set to true) */
                 1,              /* decoder_present flag (set to true) */
                 "pithy",                  /* Filter name for debugging    */
                 NULL,                       /* The "can apply" callback     */
                 NULL,                       /* The "set local" callback     */
                 (H5Z_func_t)H5Z_filter_pithy,         /* The actual filter function   */
         }};;
const H5Z_class2_t H5Z_SNAPPY[1] =
        {{
                 H5Z_CLASS_T_VERS,       /* H5Z_class_t version */
                 (H5Z_filter_t)H5Z_FILTER_SNAPPY,         /* Filter id number             */
                 1,              /* encoder_present flag (set to true) */
                 1,              /* decoder_present flag (set to true) */
                 "snappy",                  /* Filter name for debugging    */
                 NULL,                       /* The "can apply" callback     */
                 NULL,                       /* The "set local" callback     */
                 (H5Z_func_t)H5Z_filter_snappy,         /* The actual filter function   */
         }};;
const H5Z_class2_t H5Z_QLZ[1] =
        {{
                 H5Z_CLASS_T_VERS,       /* H5Z_class_t version */
                 (H5Z_filter_t)H5Z_FILTER_QLZ,         /* Filter id number             */
                 1,              /* encoder_present flag (set to true) */
                 1,              /* decoder_present flag (set to true) */
                 "qlz",                  /* Filter name for debugging    */
                 NULL,                       /* The "can apply" callback     */
                 NULL,                       /* The "set local" callback     */
                 (H5Z_func_t)H5Z_filter_qlz,         /* The actual filter function   */
         }};;	 
		 

#endif //HERMES_PROJECT_HDF5_ARES_FILTER_H
