/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
 * <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of HCompress
 * 
 * HCompress is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
//
// Created by lukemartinlogan on 7/22/19.
//

#ifndef ARES_FILTER_PROJECT_CONFIGURATION_MANAGER_H
#define ARES_FILTER_PROJECT_CONFIGURATION_MANAGER_H

#include <string>
#include <cstring>
#include <ares/c++/ares.h>
#include <common/singleton.h>

#include <rapidjson/document.h>
#include <rapidjson/writer.h>
#include <rapidjson/filewritestream.h>
#include <rapidjson/filereadstream.h>
#include <rapidjson/stringbuffer.h>
#include <rapidjson/reader.h>

#define ARES_FILTER_CONF Singleton<ConfigurationManager>::GetInstance()

class ConfigurationManager{
public:
    CompressionLibrary compressionLibrary;
    std::string metrics_file;
    float metric_weight[3]={.3,.34,.3};

    ConfigurationManager() :
            compressionLibrary(CompressionLibrary::DYNAMIC),
            metrics_file("./metrics.json"){}

    void SetMetricWeights(float w1, float w2, float w3) {
        metric_weight[0] = w1;
        metric_weight[1] = w2;
        metric_weight[2] = w3;
    }
};

#endif //ARES_FILTER_PROJECT_CONFIGURATION_MANAGER_H
