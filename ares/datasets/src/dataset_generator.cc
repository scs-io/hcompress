/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
 * <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of HCompress
 * 
 * HCompress is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "../include/dataset_generator.h"

extern "C" {


char* NormalDistributionc(int rank, size_t *dims, double mean, double stddev) {
    return DatasetGenerator<char>::NormalDistribution(rank, dims, mean, stddev);
}

char* NormalDistribution1c(size_t count, double mean, double stddev) {
    return DatasetGenerator<char>::NormalDistribution(count, mean, stddev);
}

char* GammaDistributionc(int rank, size_t *dims, double alpha, double beta) {
    return DatasetGenerator<char>::GammaDistribution(rank, dims, alpha, beta);
}

char* GammaDistribution1c(size_t count, double alpha, double beta) {
    return DatasetGenerator<char>::GammaDistribution(count, alpha, beta);
}

char* ExponentialDistributionc(int rank, size_t *dims, double theta) {
    return DatasetGenerator<char>::ExponentialDistribution(rank, dims, theta);
}

char* ExponentialDistribution1c(size_t count, double theta) {
    return DatasetGenerator<char>::ExponentialDistribution(count, theta);
}

char* UniformDistributionc(int rank, size_t *dims, double lower, double upper) {
    return DatasetGenerator<char>::UniformDistribution(rank, dims, lower, upper);
}

char* UniformDistribution1c(size_t count, double lower, double upper) {
    return DatasetGenerator<char>::UniformDistribution(count, lower, upper);
}

char* RandomDistributionc(DATASET_TYPE type, int rank, size_t *dims, double a, double b) {
    return DatasetGenerator<char>::RandomDistribution(type, rank, dims, a, b);
}

char* RandomDistribution1c(DATASET_TYPE type, size_t count, double a, double b) {
    return DatasetGenerator<char>::RandomDistribution(type, count, a, b);
}



int32_t* NormalDistributiond(int rank, size_t *dims, double mean, double stddev) {
    return DatasetGenerator<int32_t>::NormalDistribution(rank, dims, mean, stddev);
}

int32_t* NormalDistribution1d(size_t count, double mean, double stddev) {
    return DatasetGenerator<int32_t>::NormalDistribution(count, mean, stddev);
}

int32_t* GammaDistributiond(int rank, size_t *dims, double alpha, double beta) {
    return DatasetGenerator<int32_t>::GammaDistribution(rank, dims, alpha, beta);
}

int32_t* GammaDistribution1d(size_t count, double alpha, double beta) {
    return DatasetGenerator<int32_t>::GammaDistribution(count, alpha, beta);
}

int32_t* ExponentialDistributiond(int rank, size_t *dims, double theta) {
    return DatasetGenerator<int32_t>::ExponentialDistribution(rank, dims, theta);
}

int32_t* ExponentialDistribution1d(size_t count, double theta) {
    return DatasetGenerator<int32_t>::ExponentialDistribution(count, theta);
}

int32_t* UniformDistributiond(int rank, size_t *dims, double lower, double upper) {
    return DatasetGenerator<int32_t>::UniformDistribution(rank, dims, lower, upper);
}

int32_t* UniformDistribution1d(size_t count, double lower, double upper) {
    return DatasetGenerator<int32_t>::UniformDistribution(count, lower, upper);
}

int32_t* RandomDistributiond(DATASET_TYPE type, int rank, size_t *dims, double a, double b) {
    return DatasetGenerator<int32_t>::RandomDistribution(type, rank, dims, a, b);
}

int32_t* RandomDistribution1d(DATASET_TYPE type, size_t count, double a, double b) {
    return DatasetGenerator<int32_t>::RandomDistribution(type, count, a, b);
}




uint32_t* NormalDistributionu(int rank, size_t *dims, double mean, double stddev) {
    return DatasetGenerator<uint32_t>::NormalDistribution(rank, dims, mean, stddev);
}

uint32_t* NormalDistribution1u(size_t count, double mean, double stddev) {
    return DatasetGenerator<uint32_t>::NormalDistribution(count, mean, stddev);
}

uint32_t* GammaDistributionu(int rank, size_t *dims, double alpha, double beta) {
    return DatasetGenerator<uint32_t>::GammaDistribution(rank, dims, alpha, beta);
}

uint32_t* GammaDistribution1u(size_t count, double alpha, double beta) {
    return DatasetGenerator<uint32_t>::GammaDistribution(count, alpha, beta);
}

uint32_t* ExponentialDistributionu(int rank, size_t *dims, double theta) {
    return DatasetGenerator<uint32_t>::ExponentialDistribution(rank, dims, theta);
}

uint32_t* ExponentialDistribution1u(size_t count, double theta) {
    return DatasetGenerator<uint32_t>::ExponentialDistribution(count, theta);
}

uint32_t* UniformDistributionu(int rank, size_t *dims, double lower, double upper) {
    return DatasetGenerator<uint32_t>::UniformDistribution(rank, dims, lower, upper);
}

uint32_t* UniformDistribution1u(size_t count, double lower, double upper) {
    return DatasetGenerator<uint32_t>::UniformDistribution(count, lower, upper);
}

uint32_t* RandomDistributionu(DATASET_TYPE type, int rank, size_t *dims, double a, double b) {
    return DatasetGenerator<uint32_t>::RandomDistribution(type, rank, dims, a, b);
}

uint32_t* RandomDistribution1u(DATASET_TYPE type, size_t count, double a, double b) {
    return DatasetGenerator<uint32_t>::RandomDistribution(type, count, a, b);
}


float* NormalDistributionf(int rank, size_t *dims, double mean, double stddev) {
    return DatasetGenerator<float>::NormalDistribution(rank, dims, mean, stddev);
}

float* NormalDistribution1f(size_t count, double mean, double stddev) {
    return DatasetGenerator<float>::NormalDistribution(count, mean, stddev);
}

float* GammaDistributionf(int rank, size_t *dims, double alpha, double beta) {
    return DatasetGenerator<float>::GammaDistribution(rank, dims, alpha, beta);
}

float* GammaDistribution1f(size_t count, double alpha, double beta) {
    return DatasetGenerator<float>::GammaDistribution(count, alpha, beta);
}

float* ExponentialDistributionf(int rank, size_t *dims, double theta) {
    return DatasetGenerator<float>::ExponentialDistribution(rank, dims, theta);
}

float* ExponentialDistribution1f(size_t count, double theta) {
    return DatasetGenerator<float>::ExponentialDistribution(count, theta);
}

float* UniformDistributionf(int rank, size_t *dims, double lower, double upper) {
    return DatasetGenerator<float>::UniformDistribution(rank, dims, lower, upper);
}

float* UniformDistribution1f(size_t count, double lower, double upper) {
    return DatasetGenerator<float>::UniformDistribution(count, lower, upper);
}

float* RandomDistributionf(DATASET_TYPE type, int rank, size_t *dims, double a, double b) {
    return DatasetGenerator<float>::RandomDistribution(type, rank, dims, a, b);
}

float* RandomDistribution1f(DATASET_TYPE type, size_t count, double a, double b) {
    return DatasetGenerator<float>::RandomDistribution(type, count, a, b);
}


double* NormalDistributionlf(int rank, size_t *dims, double mean, double stddev) {
    return DatasetGenerator<double>::NormalDistribution(rank, dims, mean, stddev);
}

double* NormalDistribution1lf(size_t count, double mean, double stddev) {
    return DatasetGenerator<double>::NormalDistribution(count, mean, stddev);
}

double* GammaDistributionlf(int rank, size_t *dims, double alpha, double beta) {
    return DatasetGenerator<double>::GammaDistribution(rank, dims, alpha, beta);
}

double* GammaDistribution1lf(size_t count, double alpha, double beta) {
    return DatasetGenerator<double>::GammaDistribution(count, alpha, beta);
}

double* ExponentialDistributionlf(int rank, size_t *dims, double theta) {
    return DatasetGenerator<double>::ExponentialDistribution(rank, dims, theta);
}

double* ExponentialDistribution1lf(size_t count, double theta) {
    return DatasetGenerator<double>::ExponentialDistribution(count, theta);
}

double* UniformDistributionlf(int rank, size_t *dims, double lower, double upper) {
    return DatasetGenerator<double>::UniformDistribution(rank, dims, lower, upper);
}

double* UniformDistribution1lf(size_t count, double lower, double upper) {
    return DatasetGenerator<double>::UniformDistribution(count, lower, upper);
}

double* RandomDistributionlf(DATASET_TYPE type, int rank, size_t *dims, double a, double b) {
    return DatasetGenerator<double>::RandomDistribution(type, rank, dims, a, b);
}

double* RandomDistribution1lf(DATASET_TYPE type, size_t count, double a, double b) {
    return DatasetGenerator<double>::RandomDistribution(type, count, a, b);
}

}
