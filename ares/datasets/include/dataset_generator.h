/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
 * <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of HCompress
 * 
 * HCompress is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
//
// Created by lukemartinlogan on 7/28/19.
//

#ifndef HERMES_DATASET_GENERATOR_H
#define HERMES_DATASET_GENERATOR_H

typedef enum DATASET_TYPE {
    NORMAL_DISTRIBUTION,
    GAMMA_DISTRIBUTION,
    EXPONENTIAL_DISTRIBUTION,
    UNIFORM_DISTRIBUTION
} DATASET_TYPE;

#ifdef __cplusplus

#include <iostream>
#include <random>
#include <chrono>

template <typename T>
class DatasetGenerator
{
public:

    static size_t GetCount(int rank, size_t *dims) {
        size_t count = 1;
        for(int i = 0; i < rank; i++)
            count *= dims[i];
        return count;
    }

    static T* Allocate(int rank, size_t *dims) {
        size_t count = GetCount(rank, dims);
        T *dataset = (T*)malloc(count * sizeof(T));
        return dataset;
    }

    static T* Allocate(size_t count) {
        T *dataset = (T*)malloc(count * sizeof(T));
        return dataset;
    }

    static T* NormalDistribution(int rank, size_t *dims, double mean, double stddev) {
        static auto beginning = std::chrono::high_resolution_clock::now();
        size_t count = GetCount(rank, dims);
        T *dataset = (T*)malloc(count * sizeof(T));
        auto now = std::chrono::high_resolution_clock::now();

        std::default_random_engine generator(std::chrono::duration<double>(now - beginning).count()*10000);
        std::normal_distribution<double> distribution(mean, stddev);

        for(size_t i = 0; i < count; i++) {
            dataset[i] = (T)round(distribution(generator));
        }

        return dataset;
    }

    static T* NormalDistribution(size_t count, double mean, double stddev) {
        return NormalDistribution(1, &count, mean, stddev);
    }

    static T* GammaDistribution(int rank, size_t *dims, double alpha, double beta) {
        static auto beginning = std::chrono::high_resolution_clock::now();
        size_t count = GetCount(rank, dims);
        T *dataset = (T*)malloc(count * sizeof(T));
        auto now = std::chrono::high_resolution_clock::now();

        std::default_random_engine generator(std::chrono::duration<double>(now - beginning).count()*10000);
        std::gamma_distribution<double> distribution(alpha, beta);

        for(size_t i = 0; i < count; i++) {
            dataset[i] = (T)round(distribution(generator));
        }

        return dataset;
    }

    static T* GammaDistribution(size_t count, double alpha, double beta) {
        return GammaDistribution(1, &count, alpha, beta);
    }

    static T* ExponentialDistribution(int rank, size_t *dims, double theta) {
        static auto beginning = std::chrono::high_resolution_clock::now();
        size_t count = GetCount(rank, dims);
        T *dataset = (T*)malloc(count * sizeof(T));
        auto now = std::chrono::high_resolution_clock::now();

        std::default_random_engine generator(std::chrono::duration<double>(now - beginning).count()*10000);
        std::exponential_distribution<double> distribution(theta);

        for(size_t i = 0; i < count; i++) {
            dataset[i] = (T)round(distribution(generator));
        }

        return dataset;
    }

    static T* ExponentialDistribution(size_t count, double theta) {
        return ExponentialDistribution(1, &count, theta);
    }

    static T* UniformDistribution(int rank, size_t *dims, double lower, double upper) {
        static auto beginning = std::chrono::high_resolution_clock::now();
        size_t count = GetCount(rank, dims);
        T *dataset = (T*)malloc(count * sizeof(T));
        auto now = std::chrono::high_resolution_clock::now();

        std::default_random_engine generator(std::chrono::duration<double>(now - beginning).count()*10000);
        std::uniform_real_distribution<double> distribution(lower, upper);

        for(size_t i = 0; i < count; i++) {
            dataset[i] = (T)round(distribution(generator));
        }

        return dataset;
    }

    static T* UniformDistribution(size_t count, double lower, double upper) {
        return UniformDistribution(1, &count, lower, upper);
    }

    static T* RandomDistribution(DATASET_TYPE type, int rank, size_t *dims, double a, double b) {
        switch(type) {
            case NORMAL_DISTRIBUTION:
                return NormalDistribution(rank, dims, a, b);
            case GAMMA_DISTRIBUTION:
                return GammaDistribution(rank, dims, a, b);
            case EXPONENTIAL_DISTRIBUTION:
                return ExponentialDistribution(rank, dims, a);
            case UNIFORM_DISTRIBUTION:
                return UniformDistribution(rank, dims, a, b);
            default:
                return nullptr;
        }

        return nullptr;
    }

    static T* RandomDistribution(DATASET_TYPE type, int rank, size_t *dims, double a) {
        switch(type) {
            case EXPONENTIAL_DISTRIBUTION:
                return ExponentialDistribution(rank, dims, a);
            default:
                return nullptr;
        }
    }

    static T* RandomDistribution(DATASET_TYPE type, size_t count, double a, double b) {
        return RandomDistribution(type, 1, &count, a, b);
    }

    static T* RandomDistribution(DATASET_TYPE type, size_t count, double a) {
        return RandomDistribution(type, 1, &count, a);
    }
};
#endif

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

char* NormalDistributionc(int rank, size_t *dims, double mean, double stddev);
char* NormalDistribution1c(size_t count, double mean, double stddev);
char* GammaDistributionc(int rank, size_t *dims, double alpha, double beta);
char* GammaDistribution1c(size_t count, double alpha, double beta);
char* ExponentialDistributionc(int rank, size_t *dims, double theta);
char* ExponentialDistribution1c(size_t count, double theta);
char* UniformDistributionc(int rank, size_t *dims, double lower, double upper);
char* UniformDistribution1c(size_t count, double lower, double upper);
char* RandomDistributionc(DATASET_TYPE type, int rank, size_t *dims, double a, double b);
char* RandomDistribution1c(DATASET_TYPE type, size_t count, double a, double b);

int32_t* NormalDistributiond(int rank, size_t *dims, double mean, double stddev);
int32_t* NormalDistribution1d(size_t count, double mean, double stddev);
int32_t* GammaDistributiond(int rank, size_t *dims, double alpha, double beta);
int32_t* GammaDistribution1d(size_t count, double alpha, double beta);
int32_t* ExponentialDistributiond(int rank, size_t *dims, double theta);
int32_t* ExponentialDistribution1d(size_t count, double theta);
int32_t* UniformDistributiond(int rank, size_t *dims, double lower, double upper);
int32_t* UniformDistribution1d(size_t count, double lower, double upper);
int32_t* RandomDistributiond(DATASET_TYPE type, int rank, size_t *dims, double a, double b);
int32_t* RandomDistribution1d(DATASET_TYPE type, size_t count, double a, double b);

uint32_t* NormalDistributionu(int rank, size_t *dims, double mean, double stddev);
uint32_t* NormalDistribution1u(size_t count, double mean, double stddev);
uint32_t* GammaDistributionu(int rank, size_t *dims, double alpha, double beta);
uint32_t* GammaDistribution1u(size_t count, double alpha, double beta);
uint32_t* ExponentialDistributionu(int rank, size_t *dims, double theta);
uint32_t* ExponentialDistribution1u(size_t count, double theta);
uint32_t* UniformDistributionu(int rank, size_t *dims, double lower, double upper);
uint32_t* UniformDistribution1u(size_t count, double lower, double upper);
uint32_t* RandomDistributionu(DATASET_TYPE type, int rank, size_t *dims, double a, double b);
uint32_t* RandomDistribution1u(DATASET_TYPE type, size_t count, double a, double b);

float* NormalDistributionf(int rank, size_t *dims, double mean, double stddev);
float* NormalDistribution1f(size_t count, double mean, double stddev);
float* GammaDistributionf(int rank, size_t *dims, double alpha, double beta);
float* GammaDistribution1f(size_t count, double alpha, double beta);
float* ExponentialDistributionf(int rank, size_t *dims, double theta);
float* ExponentialDistribution1f(size_t count, double theta);
float* UniformDistributionf(int rank, size_t *dims, double lower, double upper);
float* UniformDistribution1f(size_t count, double lower, double upper);
float* RandomDistributionf(DATASET_TYPE type, int rank, size_t *dims, double a, double b);
float* RandomDistribution1f(DATASET_TYPE type, size_t count, double a, double b);

double* NormalDistributionlf(int rank, size_t *dims, double mean, double stddev);
double* NormalDistribution1lf(size_t count, double mean, double stddev);
double* GammaDistributionlf(int rank, size_t *dims, double alpha, double beta);
double* GammaDistribution1lf(size_t count, double alpha, double beta);
double* ExponentialDistributionlf(int rank, size_t *dims, double theta);
double* ExponentialDistribution1lf(size_t count, double theta);
double* UniformDistributionlf(int rank, size_t *dims, double lower, double upper);
double* UniformDistribution1lf(size_t count, double lower, double upper);
double* RandomDistributionlf(DATASET_TYPE type, int rank, size_t *dims, double a, double b);
double* RandomDistribution1lf(DATASET_TYPE type, size_t count, double a, double b);

#ifdef __cplusplus
}
#endif

#endif //HERMES_DATASET_GENERATOR_H
