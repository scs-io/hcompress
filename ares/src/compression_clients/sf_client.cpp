/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
 * <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of HCompress
 * 
 * HCompress is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
//
// Created by umashankar on 12/14/17.
//

#include <iostream>

#include <cstring>

#include <ares/c++/compression_clients/sf_client.h>
#include <debug.h>
#include <ares/c++/common/error_codes.h>

extern "C" {
#include "shannonfano.h"
}

using std::cerr;
using std::endl;

//-----------Estimate Compression Buffer---------------
size_t SFclient::est_compressed_size(size_t source_size)
{
    return source_size+1024*4; //(as suggested, in bcl lib's manual.doc
}

//-----------SHANNON-FANO COMPRESSION------------
bool SFclient::compress(SOURCE_TYPE source, size_t source_size, DESTINATION_TYPE destination, size_t &destination_size)
{
    //---Time measure begins---
    AutoTrace trace = AutoTrace("SFclient::compress");
    timer.start();

    //Compress
    destination_size = (size_t)SF_Compress((unsigned char *)source, (unsigned char *)destination, source_size);
    if(!destination_size){
        DBGVAR("Error in Shannon-Fano Compression, @ SF_Compress()!");
        return FAILURE;
    }

    //---Time measure ends---
    timer.stop();

    return SUCCESS;
}

//-----------SHANNON-FANO DECOMPRESSION------------
bool SFclient::decompress(SOURCE_TYPE source, size_t source_size, DESTINATION_TYPE destination, size_t &destination_size)
{
    //Allocate destination bufferd
    destination = malloc(destination_size);
    if(nullptr == destination){
        DBGVAR("Error in Shannon-Fano Decompression, @ malloc()!");
        return FAILURE;
    }

    //---Time measure begins---
    AutoTrace trace = AutoTrace("SFclient::decompress");
    timer.start();

    //Decompress
    SF_Uncompress((unsigned char*)source, (unsigned char*)destination, (unsigned int)source_size, (unsigned int)destination_size);
    if(!destination_size){
        DBGVAR("Error in Shannon-Fano Decompression, @ SF_Uncompress()!");
        free(destination);
        destination = nullptr;
        return FAILURE;
    }

    //---Time measure ends---
    timer.stop();

    return SUCCESS;
}

//****************************** EOF *********************************//
