/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
 * <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of HCompress
 * 
 * HCompress is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
//
// Created by umashankar on 12/14/17.
//

#include <iostream>

#include  <ares/c++/compression_clients/qlz_client.h>
#include <debug.h>
#include <ares/c++/common/error_codes.h>

extern "C" {
#include "quicklz.h"
}

using std::cerr;
using std::endl;

//-----------Estimate Compression Buffer---------------
size_t QLZclient::est_compressed_size(size_t source_size)
{
    return source_size + 400;
}

//-----------QLZ COMPRESSION------------
bool QLZclient::compress(SOURCE_TYPE source, size_t source_size, DESTINATION_TYPE destination, size_t &destination_size)
{
    //Init compression state
    auto *state_compress = (qlz_state_compress*)malloc(sizeof(qlz_state_compress));
    memset(state_compress, 0, sizeof(qlz_state_compress));

    //---Time measure begins---
    AutoTrace trace = AutoTrace("QLZclient::compress");
    timer.start();

    //Compress
    destination_size = qlz_compress(source, (char*)destination, source_size, state_compress);
    if (!destination_size){
        DBGVAR("Error in QLZ Compression, @ QLZ_compress()!");
        free(state_compress);
        return FAILURE;
    }

    //---Time measure ends---
    timer.stop();

    free(state_compress);
    return SUCCESS;
}

//-----------QLZ DECOMPRESSION------------
bool QLZclient::decompress(SOURCE_TYPE source, size_t source_size, DESTINATION_TYPE destination, size_t &destination_size)
{
    //Init decompression state
    auto *state_decompress = (qlz_state_decompress *)malloc(sizeof(qlz_state_decompress));
    memset(state_decompress, 0, sizeof(qlz_state_decompress));

    //Allocate destination buffer
    destination = malloc(destination_size);
    if(nullptr == destination){
        DBGVAR("Error in QLZ Decompression, @ malloc()!");
        return FAILURE;
    }

    //---Time measure begins---
    AutoTrace trace = AutoTrace("QLZclient::decompress");
    timer.start();

    //Decompress
    destination_size = qlz_decompress((const char*)source, destination, state_decompress);
    if (!destination_size){
        DBGVAR("Error in QLZ Decompression, @ QLZ_compress()!");
        free(state_decompress);
        free(destination);
        destination = nullptr;
        return FAILURE;
    }

    //---Time measure ends---
    timer.stop();

    free(state_decompress);
    return SUCCESS;
}

//****************************** EOF *********************************//
