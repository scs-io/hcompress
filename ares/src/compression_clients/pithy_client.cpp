/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
 * <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of HCompress
 * 
 * HCompress is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
//
// Created by umashankar on 12/14/17.
//

#include <iostream>

#include <ares/c++/compression_clients/pithy_client.h>
#include <debug.h>
#include <ares/c++/common/error_codes.h>

extern "C" {
#include "pithy.h"
}

using std::cerr;
using std::endl;

//-----------Estimate Compression Buffer---------------
size_t PITHYclient::est_compressed_size(size_t source_size)
{
    return pithy_MaxCompressedLength(source_size)+2*1024*1024;
}

//-----------PITHY COMPRESSION------------
bool PITHYclient::compress(SOURCE_TYPE source, size_t source_size, DESTINATION_TYPE destination, size_t &destination_size)
{
    //---Time measure begins---
    AutoTrace trace = AutoTrace("PITHYclient::compress");
    timer.start();

    //Compress - Compression level set to: 5 (in range 0-9)
    destination_size = pithy_Compress((char*)source, source_size, (char*)destination, destination_size, 5);
    if (!destination_size){
        DBGVAR("Error in PITHY Compression, @ pithy_Compress()!");
        return FAILURE;
    }


    //---Time measure ends---
    timer.stop();

    return SUCCESS;
}

//-----------PITHY DECOMPRESSION------------
bool PITHYclient::decompress(SOURCE_TYPE source, size_t source_size, DESTINATION_TYPE destination, size_t &destination_size)
{
    //Allocate destination buffer
    destination = malloc(destination_size);
    if(nullptr == destination){
        DBGVAR("Error in PITHY Decompression, @ malloc()!");
        return FAILURE;
    }

    //---Time measure begins---
    AutoTrace trace = AutoTrace("PITHYclient::decompress");
    timer.start();

    //Decompress
    if(!pithy_Decompress((char*)source, source_size, (char*)destination, destination_size)){
        DBGVAR("Error in PITHY Compression, @ pithy_Decompress()!");
        free(destination);
        destination = nullptr;
        return FAILURE;
    }

    //---Time measure ends---
    timer.stop();

    //Update destination
    return SUCCESS;
}

//****************************** EOF *********************************//
