/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
 * <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of HCompress
 * 
 * HCompress is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
//
// Created by umashankar on 12/14/17.
//

#include <cstring>
#include <iostream>

#include <ares/c++/compression_clients/rle_client.h>
#include <debug.h>
#include <ares/c++/common/error_codes.h>

extern "C" {
#include "rle.h"
}

using std::cerr;
using std::endl;

//-----------Estimate Compression Buffer---------------
size_t RLEclient::est_compressed_size(size_t source_size)
{
    return source_size * (257/256 + 1);
}

//-----------RLE COMPRESSION------------
bool RLEclient::compress(SOURCE_TYPE source, size_t source_size, DESTINATION_TYPE destination, size_t &destination_size)
{
    size_t off = AresMetadata::meta_size;

    //---Time measure begins---
    AutoTrace trace = AutoTrace("RLEclient::compress");
    timer.start();

    //Compress
    destination_size = (size_t)RLE_Compress((unsigned char*)source, (unsigned char*)destination, (unsigned int)source_size);
    if(!destination_size){
        DBGVAR("Error in RLE Compression, @ RLE_Compress()!");
        return FAILURE;
    }

    //---Time measure ends---
    timer.stop();

    return SUCCESS;
}

//-----------RLE DECOMPRESSION------------
bool RLEclient::decompress(SOURCE_TYPE source, size_t source_size, DESTINATION_TYPE destination, size_t &destination_size)
{
    size_t off = AresMetadata::meta_size;

    //Allocate destination buffer
    destination = malloc(destination_size);
    if(nullptr == destination){
        DBGVAR("Error in RLE Decompression, @ malloc()!");
        return FAILURE;
    }

    //---Time measure begins---
    AutoTrace trace = AutoTrace("RLEclient::decompress");
    timer.start();

    //Decompress
    RLE_Uncompress((unsigned char*)source, (unsigned char*)destination, (unsigned int)source_size);
    if(!destination_size){
        DBGVAR("Error in RLE Decompression, @ RLE_Uncompress()!");
        free(destination);
        destination = nullptr;
        return FAILURE;
    }

    //---Time measure ends---
    timer.stop();

    return SUCCESS;
}

//****************************** EOF *********************************//
