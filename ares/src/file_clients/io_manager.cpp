/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
 * <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of HCompress
 * 
 * HCompress is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
//
// Created by umashankar on 3/17/18.
//


#include <hdf5.h>

#include <iostream>
#include <vector>
#include <algorithm>

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <sys/stat.h>
#include <sys/types.h>

#include <ares/c++/file_clients/io_manager.h>
#include <debug.h>
#include <ares/c++/common/error_codes.h>
#include <ares/c++/common/utilities.h>
#include <boost/filesystem.hpp>


//-----------FIGURE OUT THE DESTINATION FILE's NAME---------------
std::string IOManager::DeduceFilename(const std::string &file_path, bool op_flag)
{
    boost::filesystem::path filepath(file_path);
    if(op_flag){
        return filepath.parent_path().string()+boost::filesystem::path::preferred_separator+
               filepath.filename().string()+".ares";
    }else{
        return filepath.parent_path().string()+boost::filesystem::path::preferred_separator+
               filepath.stem().string();
    }
}

//-----------CALCULATE FILE SIZE---------------
size_t IOManager::GetFileSize(const std::string &file_name)
{
    struct stat stat_buf = {0};

    if(0 == stat(file_name.c_str(), &stat_buf)){
        return static_cast<size_t>(stat_buf.st_size);
    }
    else{
        DBGVAR("IOManager::GetFileSize(): File size can't be determined!");
        return 0;
    }
}

bool IOManager::write(std::string filename, void *&buffer, size_t &size, DataFormat format, bool op) {
    //Get the correct file name for the (compressed) output file
    std::string output_file_name;
    if(format!=DataFormat::DUMMY){
        output_file_name = DeduceFilename(filename, op);
    }else{
        output_file_name = filename;
    }


    //Create an output file to hold the compressed data
    std::FILE * file_handler = fopen(output_file_name.c_str(), "w+");

    size_t write_size = size;
    //Add the compressed data to the file
    write_size = fwrite(buffer, sizeof(char), size, file_handler);
    if (write_size != size) {
        DBGVAR("ares_compress(buffer)->file: Writing buffer contents to file post compression met with an error");
        fclose(file_handler);
        return FAILURE;
    }
    //Cleanup
    fclose(file_handler);
    return SUCCESS;
}

bool IOManager::read(std::string filename, void * &buffer, size_t &size) {
    //Get the file's extension to identify the library used for compression
    unsigned int lib_value = 0;
    std::string file_ext;
    size = GetFileSize(filename);
    //Allocate source buffer
    buffer = malloc(sizeof(char) * size);
    if(nullptr == buffer){
        DBGVAR("ares_decompress(file)->buffer: source buffer for size " << size << " bytes can't be allocated!");
        return FAILURE;
    }

    std::FILE *file_handler_posix = std::fopen(filename.c_str(), "r+");
    if(!file_handler_posix) {
        DBGVAR("IOManager::read_file(): POSIX file opening failed!");
        return FAILURE;
    }

    //Read the file
    size_t read_size = std::fread(buffer, sizeof(char), size, file_handler_posix);
    if(read_size != size){
        DBGVAR("IOManager::read_file(): POSIX file read (for size: "<< size << ") failed!");
        return FAILURE;
    }
    //Cleanup (POSIX)
    fclose(file_handler_posix);
    return SUCCESS;
}


//********************************** EOF ******************************************
