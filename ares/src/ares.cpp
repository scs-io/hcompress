/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
 * <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of HCompress
 * 
 * HCompress is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
//
// Created by umashankar on 4/8/18.
//
#include <iostream>
#include <fstream>
#include <memory>
#include <vector>
#include <algorithm>
#include <exception>

#include <ctime>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cstdarg>
#include <dirent.h>
#include <sys/stat.h>

#include <ares/c++/ares.h>
#include <ares/c++/file_clients/io_manager.h>
#include <ares/c++/common/enumerations.h>
#include <debug.h>
#include <ares/c++/common/error_codes.h>
#include <ares/c++//compression_clients/lib_factory.h>
#include <ares/c++/common/utilities.h>
#include <common/singleton.h>

//------------- COMPRESSION - BUFFER to BUFFER -------------
bool ares_compress(const void * source, size_t source_size, void *&destination, size_t &destination_size, AresData *data)
{
    AutoTrace trace = AutoTrace("Ares::ares_compress");
    bool is_compressed = true;
	void *source_temp = (char*)source;
	void *true_destination;
	
    //Check for NULL or empty source buffer
    if(source == nullptr || source_size == 0){
        DBGVAR("ares_compress(buffer)->buffer: Source buffer is NULL, or it is empty (0 bytes)!");
        return FAILURE;
    }
	
    //Instantiate library factory
    LibFactory &lib_factory = LibFactory::GetInstance();
	
	//Get the library based on its chosen index
	auto comp_lib = lib_factory.get_library(data->lib);

	//If the compression library exits, use it to compress the source buffer
	if(comp_lib){
	    //Estimate compressed size. Does not account for metadata.
		destination_size = comp_lib->est_compressed_size(source_size);
		//Compression library assumes we allocate the compressed buffer
		if(destination_size > 0) {
		    //Allocate compression buffer
            true_destination = malloc(destination_size + AresMetadata::meta_size);
            if(true_destination == nullptr){
                DBGVAR("Engine::compress(): Failed to malloc!");
                return FAILURE;
            }
            //Offset destination to account for metadata
            destination = (char*)true_destination + AresMetadata::meta_size;
            //Compress the source buffer into the end of the destination buffer
            if(!comp_lib->compress(source_temp, source_size, destination, destination_size)){
                DBGVAR("Engine::compress(): Compression Failed!");
                free(true_destination);
                return FAILURE;
            }
        }
		//Compression library allocates buffer
		else {
            //Compress the source buffer into the destination buffer
            if(!comp_lib->compress(source_temp, source_size, destination, destination_size)){
                DBGVAR("Engine2::compress(): Compression Failed!");
                free(destination);
                return FAILURE;
            }
            //Make space for metadata
            true_destination = malloc(destination_size + AresMetadata::meta_size);
            if(true_destination == nullptr){
                DBGVAR("Engine2::compress(): Failed to malloc!");
                return FAILURE;
            }
            //Copy compressed data if it's better than no compression
            if(destination_size <= source_size) {
                AutoTrace trace = AutoTrace("Ares::ares_compress::Realloc");
                std::memcpy((char *) true_destination + AresMetadata::meta_size, destination, destination_size);
            }
            std::free(destination);
		}
	}
	else{
		DBGVAR("Engine::compress(): Error! No library exists for the given index");
		return FAILURE;
	}

	//Use uncompressed data if it's better
	if(destination_size > source_size) {
        is_compressed = false;
        void *new_destination = malloc(destination_size + AresMetadata::meta_size);
        if(new_destination == nullptr) {
            DBGVAR("Engine::compress(): Compression Failed!");
            return false;
        }
        comp_lib->store_uncompressed_data(source_temp, source_size, new_destination, source_size);
        std::free(true_destination);
        true_destination = new_destination;
        destination_size = source_size;
	}

	//Update destination size
	destination = true_destination;
	destination_size += AresMetadata::meta_size;

	//Encode destination buffer with metadata
	AresMetadata m;
	m.Encode(source_temp, source_size, destination, destination_size, is_compressed);

    DBGVAR("COMPRESSION SUCCESS");
	data->comp_time += comp_lib->time_elapsed_msec();
    return SUCCESS;
}


//------------- DECOMPRESSION - BUFFER to BUFFER -------------
bool ares_decompress(const void *source, size_t source_size, void *&destination, size_t &destination_size, AresData *data)
{
    AutoTrace trace = AutoTrace("Ares::ares_decompress");

    //Check for NULL source buffer
    if(source == nullptr){
        DBGVAR("ares_decompress(buffer)->buffer: Source buffer is NULL!");
        return FAILURE;
    }

	//Get compressed data
    void* source_temp = (char*)source;

    //Decode metadata
    AresMetadata m;
    m.Decode(source_temp);

	//Get compression library
	LibFactory &lib_factory = LibFactory::GetInstance();
	auto comp_lib = lib_factory.get_library(data->lib);

	//If the compression library exits, use it to decompress the source buffer
	if(comp_lib){
		destination_size = m.uncomp_size;
		//Offset source to account for metadata
		source_temp = (char*)source + AresMetadata::meta_size;
		source_size -= AresMetadata::meta_size;
		//The source buffer is not compressed
		if(!m.is_compressed) {
		    if(!comp_lib->restore_uncompressed_data(source_temp, source_size, destination, destination_size)) {
                DBGVAR("ares_decompress(buffer)->buffer: Can't restore uncompressed data from source!");
                return FAILURE;
		    }
		}
		//The source buffer is compressed
		else {
		    if(!comp_lib->decompress(source_temp, source_size, destination, destination_size)){
		        DBGVAR("Engine::decompress(): Decompression Failed!");
		        return FAILURE;
		    }
		}
	}
	else{
		DBGVAR("Engine::decompress(): Error! No library exists for the given index!");
		return FAILURE;
	}
	
	DBGVAR("DECOMPRESSION SUCCESS");
    data->decomp_time += comp_lib->time_elapsed_msec();
    return SUCCESS;
}


//------------- COMPRESSION - BUFFER to FILE -------------
bool ares_compress(void *&source, size_t source_size, std::string output_file, AresData *data)
{
    void *destination = nullptr;
    size_t destination_size = 0;

    //Check for NULL or empty source buffer
    if(source == nullptr || source_size == 0){
        DBGVAR("ares_compress(buffer)->file: Source buffer is NULL, or it is empty (0 bytes)!");
        return FAILURE;
    }
	
	//Compress buffer 
    if(!ares_compress(source,source_size,destination,destination_size,data)){
        DBGVAR("ares_compress(): Compression Failed!");
        return FAILURE;
    }
    //Instantiate the helper classes
    auto io_mgr = Singleton<IOManager>::GetInstance();
    if(!io_mgr->write(output_file,destination,destination_size,DataFormat::ARES,true)){
        DBGVAR("ares_compress(): Compression Failed!");
        return FAILURE;
    }
    free(destination);
    destination = nullptr;
    return SUCCESS;
}


//------------- DECOMPRESSION - FILE to BUFFER -------------
bool ares_decompress(std::string file_path, void *&destination, size_t &destination_size, AresData *data)
{
    void *source;
    size_t source_size = 0;
    auto io_mgr = Singleton<IOManager>::GetInstance();
    io_mgr->read(file_path,source,source_size);
    return ares_decompress(source,source_size,destination,destination_size, data);
}


//------------- COMPRESSION - FILE/DIR to FILE/DIR -------------
bool ares_compress(std::string file_path, AresData *data)
{
    std::cout<<"filepath:"<<file_path<<"\n";
    auto io_mgr = Singleton<IOManager>::GetInstance();
    if(IsFile(file_path)){
        void *source= nullptr;
        size_t source_size = 0;
        io_mgr->read(file_path,source,source_size);
        ares_compress(source, source_size, file_path,data);
        free(source);
        //remove(file_path.c_str());
    }else{
        DIR *dir;
        struct dirent *dir_entry;
        //Open the directory
        if( (dir = opendir(file_path.c_str())) != nullptr ) {
            //Read directory contents
            while((dir_entry = readdir(dir)) != nullptr){
                //Note: Sub-directories in the given directory aren't going to be processed(now). So, skip them.
                if (dir_entry->d_type != DT_DIR){ //if(dir_entry->d_name != "." || dir_entry->d_name != ". ."){
                    void *source;
                    size_t source_size = 0;
                    io_mgr->read(dir_entry->d_name,source,source_size);
                    ares_compress(source, source_size, dir_entry->d_name,data);
                    free(source);
                    //remove(dir_entry->d_name);
                }
            }
            //Cleanup
            closedir (dir);
        }
    }
    return SUCCESS;
}


//------------- DECOMPRESSION - FILE/DIR to FILE/DIR -------------
bool ares_decompress(std::string file_path, AresData *data)
{
    auto io_mgr = Singleton<IOManager>::GetInstance();
    if(IsFile(file_path)){
        void *source;
        size_t source_size = 0;
        ares_decompress(file_path,source, source_size, data);
        //io_mgr.write(file_path,source,source_size,library.format_,false);
        free(source);
        //remove(file_path.c_str());
    }else{
        DIR *dir;
        struct dirent *dir_entry;
        //Open the directory
        if( (dir = opendir(file_path.c_str())) != nullptr ) {
            //Read directory contents
            while((dir_entry = readdir(dir)) != nullptr){
                //Note: Sub-directories in the given directory aren't going to be processed(now). So, skip them.
                if (dir_entry->d_type != DT_DIR){ //if(dir_entry->d_name != "." || dir_entry->d_name != ". ."){
                    void *source;
                    size_t source_size = 0;
                    ares_decompress(dir_entry->d_name,source, source_size, data);
                    io_mgr->write(dir_entry->d_name,source,source_size,DataFormat::ARES,false);
                    free(source);
                    //remove(dir_entry->d_name);
                }
            }
            //Cleanup
            closedir (dir);
        }
    }
    return SUCCESS;
}


//****************************** EOF *********************************//
