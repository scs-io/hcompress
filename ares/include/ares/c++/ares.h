/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
 * <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of HCompress
 * 
 * HCompress is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
//
// Created by umashankar on 4/5/18.
//

#ifndef ARES_ARES_H
#define ARES_ARES_H

#include <string>
#include <ares/c++/common/data_structure.h>

//------------------ Compression Routines -------------------

/* buffer to buffer - 2 default parameters. library choice = Only ares */
bool ares_compress(const void * source, size_t source_size, void *&destination, size_t &destination_size, AresData *data);

/* buffer to file - 3 default parameters */
bool ares_compress(void *&source, size_t source_size, std::string output_file, AresData *data);

/* file/dir to file/dir - 3 default parameters */
bool ares_compress(std::string file_path, AresData *data);



//----------------- Decompression Routines ------------------

/* buffer to buffer */
bool ares_decompress(const void *source, size_t source_size, void *&destination, size_t &destination_size, AresData *data);

/* file to buffer */
bool ares_decompress(std::string file_path, void *&destination, size_t &destination_size, AresData *data);

/* file/dir to file/dir */
bool ares_decompress(std::string file_path, AresData *data);



#endif //ARES_ARES_H
