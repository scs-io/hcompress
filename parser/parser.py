# Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
# Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
# <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
#
# This file is part of HCompress
# 
# HCompress is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

"""
This file will reconstruct the call stack from
a per-thread log and estimate the total time
spent performing a series of tasks.

For example, it answers the question of how
long the mapping engine takes to select a
particular compression library.

INPUTS:
    argv[1] = [PARSE_TIMER/PARSE_ACCURACY/GET_TIMER_METRICS/GET_TIMER_METRICS2/GET_ACCURACY]
    argv[2] = input log
    argv[3] = output csv

This script requires all three arguments.

${item};${file_name};${rank};{func_name};;;start"
${item};${file_name};${rank};{func_name};${time};${time_units};finish"
"""

import pandas as pd
import sys
import re
import os.path

write_metrics = {
    "mapping_engine",
    "library_selection",
    "compression",
    "library_metadata_encoding",
    "feedback",
    "write"
}

read_metrics = {
    "library_metadata_decoding",
    "library_selection",
    "decompression",
    "feedback",
    "read"
}

net_metrics = {}
net_metrics = {*net_metrics, *read_metrics}
net_metrics = {*net_metrics, *write_metrics}

func_to_metric = {
    #Mapping engine functions
    "HDF5MaxPerformanceDataPlacementEngine::PlaceWriteData" : "mapping_engine",
    "HDF5MaxPerformanceDataPlacementEngine::FindOptimalCompression" : "mapping_engine",
    "HDF5MaxPerformanceDataPlacementEngine::CalculatePlacementScore" : "mapping_engine",
    #Library selection functions
    "LibFactory::get_library" : "library_selection",
    #Compression functions
    "BZ2client::compress" : "compression",
    "HUFFclient::compress" : "compression",
    "LZOclient::compress" : "compression",
    "PITHYclient::compress" : "compression",
    "QLZclient::compress" : "compression",
    "RICEclient::compress" : "compression",
    "RLEclient::compress" : "compression",
    "SFclient::compress" : "compression",
    "SNAPPYclient::compress" : "compression",
    "TRLEclient::compress" : "compression",
    "ZLIBclient::compress" : "compression",
    "LibClient::store_uncompressed_data" : "compression",
    #Feedback functions
    "AresFilter::H5Z_ares_filter" : "feedback",
    "CompressionMetricsManager::FitData" : "feedback",
    #Write functions
    "HDF5Client::Write" : "write",
    "HDF5InMemory::Write" : "write",
    #Library metadata encoding functions
    "Ares::ares_compress::Realloc" : "library_metadata_encoding",
    "AresMetadata::encode" : "library_metadata_encoding",
    #Library metadata decoding functions
    "AresMetadata::decode" : "library_metadata_decoding",
    #Library decompression functions
    "BZ2client::decompress" : "decompression",
    "HUFFclient::decompress" : "decompression",
    "LZOclient::decompress" : "decompression",
    "PITHYclient::decompress" : "decompression",
    "QLZclient::decompress" : "decompression",
    "RICEclient::decompress" : "decompression",
    "RLEclient::decompress" : "decompression",
    "SFclient::decompress" : "decompression",
    "SNAPPYclient::decompress" : "decompression",
    "TRLEclient::decompress" : "decompression",
    "ZLIBclient::decompress" : "decompression",
    "LibClient::restore_uncompressed_data" : "decompression",
    #Read functions
    "HDF5Client::Read" : "read",
    "HDF5InMemory::Read" : "read"
}

#Ignore these functions when counting function calls made per metric
ignore_existence = {
    "HDF5MaxPerformanceDataPlacementEngine::FindOptimalCompression",
    "HDF5MaxPerformanceDataPlacementEngine::CalculatePlacementScore",
    "CompressionMetricsManager::FitData"
}

ignore_exec_time = {
    "HDF5MaxPerformanceDataPlacementEngine::PlaceWriteData",
    "AresFilter::H5Z_ares_filter"
}

write_functions = {
    "H5_BufferWrite",
    "HDF5Client::Write",
    "HDF5InMemory::Write"
}

read_functions = {
    "H5_BufferRead",
    "HDF5Client::Read",
    "HDF5InMemory::Read"
}

def function_is_called(line):
    if re.search("start$", line):
        return True
    elif re.search("finish$", line):
        return False

    raise Exception("Cannot determine if this is a function call or return")

def extract_function_time(line):
    time_re = re.search("[^;]+;[^;]+;[^;]+;[^;]+;(.*?);", line)
    if time_re:
        return float(time_re.group(1))
    return -1

def extract_current_timer_function(line):
    func_name_re = re.search("[^;]+;[^;]+;[^;]+;(.*?);", line)
    if func_name_re:
        return func_name_re.group(1)
    return -1

def parse_log_timer(out_path, line_list):
    records = []
    func_stack = []
    func_rw_group = 0
    func_metric_group = 0
    for line in line_list:
        func_name = extract_current_timer_function(line)
        if func_name == -1:
            continue
        if function_is_called(line):
            #Determine if function called by read/write function
            func_rw_group = 0
            if func_name in read_functions:
                func_rw_group = 1
            elif func_name in write_functions:
                func_rw_group = 2
            elif (len(func_stack) > 0):
                func_rw_group = func_stack[-1][1]
            #Determine if function is apart of a metric
            func_metric_group = 0
            if func_name in func_to_metric:
                func_metric_group = func_to_metric[func_name]
            #Determine if the function depends on another
            count = 1
            if func_name in ignore_existence:
                count = 0
            #Append record
            func_stack.append([func_name, func_rw_group, func_metric_group, 0, 0, count])
        else:
            time = extract_function_time(line)
            if time == -1:
                continue
            if func_stack[-1][0] not in ignore_exec_time:
                func_stack[-1][3] += time
            records.append(func_stack[-1])
            func_stack.pop(-1)
            if len(func_stack) > 0:
                if func_stack[-1][0] not in ignore_exec_time:
                    func_stack[-1][4] += time

    #Create dataframe
    df = pd.DataFrame(records, columns=["func_name", "func_rw_group", "func_metric_group", "exec_time", "sub_exec_time", "count"])
    df["exec_time"] -= df["sub_exec_time"]
    df = df.drop("sub_exec_time", axis=1)
    #Commit records
    output = open(out_path, "w")
    df.to_csv(output, index=False, header=True)

def extract_current_accuracy_function(line):
    func_name_re = re.search("(.*?)-", line)
    if not func_name_re:
        return -1
    return func_name_re.group(1);

def extract_feedback_accuracy(line, func_name, records):
    if re.search("CompressionMetricsManager::AddCompressionMetrics", func_name):
        tc = float(re.search("CompressionMetricsManager::AddCompressionMetrics-{(.*?)}", line).group(1))
        r = float(re.search("CompressionMetricsManager::AddCompressionMetrics-{.*?}-{(.*?)}", line).group(1))
        tc_est = float(re.search("CompressionMetricsManager::AddCompressionMetrics-{.*?}-{.*?}-{(.*?)}", line).group(1))
        r_est = float(re.search("CompressionMetricsManager::AddCompressionMetrics-{.*?}-{.*?}-{.*?}-{(.*?)}", line).group(1))
        if r_est == 0 or tc_est == 0:
            return
        records.append([tc,tc_est, 1-abs(tc-tc_est)/tc,r,r_est, 1-abs(r-r_est)/r,None,None,None])
    elif re.search("CompressionMetricsManager::AddDecompressionMetrics", func_name):
        td = float(re.search("CompressionMetricsManager::AddDecompressionMetrics-{(.*?)}", line).group(1))
        td_est = float(re.search("CompressionMetricsManager::AddDecompressionMetrics-{.*?}-{(.*?)}", line).group(1))
        if td == 0 or td_est == 0:
            return
        records.append([None,None,None,None,None,None,td,td_est,1-abs(td-td_est)/td])

def parse_log_accuracy(out_path, line_list):
    records=[]
    for line in line_list:
        func_name = extract_current_accuracy_function(line)
        if func_name == -1:
            continue
        extract_feedback_accuracy(line,func_name,records)

    #Create dataframes
    df = pd.DataFrame(records, columns=["tc", "tc_est", "tc_err", "r", "r_est", "r_err", "td", "td_est", "td_err"])
    #Commit records
    output = open(out_path, "w")
    df.to_csv(output, index=False, header=True)

#Get number of times each function is called
def get_func_counts(out_path, df):
    #Compute average cost of all metrics
    count_funcs = df.groupby("func_name", as_index=False).count()
    count_read_funcs = df[df["func_rw_group"] == 1].groupby("func_name", as_index=False).count()
    count_write_funcs = df[df["func_rw_group"] == 2].groupby("func_name", as_index=False).count()
    #Commit
    output = open(out_path, "w")
    count_funcs[["func_name", "count"]].to_csv(output, index=False)
    count_read_funcs[["func_name", "count"]].to_csv(output, index=False)
    count_write_funcs[["func_name", "count"]].to_csv(output, index=False)

#Get number of times each function is called
def get_func_count_sums(out_path, df):
    #Compute average cost of all metrics
    count_funcs = df.groupby("func_name", as_index=False).count()
    count_read_funcs = df[df["func_rw_group"] == 1].groupby("func_name", as_index=False).sum()
    count_write_funcs = df[df["func_rw_group"] == 2].groupby("func_name", as_index=False).sum()
    #Commit
    output = open(out_path, "w")
    count_funcs[["func_name", "count"]].to_csv(output, index=False)
    count_read_funcs[["func_name", "count"]].to_csv(output, index=False)
    count_write_funcs[["func_name", "count"]].to_csv(output, index=False)

#Get average cost of each function
def get_avg_func_cost(out_path, df):
    #Compute average cost of all metrics
    avg_read_funcs = df[df["func_rw_group"] == 1].groupby("func_name", as_index=False).mean()
    avg_write_funcs = df[df["func_rw_group"] == 2].groupby("func_name", as_index=False).mean()
    #Commit
    output = open(out_path, "w")
    avg_read_funcs[["func_name", "exec_time"]].to_csv(output, index=False)
    avg_write_funcs[["func_name", "exec_time"]].to_csv(output, index=False)

#Get average cost of each metric
def get_avg_metric_cost(out_path, df):
    #Compute average cost of all metrics
    read_groups = df[df["func_rw_group"] == 1].groupby("func_metric_group", as_index=False)
    write_groups = df[df["func_rw_group"] == 2].groupby("func_metric_group", as_index=False)
    avg_read_funcs = read_groups.sum()
    avg_write_funcs = write_groups.sum()
    avg_read_funcs["exec_time"] = avg_read_funcs["exec_time"].divide(avg_read_funcs["count"])
    avg_write_funcs["exec_time"] = avg_write_funcs["exec_time"].divide(avg_write_funcs["count"])
    #Commit
    output = open(out_path, "w")
    avg_read_funcs[["func_metric_group", "exec_time"]].to_csv(output, index=False)
    avg_write_funcs[["func_metric_group", "exec_time"]].to_csv(output, index=False)

#Get average cost of each metric (with feedback hack)
def get_avg_metric_cost2(out_path, df):
    #Compute average cost of all metrics
    read_groups = df[df["func_rw_group"] == 1].groupby("func_metric_group", as_index=False)
    write_groups = df[df["func_rw_group"] == 2].groupby("func_metric_group", as_index=False)
    avg_read_funcs = read_groups.sum()
    avg_write_funcs = write_groups.sum()
    avg_read_funcs["exec_time"] = avg_read_funcs["exec_time"].divide(avg_read_funcs["count"])
    avg_write_funcs["exec_time"] = avg_write_funcs["exec_time"].divide(avg_write_funcs["count"])
    #Divide feedback cost by 100
    feedback_read_df = avg_read_funcs[avg_read_funcs["func_metric_group"] == "feedback"]
    feedback_write_df = avg_write_funcs[avg_write_funcs["func_metric_group"] == "feedback"]
    if feedback_read_df.shape[0] > 0:
        avg_read_funcs.loc[avg_read_funcs["func_metric_group"]=="feedback", "exec_time"] /= 100
    if feedback_write_df.shape[0] > 0:
        avg_write_funcs.loc[avg_write_funcs["func_metric_group"]=="feedback", "exec_time"] /= 100
    #Commit
    output = open(out_path, "w")
    avg_read_funcs[["func_metric_group", "exec_time"]].to_csv(output, index=False)
    avg_write_funcs[["func_metric_group", "exec_time"]].to_csv(output, index=False)

#Get the average throughput of dpe
def get_mapping_engine_throughput(out_path, df):
    #Average mapping engine throughput (MB/s)
    avg_library_selection = df[df["func_metric_group"] == "mapping_engine"]
    if avg_library_selection.shape[0] > 0:
        place_write_data = avg_library_selection[avg_library_selection["func_name"]=="HDF5MaxPerformanceDataPlacementEngine::PlaceWriteData"]
        calc_placement = avg_library_selection[avg_library_selection["func_name"]=="HDF5MaxPerformanceDataPlacementEngine::CalculatePlacementScore"]
        avg_library_selection = avg_library_selection.sum()
        avg_library_selection = (place_write_data.count()["count"]/calc_placement.count()["count"])*(1/.0009)*10**3
        avg_library_selection = pd.DataFrame({"throughput" : avg_library_selection}, columns=["throughput"], index=[0])
    else:
        avg_library_selection = pd.DataFrame({"throughput" : 0}, columns=["throughput"], index=[0])
    #Commit
    output = open(out_path, "w")
    avg_library_selection.to_csv(output, index=False)

#Get the average throughput of feedback
def get_feedback_throughput(out_path, df):
    #Average feedback engine throughput (MB/s)
    avg_feedback = df[df["func_metric_group"] == "feedback"]
    avg_feedback = avg_feedback.sum()
    avg_feedback = avg_feedback["count"]/avg_feedback["exec_time"] * 10**3
    avg_feedback = pd.DataFrame({"throughput" : avg_feedback}, columns=["throughput"], index=[0])
    #Commit
    output = open(out_path, "w")
    avg_feedback.to_csv(output, index=False)

#Get the average accuracy of feedback
def get_feedback_accuracy(out_path, df):
    tc_df = df["tc_err"].dropna()
    r_df = df["r_err"].dropna()
    td_df = df["td_err"].dropna()
    tc_accuracy = tc_df.mean()
    r_accuracy = r_df.mean()
    td_accuracy = td_df.mean()
    accuracy = (tc_accuracy + r_accuracy + td_accuracy)/3
    avg_feedback = pd.DataFrame({"accuracy" : accuracy}, columns=["accuracy"], index=[0])
    #Commit
    output = open(out_path, "w")
    avg_feedback.to_csv(output, index=False)


#####MAIN
if len(sys.argv) != 4:
    print("Invalid number of arguments. This program requires exactly 3.")
    print()
    sys.exit(1)
if sys.argv[1] == "PARSE_TIMER":
    print("PARSE_TIMER")
    log = open(sys.argv[2], "r")
    line_list = log.readlines()
    parse_log_timer(sys.argv[3], line_list)
    print()
elif sys.argv[1] == "PARSE_ACCURACY":
    print("PARSE_ACCURACY")
    log = open(sys.argv[2], "r")
    line_list = log.readlines()
    parse_log_accuracy(sys.argv[3], line_list)
    print()
elif sys.argv[1] == "GET_AVG_FUNC_COST":
    print("GET_AVG_FUNC_COST")
    df = pd.read_csv(sys.argv[2])
    get_avg_func_cost(sys.argv[3], df)
    print()
elif sys.argv[1] == "GET_AVG_METRIC_COST":
    print("GET_AVG_METRIC_COST")
    df = pd.read_csv(sys.argv[2])
    get_avg_metric_cost(sys.argv[3], df)
    print()
elif sys.argv[1] == "GET_AVG_METRIC_COST2":
    print("GET_AVG_METRIC_COST2")
    df = pd.read_csv(sys.argv[2])
    get_avg_metric_cost2(sys.argv[3], df)
    print()
elif sys.argv[1] == "GET_FUNC_COUNTS":
    print("GET_FUNC_COUTNS")
    df = pd.read_csv(sys.argv[2])
    get_func_counts(sys.argv[3], df)
    print()
elif sys.argv[1] == "GET_FUNC_COUNT_SUMS":
    print("GET_FUNC_COUNT_SUMS")
    df = pd.read_csv(sys.argv[2])
    get_func_count_sums(sys.argv[3], df)
    print()
elif sys.argv[1] == "GET_MAPPING_ENGINE_THROUGHPUT":
    print("GET_MAPPING_ENGINE_THROUGHPUT")
    df = pd.read_csv(sys.argv[2])
    get_mapping_engine_throughput(sys.argv[3], df)
    print()
elif sys.argv[1] == "GET_FEEDBACK_THROUGHPUT":
    print("GET_FEEDBACK_THROUGHPUT")
    df = pd.read_csv(sys.argv[2])
    get_feedback_throughput(sys.argv[3], df)
    print()
elif sys.argv[1] == "GET_FEEDBACK_ACCURACY":
    print("GET_FEEDBACK_ACCURACY")
    df = pd.read_csv(sys.argv[2])
    get_feedback_accuracy(sys.argv[3], df)
    print()
else:
    print("Invalid parser call: " + sys.argv[1])
    sys.exit(1)
