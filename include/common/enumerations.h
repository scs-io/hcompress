/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
 * <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of HCompress
 * 
 * HCompress is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
/*-------------------------------------------------------------------------
*
* Created: enumerations.h
* June 5 2018
* Hariharan Devarajan <hdevarajan@hdfgroup.org>
*
* Purpose: Defines enumerations for Hermes.
*
*-------------------------------------------------------------------------
*/

#ifndef HERMES_ENUMERATIONS_H
#define HERMES_ENUMERATIONS_H


#include <rpc/msgpack.hpp>

/**
 * Operation Type enum defines the various operations supported in the Hermes platform.
 */
typedef enum OperationType{
    WRITE=0,
    READ=1
} OperationType;

MSGPACK_ADD_ENUM(OperationType);

/**
 * EngineType defines the various Data Placement Engines present in the Hermes platform.
 */
typedef enum EngineType{
    HDF_MAX_BW=0
} EngineType;
/**
 * InterfaceType defines various interfaces supported by Hermes.
 */
typedef enum InterfaceType{
    HDF5=0
} InterfaceType;
/**
 * ReplacementPolicyType defines various cache replacement policies supported by Hermes.
 */
typedef enum ReplacementPolicyType{
    LRU=0,
    LFU=1
} ReplacementPolicyType;
/**
 * IOClient defines various types of IO Clients supported by Hermes
 */
typedef enum IOClientType{
    HDF5_FILE=0,
    HDF5_MEMORY=1
} IOClientType;

/**
 * MDMType defines various types of MDM supported by Hermes
 */
typedef enum MDMType{
    MDM_FILE_PER_PROCESS=0,
    MDM_OPT=1
} MDMType;

MSGPACK_ADD_ENUM(IOClientType);

/**
 * Prefetch enums
 */

typedef enum EventType{
    OPEN_FILE=0,
    CLOSE_FILE=1,
    OPEN_DATASET=2,
    CLOSE_DATASET=3,
    READ_DATASET=4,
    WRITE_DATASET=5
} EventType;

MSGPACK_ADD_ENUM(EventType);

typedef enum PrefetchStatus{
    PREFETCH_SUCCESS=0,
    PREFETCH_FAILED=1
} PrefetchStatus;

typedef enum GraphManagerType{
    HDF5_GRAPH_MANAGER=0
} GraphManagerType;

typedef enum PredictionEngineType{
    HDF5_HISTORY_BASED_PREDICTION_ENGINE=0
} PredictionEngineType;

typedef enum DataPlacementEngineType{
    HDF5_MAX_BW_DATA_PLACEMENT_ENGINE=0
} DataPlacementEngineType;

typedef enum PredictionMode{
    GRAPH=0,
    PATTERN_PREDICTOR=1
} PredictionMode;

#ifndef ENABLE_COMPRESSION
enum class CompressionLibrary //"lib_choice" argument in ares_compress()
{
    /* 0 */
    DUMMY
    };
#endif


#endif //HERMES_ENUMERATIONS_H
