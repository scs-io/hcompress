/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
 * <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of HCompress
 * 
 * HCompress is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
//
// Created by umashankar on 12/22/2017.
//

#ifndef ARES_TIME_COUNT_H
#define ARES_TIME_COUNT_H

#include <chrono>

class TimeCounter
{
public:
    TimeCounter() = default; //Ctor
    ~TimeCounter() = default; //Dtor

    void start()
    {
        begin = std::chrono::high_resolution_clock::now();
    }

    void stop()
    {
        end = std::chrono::high_resolution_clock::now();
    }

    //Get time in seconds precision
    double get_duration_sec()
    {
        return std::chrono::duration<double>(end - begin).count();
    }

    //Get time in milliseconds precision
    double get_duration_msec()
    {
        return std::chrono::duration<double, std::milli>(end - begin).count();
    }

private:
    std::chrono::system_clock::time_point begin;
    std::chrono::system_clock::time_point end;
};

#endif //ARES_TIME_COUNT_H
