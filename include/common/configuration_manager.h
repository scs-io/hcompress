/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
 * <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of HCompress
 * 
 * HCompress is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
//
// Created by lukemartinlogan on 7/22/19.
//

#ifndef HERMES_PROJECT_CONFIGURATION_MANAGER_H
#define HERMES_PROJECT_CONFIGURATION_MANAGER_H


#ifdef __cplusplus

#include <string>
#include <cstring>
#include <common/singleton.h>
#include <common/data_structures.h>
#include <common/configuration_manager.h>
#include <hermes.h>

#include <rapidjson/document.h>
#include <rapidjson/writer.h>
#include <rapidjson/filewritestream.h>
#include <rapidjson/filereadstream.h>
#include <rapidjson/stringbuffer.h>
#include <rapidjson/reader.h>

#define HERMES_CONF Singleton<HermesConfiguration>::GetInstance()
/**
 * FIXME: change HermesConfiguration to ConfigurationManager.
 */
class HermesConfiguration{
public:
    bool enable_hermes;
    std::string configuration_file;
    LayerInfo *layers;
    int num_layers,rank;
    int RANKS_PER_SERVER; /* Defines the number of ranks per server */
    int COMM_SIZE,NUM_SERVERS;
    uint16_t MY_SERVER;
    bool SERVER_ON_NODE;
    bool IS_SERVER;

    EngineType ENGINE_TYPE; /* defines the Data Placement Engine to be used. (Default HDF_MAX_BW: Maximum Bandwidth)*/
    ReplacementPolicyType CACHE_REPLACEMENT_POLICY; /* Defines which cache replacement policy to be used (Default LRU: Least recently used. */
    int MAX_PREFETCH_EVENTS; /* Defines the maximum number of events thats would be prefetched at a given time */
    double MAX_PREFETCH_TIME; /* Defines the prefetch window to wait for more events */
    GraphManagerType CURRENT_GRAPH_MANAGER_TYPE; /* configures the graph Manager type */
    PredictionEngineType CURRENT_PREDICTION_ENGINE_TYPE; /* configures the prediction engine type */
    DataPlacementEngineType CURRENT_DATA_PLACEMENT_ENGINE_TYPE; /* configures the data placement engine for prefetching */

    size_t THREADS_PER_SERVER;
    uint16_t RPC_PORT;
    size_t MAX_NUM_FILES;
    MDMType MDM_TYPE;
    PredictionMode PREDICTION_MODE;
    std::string APPLICATION_GRAPH_PATH;
    std::string APPLICATION_GRAPH_NAME;
    long PREFETCHING_ALGO_DELAY_US;
    size_t MAX_DIMS;
    double CHUNK_SIZE;

    HermesConfiguration() :
            enable_hermes(true),
            configuration_file("configuration.json"),
            layers(NULL),
            num_layers(0),
            ENGINE_TYPE(EngineType::HDF_MAX_BW),
            CACHE_REPLACEMENT_POLICY(ReplacementPolicyType::LRU),
            MAX_PREFETCH_EVENTS(1),
            MAX_PREFETCH_TIME(10),
            CURRENT_GRAPH_MANAGER_TYPE(GraphManagerType::HDF5_GRAPH_MANAGER),
            CURRENT_PREDICTION_ENGINE_TYPE(PredictionEngineType::HDF5_HISTORY_BASED_PREDICTION_ENGINE),
            CURRENT_DATA_PLACEMENT_ENGINE_TYPE(DataPlacementEngineType::HDF5_MAX_BW_DATA_PLACEMENT_ENGINE),
            THREADS_PER_SERVER(4),
            RPC_PORT(8000),
            MAX_NUM_FILES(1024),
            MDM_TYPE(MDMType::MDM_OPT),
            PREDICTION_MODE(PredictionMode::GRAPH),
            APPLICATION_GRAPH_PATH("/home/hariharan/CLionProjects/hfetch/test/workflow/graph/"),
            APPLICATION_GRAPH_NAME("h5_buffer_read_fpp_mpi.graph"),
            PREFETCHING_ALGO_DELAY_US(750000),
            MAX_DIMS(2),
            RANKS_PER_SERVER(1),
            CHUNK_SIZE(1048576.0),rank(0),
            COMM_SIZE(1),NUM_SERVERS(1),
            MY_SERVER(0),
            SERVER_ON_NODE(true),
            IS_SERVER(true){
        MPI_Comm_rank(MPI_COMM_WORLD, &rank);
        MPI_Comm_size(MPI_COMM_WORLD, &COMM_SIZE);
        calculateServerValues();
    }
    void calculateServerValues(){
        NUM_SERVERS=COMM_SIZE/RANKS_PER_SERVER;
        MY_SERVER=rank/RANKS_PER_SERVER;
        SERVER_ON_NODE=true;
        IS_SERVER=rank%RANKS_PER_SERVER==0;
    }

    int Configure(void) {
        //Open input file
        using namespace rapidjson;
        FILE *outfile = fopen(configuration_file.c_str(), "r");
        if(outfile == NULL){
            std::cout << "HermesConfiguration - Canfiguration JSON not found" << std::endl;
            char* homepath = getenv("RUN_DIR");
            num_layers = 4;
            layers = (LayerInfo*)malloc(num_layers * sizeof(LayerInfo));
            sprintf(layers[0].mount_point_,"%s/ramfs/",homepath);
            layers[0].capacity_mb_=1024;
            layers[0].bandwidth=8000;
            layers[0].is_memory=true;
            sprintf(layers[1].mount_point_,"%s/nvme/",homepath);
            layers[1].capacity_mb_=2*1024;
            layers[1].bandwidth=2000;
            layers[1].is_memory=false;
            sprintf(layers[2].mount_point_,"%s/bb/",homepath);
            layers[2].capacity_mb_=4*1024;
            layers[2].bandwidth=400;
            layers[2].is_memory=false;
            sprintf(layers[3].mount_point_,"%s/pfs/",homepath);
            layers[3].capacity_mb_=8*1024;
            layers[3].bandwidth=100;
            layers[3].is_memory=false;
            return 0;
        }

        //Initialize read stream
        char buf[65536];
        FileReadStream instream(outfile, buf, sizeof(buf));
        Document d;

        //Get JSON object
        d.ParseStream<kParseStopWhenDoneFlag>(instream);
        if(!d.IsObject()) {
            std::cout << "HermesConfiguration - Canfiguration JSON is invalid" << std::endl;
            fclose(outfile);
            return -1;
        }

        //Read in layers
        assert(d["layer_info"].IsArray());
        num_layers = (int)d["layer_info"].Size();
        layers = (LayerInfo*)malloc(num_layers * sizeof(LayerInfo));
        int i = 0;
        for (Value::ConstValueIterator itr = d["layer_info"].Begin(); itr != d["layer_info"].End(); ++itr) {
            const Value& attr = (Value&)*itr;
            layers[i].capacity_mb_= attr["capacity_mb"].GetFloat();
            layers[i].bandwidth= attr["bandwidth"].GetFloat();
            layers[i].is_memory= attr["is_memory"].GetInt();
            strcpy(layers[i].mount_point_, attr["mount_point"].GetString());
            layers[i].direct_io= attr["direct_io"].GetInt();
            //std::cout << "Layer : "<<i<<" s: "<<layers[i].mount_point_<< std::endl;
            i++;
        }



        return 0;
    }
};

#endif //HERMES_PROJECT_CONFIGURATION_MANAGER_H


#ifdef __cplusplus
extern "C" {
#endif

void SetRanksPerServer(int RANKS_PER_SERVER);
void SetHermesConfiguration(char *config);
void SetMetricsFile(char *metrics);
void SetCompressionLibrary(int comp_lib);
void PrintCompressionMetrics(void);
void SetMetricWeights(float w1, float w2, float w3);

#ifdef __cplusplus
}
#endif

#endif
