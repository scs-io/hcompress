/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
 * <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of HCompress
 * 
 * HCompress is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
/*-------------------------------------------------------------------------
*
* Created: hermes_vol_private.c
* June 11 2018
* Hariharan Devarajan <hdevarajan@hdfgroup.org>
*
* Purpose:Defines the Hermes VOL Plugin
*
*-------------------------------------------------------------------------
*/

#ifndef HERMES_PROJECT_HERMES_VOL_H
#define HERMES_PROJECT_HERMES_VOL_H
#include "H5public.h"
#include "H5Rpublic.h"
#include "hermes.h"

#ifdef __cplusplus
extern "C" {
#endif
#define HERMES 555
/**
 * This method implements the setting of vol driver inside application's fapl.
 *
 * @param fapl_id
 * @param layers
 * @param count_
 * @return vol_id
 */
H5_DLL hid_t H5Pset_fapl_hermes_vol(hid_t fapl_id,LayerInfo* layers, uint16_t count_);

#ifdef __cplusplus
}
#endif
#endif //HERMES_PROJECT_HERMES_VOL_H
