/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
 * <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of HCompress
 * 
 * HCompress is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
/*-------------------------------------------------------------------------
*
* Created: hermes.h
* June 5 2018
* Hariharan Devarajan <hdevarajan@hdfgroup.org>
*
* Purpose:Defines the Buffer API's for Hermes Platform
*
*-------------------------------------------------------------------------
*/

#ifndef HERMES_HERMES_H
#define HERMES_HERMES_H

#include <hdf5.h>
#include "H5public.h"
#include "H5Rpublic.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Data structure to hold LayerInfo
 */
typedef struct LayerInfo {
    char mount_point_[256];
    float capacity_mb_;
    float bandwidth;
    bool is_memory;
    bool direct_io;
} LayerInfo;


/**
 * This API updates a given layer in Hermes. It has four layers and ID starts
 * from 0 the fastes to 3 the slowest.
 *
 * @param layers
 * @param count
 * @return 0 Success <0 Error
 */
herr_t H5_UpdateLayer(LayerInfo* layers,int count);
/**
 * This API should be called on dataset creation or open.
 * This initializes the buffer for the coming dataset.
 *
 * @param filename
 * @param dataset_name
 * @param rank_
 * @param type_
 * @param dimentions_
 * @param max_dimentions_
 * @param dataset_id
 * @return 0 Success <0 Error
 */
herr_t H5_BufferInit(char* filename, char* dataset_name, int rank_, int64_t type_, hsize_t* dimentions_, hsize_t* max_dimentions_, void* dataset_object, hid_t vol_id);
/**
 * This API write data into the buffers for a perticular dataset.
 *
 * @param filename
 * @param dataset_name
 * @param rank_
 * @param type_
 * @param file_start_
 * @param file_end_
 * @param buf_start_
 * @param buf_end_
 * @param dataset_id
 * @param buffer
 * @return  0 Success <0 Error
 */
herr_t H5_BufferWrite(char* filename, char* dataset_name, int rank_, int64_t type_, hsize_t* file_start_, hsize_t* file_end_, hsize_t* buf_start_, hsize_t* buf_end_, void* dataset_object, hid_t vol_id, void * buffer);
/**
 * This API reads data from the buffers for a perticular dataset.
 *
 * @param filename
 * @param dataset_name
 * @param rank_
 * @param type_
 * @param file_start_
 * @param file_end_
 * @param buf_start_
 * @param buf_dim_
 * @param dataset_id
 * @param buffer
 * @return  0 Success <0 Error
 */
herr_t H5_BufferRead(char* filename, char* dataset_name, int rank_, int64_t type_, hsize_t* file_start_, hsize_t* file_end_, hsize_t* buf_start_, hsize_t* buf_dim_, void* dataset_object, hid_t vol_id, void * buffer);
/**
 * This API sync data from the buffers to the PFS for a perticular dataset.
 *
 * @param filename
 * @param dataset_name
 * @param rank_
 * @param type_
 * @param file_start_
 * @param file_end_
 * @param buf_start_
 * @param buf_end_
 * @param dataset_id
 * @param buffer
 * @return  0 Success <0 Error
 */
herr_t H5_BufferSync(char* filename, char* dataset_name, int rank_, hsize_t *dims, hsize_t *max_dims, void* dataset_object, hid_t vol_id);
/**
 * Cleans Up Buffer Libraries data structure
 * @return 0 Success <0 Error
 */
herr_t H5_CleanBuffer();
/**
 * Intercept MPI init calls
 */

int H5_Init();

int H5_Finalize();


#ifdef __cplusplus
}
#endif
#endif //HERMES_HERMES_H
