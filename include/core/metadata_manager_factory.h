/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
 * <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of HCompress
 * 
 * HCompress is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
//
// Created by hariharan on 2/21/19.
//

#ifndef HERMES_PROJECT_METADATA_MANAGER_FACTORY_H
#define HERMES_PROJECT_METADATA_MANAGER_FACTORY_H

#include <common/data_structures.h>
#include <core/interfaces/metadata_manager.h>
#include <debug.h>
#include <core/hdf5_impl/hdf5_metadata_manager.h>
#include <core/hdf5_impl/hdf5_metadata_manager_opt.h>

template <typename I,
        typename std::enable_if<std::is_base_of<Input, I>::value>::type* = nullptr>
class MetadataManagerFactory {
public:

    std::shared_ptr<MetadataManager<I>> GetMDM(MDMType type){
        AutoTrace trace = AutoTrace("IOClientFactory::GetClient",type);
        switch(type){
            case MDMType::MDM_FILE_PER_PROCESS:{
                return Singleton<HDF5MetadataManager>::GetInstance();
            }
            case MDMType::MDM_OPT:{
                return Singleton<HDF5MetadataManagerOpt>::GetInstance();
            }
        }
    }
};
#endif //HERMES_PROJECT_METADATA_MANAGER_FACTORY_H
