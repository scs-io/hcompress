/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
 * <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of HCompress
 * 
 * HCompress is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
//
// Created by HariharanDevarajan on 2/15/2019.
//

#ifndef HERMES_PROJECT_GRAPH_MANAGER_FACTORY_H
#define HERMES_PROJECT_GRAPH_MANAGER_FACTORY_H

#include <core/hdf5_impl/prefetcher/hdf5_graph_manager.h>
#include <common/data_structures.h>
#include <common/enumerations.h>
#include <common/singleton.h>
#include <core/interfaces/prefetcher/graph_manager.h>

template <typename I,typename E,typename G
, typename std::enable_if<std::is_base_of<GraphNode<I>, G>::value>::type* = nullptr
, typename std::enable_if<std::is_base_of<Input, I>::value>::type* = nullptr
, typename std::enable_if<std::is_base_of < Event < I>, E>::value>::type* = nullptr>
class GraphManagerFactory{
public:
std::shared_ptr<GraphManager<I,E,G>> getGraphManager(GraphManagerType type){
    switch(type){
        case GraphManagerType::HDF5_GRAPH_MANAGER:{
            return Singleton<HDF5GraphManager>::GetInstance();
        }
        default:{
            break;
        }
    }
}
};
#endif //HERMES_PROJECT_GRAPH_MANAGER_FACTORY_H
