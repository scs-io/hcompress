/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
 * <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of HCompress
 * 
 * HCompress is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
//
// Created by HariharanDevarajan on 2/7/2019.
//

#ifndef HERMES_PROJECT_PREFETCH_DATA_PLACEMENT_ENGINE_H
#define HERMES_PROJECT_PREFETCH_DATA_PLACEMENT_ENGINE_H

#include <common/data_structures.h>
template <typename I,typename O, typename E
        , typename std::enable_if<std::is_base_of < Event < I>, E>::value>::type* = nullptr
        , typename std::enable_if<std::is_base_of<Input, I>::value>::type* = nullptr
        , typename std::enable_if<std::is_base_of<Output, O>::value>::type* = nullptr>
class PrefetchDataPlacementEngine{
public:
    virtual vector<Placement<I,O>> place(I input) = 0;
};
#endif //HERMES_PROJECT_DATA_PLACEMENT_ENGINE_H
