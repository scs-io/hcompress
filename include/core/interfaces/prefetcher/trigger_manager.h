/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
 * <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of HCompress
 * 
 * HCompress is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
//
// Created by hariharan on 2/25/19.
//

#ifndef HERMES_PROJECT_TRIGGER_MANAGER_H
#define HERMES_PROJECT_TRIGGER_MANAGER_H

#include <common/data_structures.h>
#include <basket/priority_queue/priority_queue.h>
#include <core/metadata_manager_factory.h>
#include <core/hdf5_impl/hdf5_data_organizer.h>
#include <basket/clock/global_clock.h>
#include <core/interfaces/prefetcher/data_placement_engine.h>
#include <core/interfaces/prefetcher/data_placement_engine_factory.h>

template <typename I,typename O,typename E,typename G
        , typename std::enable_if<std::is_base_of<GraphNode<I>, G>::value>::type* = nullptr
    , typename std::enable_if<std::is_base_of<Input, I>::value>::type* = nullptr
    , typename std::enable_if<std::is_base_of<Output, O>::value>::type* = nullptr
    , typename std::enable_if<std::is_base_of < Event < I>, E>::value>::type* = nullptr>
class TriggerManager{
private:
    size_t default_server;
    int rank;
    basket::priority_queue<pair<t_mili,E>,PairCompare> queue;
    std::shared_ptr<PrefetchDataPlacementEngine<I,O,E>> dataPlacementEngine;
    std::shared_ptr<DataOrganizer<I,O>> data_organizer;

    std::shared_ptr<IOClientFactory<I,O>> io_factory;
    bool isInitialized;
    std::shared_ptr<basket::global_clock> clock;
public:
    TriggerManager():queue("TRIGGER_QUEUE",HERMES_CONF->IS_SERVER,HERMES_CONF->MY_SERVER,HERMES_CONF->NUM_SERVERS,HERMES_CONF->SERVER_ON_NODE),isInitialized(false){
        clock=Singleton<basket::global_clock>::GetInstance();
        MPI_Comm_rank(MPI_COMM_WORLD, &rank);
        default_server=(size_t)rank/HERMES_CONF->RANKS_PER_SERVER;
        dataPlacementEngine=Singleton<DataPlacementEngineFactory<I,O,E>>::GetInstance()->getDataPlacementEngine(CURRENT_DATA_PLACEMENT_ENGINE_TYPE);
        io_factory=Singleton<IOClientFactory<I,O>>::GetInstance();/* Initialize Buffers*/
    }
    void Init(InterfaceType interface_type) {
        if (!isInitialized) {
            switch (interface_type) {
                /* when interface used is HDF5 */
                case InterfaceType::HDF5: {
                    data_organizer=Singleton<HDF5DataOrganizer>::GetInstance();
                }
            }
            isInitialized=true;
        }
    }
    PrefetchStatus handle(E event){
        std::vector<std::pair<I,I>> buffered_datas = Singleton<MetadataManagerFactory<I>>::GetInstance()->GetMDM(MDM_TYPE)->FindBufferedData(event.input);
        //printf("found %d data\n",buffered_datas.size());
        for(auto buffered_data:buffered_datas){
            buffered_data.second.memory_start_=buffered_data.second.file_start_;
            buffered_data.second.memory_dim_=buffered_data.second.file_end_;
            vector<Placement<I,O>> ideal_placements = dataPlacementEngine->place(buffered_data.second);
            //printf("placing %d data\n",ideal_placements.size());
            O main_output;
            for(Placement<I,O> placement:ideal_placements){
                if(buffered_data.second.layer != placement.destination_info.layer){
                    //printf("Prefetch move data from %s to %s\n",buffered_data.second.layer.layer_loc.c_str(),placement.destination_info.layer.layer_loc.c_str());
                    /* move data to next layer*/
                    HDF5Output output = data_organizer->Move(placement.source_info,placement.destination_info);
                    if(placement.source_info.layer==*Layer::LAST){
                        Singleton<MetadataManagerFactory<HDF5Input>>::GetInstance()->GetMDM(MDM_TYPE)->UpdateOnWrite(buffered_data.second,placement.destination_info);
                    }else{
                        Singleton<MetadataManagerFactory<HDF5Input>>::GetInstance()->GetMDM(MDM_TYPE)->UpdateLayer(placement.source_info,placement.destination_info.layer);
                        if(!Singleton<MetadataManagerFactory<HDF5Input>>::GetInstance()->GetMDM(MDM_TYPE)->HasChunk(placement.source_info,placement.destination_info.layer)){
                            io_factory->GetClient(placement.source_info.layer.io_client_type)->Delete(placement.source_info);
                        }
                    }
                }else{
                    //printf("Prefetch not moving data from %s to %s\n",buffered_data.second.layer.layer_loc.c_str(),placement.destination_info.layer.layer_loc.c_str());
                }
            }
        }
        return PrefetchStatus::PREFETCH_SUCCESS;
    }
    PrefetchStatus publish(vector<pair<t_mili,E>> events, uint32_t server=-1){
        if(server==-1) server=default_server;
        bool status=true;
        for(pair<t_mili,E> event:events){
            status = status && queue.Push(event,server);
        }
        return PrefetchStatus::PREFETCH_SUCCESS;

    }

    bool subscribe(E &event, uint32_t server=-1){
        if(server==-1) server=default_server;
        bool status=true;
        auto result = queue.Top(server);
        if(result.first && clock->GetTime() >= result.second.first){
            auto result = queue.Pop(server);
            event = result.second.second;
            return true;
        }
        else return false;
    }

    bool hasEvents(uint32_t server=-1){
        if(server==-1) server=default_server;
        bool status=true;
        auto result = queue.Size(server);
        return result > 0;
    }

};
#endif //HERMES_PROJECT_TRIGGER_MANAGER_H
