/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
 * <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of HCompress
 * 
 * HCompress is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
//
// Created by HariharanDevarajan on 2/7/2019.
//

#ifndef HERMES_PROJECT_ACTION_ACCUMULATOR_H
#define HERMES_PROJECT_ACTION_ACCUMULATOR_H


#include <common/constants.h>
#include <core/hdf5_impl/hdf5_data_organizer.h>
#include <core/metadata_manager_factory.h>
#include <common/singleton.h>
#include <core/io_client_factory.h>
#include <core/interfaces/prefetcher/graph_manager_factory.h>
#include <core/interfaces/prefetcher/prediction_engine_factory.h>
#include <core/interfaces/prefetcher/graph_manager.h>
#include <core/interfaces/prefetcher/prediction_engine.h>
#include <core/interfaces/prefetcher/trigger_manager.h>

template <typename I,typename O,typename E,typename G
        , typename std::enable_if<std::is_base_of<GraphNode<I>, G>::value>::type* = nullptr
        , typename std::enable_if<std::is_base_of<Input, I>::value>::type* = nullptr
        , typename std::enable_if<std::is_base_of<Output, O>::value>::type* = nullptr
        , typename std::enable_if<std::is_base_of < Event < I>, E>::value>::type* = nullptr>
class EventManager{
private:
    std::shared_ptr<GraphManager<I,E,G>> graphManager;
    std::shared_ptr<PredictionEngine<I,E,G>> predictionEngine;
public:
    EventManager(){
        switch(PREDICTION_MODE){
            case PredictionMode::GRAPH:{
                graphManager=Singleton<GraphManagerFactory<I,E,G>>::GetInstance()->getGraphManager(CURRENT_GRAPH_MANAGER_TYPE);
                graphManager->load();
                break;
            }
            case PredictionMode::PATTERN_PREDICTOR:{
                predictionEngine=Singleton<PredictionEngineFactory<I,E,G>>::GetInstance()->getPredictionEngine(CURRENT_PREDICTION_ENGINE_TYPE);
                break;
            }
            default:{
                break;
            };
        }
    }

    PrefetchStatus handle(vector<E> events){
        vector<pair<t_mili,E>> triggers;
        switch(PREDICTION_MODE){
            case PredictionMode::GRAPH:{
                graphManager->enhance(events);
                triggers = graphManager->getNextTriggers();
                break;
            }
            case PredictionMode::PATTERN_PREDICTOR:{
                // triggers = predictionEngine->predict(events);
                break;
            }
            default:{
                break;
            };
        }
        Singleton<TriggerManager<I,O,E,G>>::GetInstance()->publish(triggers);
        return PrefetchStatus::PREFETCH_SUCCESS;
    }
    PrefetchStatus finalize(){
        switch(PREDICTION_MODE){
            case PredictionMode::GRAPH: {
                graphManager->store();
                break;
            }
            case PredictionMode::PATTERN_PREDICTOR:{
                break;
            }
            default:{
                break;
            };
        }
        return PrefetchStatus::PREFETCH_SUCCESS;
    }
};

#endif //HERMES_PROJECT_ACTION_ACCUMULATOR_H
