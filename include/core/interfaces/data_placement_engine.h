/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
 * <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of HCompress
 * 
 * HCompress is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
/*-------------------------------------------------------------------------
*
* Created: data_placement_engine.h
* June 5 2018
* Hariharan Devarajan <hdevarajan@hdfgroup.org>
*
* Purpose: It is the interface for all Data Placement Engines defined
* in Hermes.
*
*-------------------------------------------------------------------------
*/
#ifndef HERMES_DATA_PLACEMENT_ENGINE_H
#define HERMES_DATA_PLACEMENT_ENGINE_H


#include <vector>
#include <common/data_structures.h>



template <typename IR,typename IW, typename I,typename O,
        typename std::enable_if<std::is_base_of<ReadInput, IR>::value>::type* = nullptr,
        typename std::enable_if<std::is_base_of<WriteInput, IW>::value>::type* = nullptr,
        typename std::enable_if<std::is_base_of<Output, O>::value>::type* = nullptr,
        typename std::enable_if<std::is_base_of<Input, I>::value>::type* = nullptr>
class DataPlacementEngine {
public:
    /**
     * Virtual Methods
     */

    /**
     * Identifies placement based on policy for data to be written.
     *
     * @param input
     * @return vector of placements
     */
    virtual std::vector<Placement<I,O>> PlaceWriteData(IW input)=0;

    /**
     * Identifies where data is placed in DMSH.
     *
     * @param input
     * @return vector of placements
     */
    virtual std::vector<Placement<I,O>> PlaceReadData(IR input)=0;
};


#endif //HERMES_DATA_PLACEMENT_ENGINE_H
