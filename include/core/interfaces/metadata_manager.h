/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
 * <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of HCompress
 * 
 * HCompress is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
/*-------------------------------------------------------------------------
*
* Created: metadata_manager.h
* June 5 2018
* Hariharan Devarajan <hdevarajan@hdfgroup.org>
*
* Purpose: It is the interface for all Metadata Managers defined
* in Hermes.
*
*-------------------------------------------------------------------------
*/

#ifndef HERMES_METADATA_MANAGER_H
#define HERMES_METADATA_MANAGER_H

#include <functional>
#include <vector>
#include <string>
#include <common/data_structures.h>
#include <common/configuration_manager.h>





template<typename I, typename std::enable_if<std::is_base_of<Input, I>::value>::type * = nullptr>
class MetadataManager {
private:

public:

    /**
     * Initialize Hermes selection engine.
     *
     * [type_id][comp_lib_id][0] = [c1]
     * [type_id][comp_lib_id][1] = [c2]
     * [type_id][comp_lib_id][2] = [c3]
     */

    MetadataManager() {
    }





    /**
     * Virtual Methods
     */
    /**
     * This methods updates the metadata on initialization (i.e. Open or Create)
     *
     * @param source
     * @return status of operation success 0, failure < 0
     */
    virtual int UpdateOnInit(I source) = 0;

    /**
     * This methods updates the metadata on read
     *
     * @param source
     * @param destination
     * @return status of operation success 0, failure < 0
     */
    virtual int UpdateOnRead(I source, I destination) = 0;

    /**
     * This methods updates the metadata on write
     *
     * @param source
     * @param destination
     * @return status of operation success 0, failure < 0
     */
    virtual int UpdateOnWrite(I source, I destination) = 0;

    /**
     * This methods updates the metadata on Sync of data
     *
     * @param source
     * @return status of operation success 0, failure < 0
     */
    virtual int UpdateOnSync(I source) = 0;

    /**
     * This method finds where the given data is buffered in the DMSH
     *
     * @param source
     * @return vector of sources and destination of buffered data
     */
    virtual std::vector<std::pair<I, I>> FindBufferedData(I source) = 0;

    /**
     * This method fetches all the data is buffered in the DMSH.
     *
     * @param source
     * @return vector of sources and destination of buffered data
     */
    virtual std::vector<std::pair<I, I>> GetAllBufferedData(I source) = 0;

    /**
     * This method fetches which data should be evicted to make more space (remaining_capacity) in the layer.
     *
     * @param source
     * @param layer
     * @param remaining_capacity
     * @return vector of destination of buffered data
     */
    virtual std::vector<I> GetBufferedDataToEvict(I source, Layer layer, float remaining_capacity) = 0;

    /**
     * Updates the buffered data's layer location. Typically used when we move data between layers.
     *
     * @param source
     * @param layer
     * @return status of operation success 0, failure < 0
     */
    virtual int UpdateLayer(I source, Layer layer) = 0;

    /**
     * Generates a unique filename for the data.
     *
     * @return filename of buffered data
     */
    virtual std::string GenerateBufferFilename() = 0;

    /**
    * Check if file has some pieces left
    *
    * @param buf_source
    * @return True or False
    */
    virtual bool HasChunk(HDF5Input buf_source, Layer layer) = 0;
};


#endif //HERMES_METADATA_MANAGER_H
