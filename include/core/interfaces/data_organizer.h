/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
 * <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of HCompress
 * 
 * HCompress is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
/*-------------------------------------------------------------------------
*
* Created: data_organizer.h
* June 5 2018
* Hariharan Devarajan <hdevarajan@hdfgroup.org>
*
* Purpose: It is the interface for all Data Organizers defined
* in Hermes.
*
*-------------------------------------------------------------------------
*/
#ifndef HERMES_DATA_ORGANIZER_H
#define HERMES_DATA_ORGANIZER_H


#include <common/data_structures.h>

template <typename I, typename O, typename std::enable_if<std::is_base_of<Input, I>::value>::type* = nullptr,
         typename std::enable_if<std::is_base_of<Output, O>::value>::type* = nullptr>
class DataOrganizer {
public:
    /**
     * Virtual Methods
     */

    /**
     * Method moves data from source to destination.
     *
     * @param source
     * @param destination
     * @return output
     */
    virtual O Move(I source, I destination)=0;

    /**
     * Methods checks if a layer has capacity to accomodate the given source of data.
     *
     * @param source
     * @param layer
     * @return true if layer can fit source else false.
     */
    virtual bool HasCapacity(I source, Layer layer)=0;

    /**
     * Makes capacity required to fit the source in the layer
     *
     * @param source
     * @param layer
     * @return output
     */
    virtual O MakeCapacity(I source, Layer layer)=0;

    /**
     * Checks if a layer can fit the given data.
     *
     * @param source
     * @param layer
     * @return true if layer can fit source else false.
     */
    virtual bool CanFit(I source, Layer layer)=0;

    /**
     * Initilizes Buffer
     *
     * @param source
     * @return output
     */
    virtual O BufferInit(I source)=0;

    /**
     * Flushes Buffers for a given source.
     *
     * @param source
     * @return output.
     */
    virtual O BufferFlush(I source)=0;


};


#endif //HERMES_DATA_ORGANIZER_H
