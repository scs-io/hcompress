/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
 * <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of HCompress
 * 
 * HCompress is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
/*-------------------------------------------------------------------------
*
* Created: io_client.h
* June 5 2018
* Hariharan Devarajan <hdevarajan@hdfgroup.org>
*
* Purpose: It is the interface for all IO Clients defined
* in Hermes.
*
*-------------------------------------------------------------------------
*/

#ifndef HERMES_IO_CLIENT_H
#define HERMES_IO_CLIENT_H

#include <common/data_structures.h>

template <typename I,typename O,
        typename std::enable_if<std::is_base_of<Input, I>::value>::type* = nullptr,
        typename std::enable_if<std::is_base_of<Output, O>::value>::type* = nullptr>
class IOClient {
public:
    /**
     * Virtual Methods
     */
     /**
      * Creates a file using input if it doesnt exists.
      *
      * @param input
      * @return output
      */
    virtual O Create(I input)=0;

    /**
     * Reads data from source file into destination (memory)
     *
     * @param source
     * @param destination
     * @return read_output
     */
    virtual O Read(I source, I destination)=0;

    /**
     * Writes data to a destination file from source (memory)
     *
     * @param input
     * @param destination
     * @return write_output
     */
    virtual O Write(I input, I destination)=0;

    /**
     * Deletes the file specified by input.
     *
     * @param input
     * @return output
     */
    virtual O Delete(I input)=0;

    /*
     * TODO:
     *
     * Input compress_input(I input)
     * returns compressed input with type = string and new size.
     * Input decompress_input(I input)
     * */

    /**
     * Get Capacity of the current layer
     *
     * @param layer
     * @return
     */
    virtual uint64_t GetCurrentCapacity(Layer layer) = 0;
};


#endif //HERMES_IO_CLIENT_H
