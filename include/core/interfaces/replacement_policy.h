/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
 * <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of HCompress
 * 
 * HCompress is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
/*-------------------------------------------------------------------------
*
* Created: replacement_policy.h
* June 5 2018
* Hariharan Devarajan <hdevarajan@hdfgroup.org>
*
* Purpose: It is the interface for all Cache Replacement Policies defined
* in Hermes.
*
*-------------------------------------------------------------------------
*/

#ifndef HERMES_REPLACEMENT_POLICY_H
#define HERMES_REPLACEMENT_POLICY_H

#include <common/data_structures.h>

template <typename I,typename D,
        typename std::enable_if<std::is_base_of<Input, I>::value>::type* = nullptr,
        typename std::enable_if<std::is_base_of<Data, D>::value>::type* = nullptr>
class ReplacementPolicy{
public:
    /**
     * Virtual Methods
     */
    /**
    * Inserts a new data and input into cache-line.
    *
    * @param data
    * @param input
    * @return status of code. success 0 failure < 0
    */
    virtual int Insert(D data, I input) = 0;

    /**
    * Evicts LRU data from layer.
    *
    * @param layer
    * @return evicted pair of data and input.
    */
    virtual std::pair<D, I> Evict(Layer layer) = 0;

    /**
    * Deletes cache-line for a given Data.
    *
    * @param data
    * @return deleted pair of data and Input
    */
    virtual std::pair<D, I> Delete(D data) = 0;

};
#endif //HERMES_REPLACEMENT_POLICY_H
