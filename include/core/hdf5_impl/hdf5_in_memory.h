/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
 * <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of HCompress
 * 
 * HCompress is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
/*-------------------------------------------------------------------------
*
* Created: hdf5_in_memory.h
* June 5 2018
* Hariharan Devarajan <hdevarajan@hdfgroup.org>
*
* Purpose: It is the implementation of io_client on hdf5 file format.
*
*-------------------------------------------------------------------------
*/

#ifndef HERMES_PROJECT_HDF5_IN_MEMORY_H
#define HERMES_PROJECT_HDF5_IN_MEMORY_H


#include <unordered_map>
#include <map>
#include <unordered_set>
#include <basket/unordered_map/unordered_map.h>
#include <common/data_structures.h>
#include <common/configuration_manager.h>
#include <core/interfaces/io_client.h>
#include <hdf5_hl.h>

class HDF5InMemory: public IOClient<HDF5Input,HDF5Output> {
    typedef bip::allocator<char, bip::managed_shared_memory::segment_manager> CharAllocator;
    typedef boost::interprocess::basic_string<char, std::char_traits<char>, CharAllocator> MyShmString;
    typedef boost::interprocess::allocator<MyShmString, boost::interprocess::managed_shared_memory::segment_manager> StringAllocator;

    basket::unordered_map<CharStruct,FileInfo> existing_file;
    hid_t load(CharStruct name){
        hid_t fileId=-1;
        auto result = existing_file.Get(name);
        if(result.first){
            auto info = result.second;
            std::hash<std::string> stringHash;
            std::string hash_name=std::to_string(stringHash(std::string(name.data())));
            auto shm = new bip::managed_shared_memory(bip::open_only, name.c_str());
            MyShmString *str = shm->find<MyShmString>("myShmString").first;
            unsigned flags = H5LT_FILE_IMAGE_DONT_COPY | H5LT_FILE_IMAGE_DONT_RELEASE;
            fileId= H5LTopen_file_image(&str->front(), info.size, flags);
        }
        return fileId;
    }
    void store(CharStruct name, hid_t buf_file,size_t size){
        H5Fflush(buf_file,H5F_SCOPE_GLOBAL);
        ssize_t buf_size = H5Fget_file_image(buf_file, NULL, 0);
        void* buffer_ptr = malloc(buf_size);
        H5Fget_file_image(buf_file, buffer_ptr, buf_size);
        unsigned flags = H5LT_FILE_IMAGE_DONT_COPY | H5LT_FILE_IMAGE_DONT_RELEASE;
        //H5LTopen_file_image(buffer_ptr, buf_size, flags);
        bip::shared_memory_object::remove(name.c_str());
        auto shm = new bip::managed_shared_memory(bip::create_only,name.c_str() , buf_size+1024);
        CharAllocator charallocator(shm->get_segment_manager());
        MyShmString *myShmString = shm->construct<MyShmString>("myShmString")(charallocator);
        myShmString->assign((char*)buffer_ptr, buf_size);
        FileInfo info;
        info.size=buf_size;
        H5LTopen_file_image(&myShmString->front(), info.size, flags);
        existing_file.Put(name,info);
    }
public:
    /**
     * Constructor
     */
    HDF5InMemory():existing_file("IN_MEMORY_HDF5",HERMES_CONF->IS_SERVER,HERMES_CONF->MY_SERVER,HERMES_CONF->NUM_SERVERS,HERMES_CONF->SERVER_ON_NODE){} /* default constructor */
    /**
     * Methods
     */

    /**
     * Reads data from source memory placeholder into destination (memory)
     *
     * @param source
     * @param destination
     * @return read_output
     */
    HDF5Output Read(HDF5Input source, HDF5Input destination) override;

    /**
     * Writes data to a destination memory placeholder from source (memory)
     *
     * @param input
     * @param destination
     * @return write_output
     */
    HDF5Output Write(HDF5Input input, HDF5Input destination) override;

    /**
     * Deletes the memory placeholder specified by input.
     *
     * @param input
     * @return output
     */
    HDF5Output Delete(HDF5Input input) override;

    /**
     * Creates a memory placeholder using input if it doesnt exists.
     *
     * @param input
     * @return output
     */
    HDF5Output Create(HDF5Input input) override;

    /**
     * Get Capacity of the current layer
     *
     * @param layer
     * @return
     */
    uint64_t GetCurrentCapacity(Layer layer) override;
private:
    inline bool FileExists (const CharStruct& name) {
        auto iter=existing_file.Get(name);
        return iter.first;
    }
};


#endif //HERMES_PROJECT_HDF5_IN_MEMORY_H
