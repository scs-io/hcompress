/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
 * <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of HCompress
 * 
 * HCompress is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
/*-------------------------------------------------------------------------
*
* Created: hdf5_in_memory.h
* June 5 2018
* Hariharan Devarajan <hdevarajan@hdfgroup.org>
*
* Purpose: It is the implementation of io_client on hdf5 file format.
*
*-------------------------------------------------------------------------
*/

#ifndef HERMES_PROJECT_HDF5_MEMORY_H
#define HERMES_PROJECT_HDF5_MEMORY_H


#include <unordered_map>
#include <map>
#include <unordered_set>
#include <common/distributed_ds/hashmap/DistributedHashMap.h>
#include <common/distributed_ds/map/DistributedMap.h>
#include <common/data_structures.h>
#include <core/interfaces/io_client.h>

class HDF5Memory: public IOClient<HDF5Input,HDF5Output> {

    DistributedHashMap<std::string,FileInfo> existing_file;
public:
    /**
     * Constructor
     */
    HDF5Memory():existing_file("EXISTING_FILE_MEMORY"){
    } /* default constructor */
    /**
     * Methods
     */

    /**
     * Reads data from source memory placeholder into destination (memory)
     *
     * @param source
     * @param destination
     * @return read_output
     */
    HDF5Output Read(HDF5Input source, HDF5Input destination) override;

    /**
     * Writes data to a destination memory placeholder from source (memory)
     *
     * @param input
     * @param destination
     * @return write_output
     */
    HDF5Output Write(HDF5Input input, HDF5Input destination) override;

    /**
     * Deletes the memory placeholder specified by input.
     *
     * @param input
     * @return output
     */
    HDF5Output Delete(HDF5Input input) override;

    /**
     * Creates a memory placeholder using input if it doesnt exists.
     *
     * @param input
     * @return output
     */
    HDF5Output Create(HDF5Input input) override;

    /**
     * Get Capacity of the current layer
     *
     * @param layer
     * @return
     */
    int64_t GetCurrentCapacity(Layer layer) override;
private:
    inline bool FileExists (const std::string& name) {
        auto iter=existing_file.Get(name);
        return iter.first;
    }
};


#endif //HERMES_PROJECT_HDF5_IN_MEMORY_H
