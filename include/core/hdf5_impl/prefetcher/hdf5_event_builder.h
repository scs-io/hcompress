/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
 * <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of HCompress
 * 
 * HCompress is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
//
// Created by HariharanDevarajan on 2/7/2019.
//

#ifndef HERMES_PROJECT_HDF5_ACTION_BUILDER_H
#define HERMES_PROJECT_HDF5_ACTION_BUILDER_H


#include <common/data_structures.h>
#include <core/interfaces/prefetcher/event_builder.h>
#include <basket/sequencer/global_sequence.h>
#include <common/constants.h>

class HDF5EventBuilder: public EventBuilder<HDF5Input,HDF5Event> {
    basket::global_sequence globalSequence;
    int rank;
public:
    HDF5EventBuilder():globalSequence("EVENT_SEQUENCE",HERMES_CONF->IS_SERVER,HERMES_CONF->MY_SERVER,HERMES_CONF->NUM_SERVERS,HERMES_CONF->SERVER_ON_NODE){
        MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    }
    vector<HDF5Event> build(HDF5Input input, EventType type, uint64_t sequenceId, bool is_end) override;
};


#endif //HERMES_PROJECT_HDF5_ACTION_BUILDER_H
