/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
 * <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of HCompress
 * 
 * HCompress is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
//
// Created by HariharanDevarajan on 2/15/2019.
//

#ifndef PROJECT_HDF5_PREFETCH_MAX_PERFORMANCE_DPE_H
#define PROJECT_HDF5_PREFETCH_MAX_PERFORMANCE_DPE_H


#include <common/data_structures.h>
#include <core/interfaces/prefetcher/data_placement_engine.h>
#include <core/hdf5_impl/hdf5_data_organizer.h>

class PrefetchHDF5MaxPerformanceDataPlacementEngine: public PrefetchDataPlacementEngine<HDF5Input,HDF5Output,HDF5Event> {
private:
    std::vector<Placement<HDF5Input, HDF5Output>> CalculatePlacementOptimally(HDF5Input input,Layer layer, const Matrix &buffer_dims);
    float CalculatePlacementScore(HDF5Input input, Layer layer);
    float CalculateMovingScore(HDF5Input input, Layer source_layer, Layer destination_layer);
    Placement<HDF5Input, HDF5Output> BuildPlacement(HDF5Input input,Layer layer, bool move_required, bool generate_name,  const Matrix &buffer_dims);
    std::vector<HDF5Input> SplitInput(HDF5Input input, uint64_t capacity_);
public:
    vector<Placement<HDF5Input,HDF5Output>> place(HDF5Input input) override;
};


#endif //PROJECT_HDF5_MAX_PERFORMANCE_DPE_H
