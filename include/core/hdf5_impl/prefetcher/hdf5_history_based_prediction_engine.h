/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
 * <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of HCompress
 * 
 * HCompress is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
//
// Created by HariharanDevarajan on 2/15/2019.
//

#ifndef HERMES_PROJECT_HDF5HISTORYBASEDPREDICTIONENGINE_H
#define HERMES_PROJECT_HDF5HISTORYBASEDPREDICTIONENGINE_H

#include <common/constants.h>
#include <common/data_structures.h>
#include <core/interfaces/prefetcher/prediction_engine.h>
#include <common/configuration_manager.h>

class HDF5HistoryBasedPredictionEngine: public PredictionEngine<HDF5Input,HDF5Event,HDF5GraphNode> {
private:
    unordered_map<uint64_t, HDF5Input> file_map;
    typedef pair<t_mili, t_mili> timespan;
    vector<unordered_map<uint64_t, vector<pair<Matrix, timespan>>>> pattern;
    // each element in the array is an MPI rank, vectors are so we can have multiple files, and the set is for deltas within file as accessed at certain times.

    vector<pair<uint64_t,size_t>> current_pointers;
public:
    void load() override;
    void enhance(vector<HDF5Event> events) override;
    vector<pair<t_mili,HDF5Event>> getNextTriggers() override;
    void store() override;

    HDF5HistoryBasedPredictionEngine(){
        for(int i = 0; i < HERMES_CONF->RANKS_PER_SERVER; ++i) {
            current_pointers.push_back(std::pair<uint64_t,size_t>());
            pattern.push_back(unordered_map<uint64_t, vector<pair<Matrix, timespan>>>());
        }
    }
};


#endif //HERMES_PROJECT_HDF5HISTORYBASEDPREDICTIONENGINE_H
