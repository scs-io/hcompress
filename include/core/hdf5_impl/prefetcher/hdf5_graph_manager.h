/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
 * <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of HCompress
 * 
 * HCompress is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
//
// Created by HariharanDevarajan on 2/15/2019.
//

#ifndef HERMES_PROJECT_HDF5_GRAPH_MANAGER_H
#define HERMES_PROJECT_HDF5_GRAPH_MANAGER_H

#include <common/data_structures.h>
#include <common/constants.h>
#include <common/configuration_manager.h>
#include <core/interfaces/prefetcher/graph_manager.h>
#include <iostream>
#include <vector>
#include <string>
#include <dirent.h>
#include <fstream>
#include <exception>
#include <rpc/msgpack.hpp>

namespace msgpk=clmdep_msgpack;

using namespace std;

class HDF5GraphManager: public GraphManager<HDF5Input,HDF5Event,HDF5GraphNode> {
private:
    std::string graph_file;
    bool is_initialized;
    int my_rank,comm_size,server_index;
    RootGraphNode root_node;
    unordered_map<size_t,size_t> current_nodes_index;
    std::unordered_map<size_t,std::shared_ptr<HDF5GraphNode>> nodes;
    std::unordered_set<size_t> scheduled_events;
    vector<size_t> findReadLink(size_t link);
public:
    HDF5GraphManager():root_node(),current_nodes_index(),nodes(),is_initialized(false),graph_file(APPLICATION_GRAPH_PATH+APPLICATION_GRAPH_NAME){
        MPI_Comm_size(MPI_COMM_WORLD,&comm_size);
        MPI_Comm_rank(MPI_COMM_WORLD,&my_rank);
        server_index = static_cast<uint16_t>(my_rank / HERMES_CONF->RANKS_PER_SERVER);
        graph_file = APPLICATION_GRAPH_PATH+APPLICATION_GRAPH_NAME+"_"+std::to_string(server_index);
    }
    RootGraphNode load() override;
    vector<std::shared_ptr<HDF5GraphNode>> enhance(vector<HDF5Event> events) override;
    vector<pair<t_mili,HDF5Event>> getNextTriggers() override;
    void store() override;
    RootGraphNode fetch() override;

};


#endif //HERMES_PROJECT_HDF5_GRAPH_MANAGER_H
