/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
 * <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of HCompress
 * 
 * HCompress is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
/*-------------------------------------------------------------------------
*
* Created: hdf5_metdata_manager.h
* June 5 2018
* Hariharan Devarajan <hdevarajan@hdfgroup.org>
*
* Purpose: It is the implementation of metadata manager on hdf5 file format.
*
*-------------------------------------------------------------------------
*/

#ifndef HERMES_HDF5_METADATA_MANAGER_H
#define HERMES_HDF5_METADATA_MANAGER_H


#include <unordered_map>
#include <map>
#include <memory>
#include <common/data_structures.h>
#include <core/interfaces/metadata_manager.h>
#include <core/cache_manager.h>
#include <sys/time.h>
#include <basket/unordered_map/unordered_map.h>
#include <basket/map/map.h>

using namespace std;
class HDF5MetadataManager: public MetadataManager<HDF5Input> {
private:
    uint8_t CRC8(const char *data, uint8_t len) {
        uint8_t crc = 0;
        while (len--) {
            char extract = *data++;
            for (uint8_t tempI = 8; tempI; tempI--) {
                uint8_t sum = (crc ^ extract) & 0x01;
                crc >>= 1;
                if (sum) {
                    crc ^= 0x8C;
                }
                extract >>= 1;
            }
        }
        return crc;
    }
    /**
     * Attributes
     */
    basket::unordered_map<CharStruct,HDF5Input> file_meta_map; /* file metadata map*/
    basket::unordered_map<CharStruct, std::map<Matrix,HDF5Input,MapCompare>> valid_buffered_dataset; /* map of file+dataset -> {Matrix,Data}*/
    basket::unordered_map<CharStruct, CharStruct> buffer_file_actual_map; /* buffered filename to actual map*/
    //unordered_map<std::string,HDF5Input> file_meta_map; /* file metadata map*/
    //unordered_map<std::string, std::map<Matrix,HDF5Input,MapCompare>> valid_buffered_dataset; /* map of file+dataset -> {Matrix,Data}*/
    //unordered_map<std::string, std::string> buffer_file_actual_map; /* buffered filename to actual map*/
    bool Put(std::map<Matrix,HDF5Input,MapCompare> &offsets, Matrix key, HDF5Input data){

        std::map<Matrix,HDF5Input,MapCompare>::iterator iterator = offsets.find(key);
        if (iterator != offsets.end()) {
            iterator->second=data;
        }else{
            auto val=offsets.insert(std::pair<Matrix,HDF5Input>(key, data));
            return val.second;
        }
        return true;

    }
    std::pair<bool,HDF5Input> Erase(std::map<Matrix,HDF5Input,MapCompare> &offsets, Matrix key) {

        std::map<Matrix,HDF5Input,MapCompare>::iterator iterator = offsets.find(key);
        if (iterator != offsets.end()) {
            offsets.erase(iterator);
            return std::pair<bool, HDF5Input>(true, iterator->second);
        } else {
            return std::pair<bool, HDF5Input>(false, HDF5Input());
        }

    }
    std::vector<std::pair<Matrix,HDF5Input>> Contains(std::map<Matrix,HDF5Input,MapCompare> &offsets, Matrix key) {
        std::vector<std::pair<Matrix,HDF5Input>> final_values = std::vector<std::pair<Matrix,HDF5Input>>();
        {
            std::map<Matrix,HDF5Input,MapCompare>::iterator lower_bound;
            size_t size=offsets.size();
            if (size == 0){

            }
            else if (size == 1){
                lower_bound = offsets.begin();
                final_values.insert(final_values.end(),std::pair<Matrix,HDF5Input>(lower_bound->first, lower_bound->second));
            }
            else {
                Matrix k;
                lower_bound = offsets.lower_bound(key);
                if (lower_bound->first != key) {
                    if (lower_bound != offsets.begin()) {
                        --lower_bound;
                        if (!key.Contains(lower_bound->first)) lower_bound++;
                    }
                }
                k=lower_bound->first;
                if (key != lower_bound->first) {
                    while (lower_bound != offsets.end()) {
                        k=lower_bound->first;
                        if(!(key.Contains(lower_bound->first) || lower_bound->first.Contains(key))) break;
                        final_values.insert(final_values.end(),std::pair<Matrix,HDF5Input>(lower_bound->first, lower_bound->second));
                        lower_bound++;
                    }
                }
            }

        }
        return final_values;
    }
    std::vector<std::pair<Matrix,HDF5Input>> GetAllData(std::map<Matrix,HDF5Input,MapCompare> &offsets) {
        std::vector<std::pair<Matrix,HDF5Input>> final_values = std::vector<std::pair<Matrix,HDF5Input>>();
        {
            std::map<Matrix,HDF5Input,MapCompare>::iterator lower_bound;
            lower_bound = offsets.begin();
            while (lower_bound != offsets.end()){
                final_values.insert(final_values.end(),std::pair<Matrix,HDF5Input>(lower_bound->first, lower_bound->second));
                lower_bound++;
            }
        }
        return final_values;
    }

public:
    ~HDF5MetadataManager(){

    }
    /**
     * Constructors
     */
    HDF5MetadataManager():valid_buffered_dataset("BUFFERED_DATASET",HERMES_CONF->IS_SERVER,HERMES_CONF->MY_SERVER,HERMES_CONF->NUM_SERVERS,HERMES_CONF->SERVER_ON_NODE),file_meta_map("FILE_META_MAP",HERMES_CONF->IS_SERVER,HERMES_CONF->MY_SERVER,HERMES_CONF->NUM_SERVERS,HERMES_CONF->SERVER_ON_NODE),buffer_file_actual_map("BUFFER_FILE_ACTUAL_MAP",HERMES_CONF->IS_SERVER,HERMES_CONF->MY_SERVER,HERMES_CONF->NUM_SERVERS,HERMES_CONF->SERVER_ON_NODE){
    } /* default constructor */

    /**
     * Methods
     */

    /**
     * This methods updates the metadata on read
     *
     * @param source
     * @param destination
     * @return status of operation success 0, failure < 0
     */
    int UpdateOnRead(HDF5Input source, HDF5Input destination) override;

    /**
     * This methods updates the metadata on write
     *
     * @param source
     * @param destination
     * @return status of operation success 0, failure < 0
     */
    int UpdateOnWrite(HDF5Input source, HDF5Input destination) override;

    /**
     * This method finds where the given data is buffered in the DMSH
     *
     * @param source
     * @return vector of sources and destination of buffered data
     */
    std::vector<std::pair<HDF5Input,HDF5Input>> FindBufferedData(HDF5Input source) override;

    /**
     * This method fetches which data should be evicted to make more space (remaining_capacity) in the layer.
     *
     * @param source
     * @param layer
     * @param remaining_capacity
     * @return vector of destination of buffered data
     */
    vector<HDF5Input> GetBufferedDataToEvict(HDF5Input source, Layer layer,float remaining_capacity) override;

    /**
     * Generates a unique filename for the data.
     *
     * @return filename of buffered data
     */
    string GenerateBufferFilename() override;

    /**
     * This methods updates the metadata on initialization (i.e. Open or Create)
     *
     * @param source
     * @return status of operation success 0, failure < 0
     */
    int UpdateOnInit(HDF5Input source) override;

    /**
     * This methods updates the metadata on Sync of data
     *
     * @param source
     * @return status of operation success 0, failure < 0
     */
    int UpdateOnSync(HDF5Input source) override;

    /**
     * This method fetches all the data is buffered in the DMSH.
     *
     * @param source
     * @return vector of sources and destination of buffered data
     */
    vector<pair<HDF5Input,HDF5Input>> GetAllBufferedData(HDF5Input source) override;

    /**
     * Updates the buffered data's layer location. Typically used when we move data between layers.
     *
     * @param source
     * @param layer
     * @return status of operation success 0, failure < 0
     */
    int UpdateLayer(HDF5Input buf_source, Layer layer) override;
    /**
     * Check if file has some pieces left
     *
     * @param buf_source
     * @return True or False
     */
    bool HasChunk(HDF5Input buf_source, Layer layer) override;
};


#endif //HERMES_HDF5_METADATA_MANAGER_H
