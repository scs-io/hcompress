/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
 * <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of HCompress
 * 
 * HCompress is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
/*-------------------------------------------------------------------------
*
* Created: hdf5_client.h
* June 5 2018
* Hariharan Devarajan <hdevarajan@hdfgroup.org>
*
* Purpose: It is the implementation of io_client on hdf5 file format.
*
*-------------------------------------------------------------------------
*/

#ifndef HERMES_HDF5_CLIENT_H
#define HERMES_HDF5_CLIENT_H


#include <sys/stat.h>
#include <iostream>
#include <common/singleton.h>
#include <core/metadata_manager_factory.h>
#include <core/interfaces/io_client.h>

class HDF5Client: public IOClient<HDF5Input,HDF5Output> {
public:
    /**
     * Constructor
     */
    HDF5Client(){} /* default constructor */
    /**
     * Methods
     */

    /**
     * Reads data from source file into destination (memory)
     *
     * @param source
     * @param destination
     * @return read_output
     */
    HDF5Output Read(HDF5Input source,HDF5Input destination) override;

    /**
     * Writes data to a destination file from source (memory)
     *
     * @param input
     * @param destination
     * @return write_output
     */
    HDF5Output Write(HDF5Input source,HDF5Input destination) override;

    /**
     * Deletes the file specified by input.
     *
     * @param input
     * @return output
     */
    HDF5Output Delete(HDF5Input input) override;

    /**
     * Creates a file using input if it doesnt exists.
     *
     * @param input
     * @return output
     */
    HDF5Output Create(HDF5Input input) override;
    /**
     * Get Capacity of the current layer
     *
     * @param layer
     * @return
     */
    uint64_t GetCurrentCapacity(Layer layer) override ;
private:
    inline bool FileExists (const std::string& name) {
        struct stat buffer;
        return (stat (name.c_str(), &buffer) == 0);
    }
    void dataset_get_wrapper(void *dset, hid_t driver_id, H5VL_dataset_get_t get_type, hid_t dxpl_id, void **req, ...)
    {
        va_list args;
        va_start(args, req);
        H5VLdataset_get(dset, driver_id, get_type, dxpl_id, req, args);
    }
};




#endif //HERMES_HDF5_CLIENT_H
