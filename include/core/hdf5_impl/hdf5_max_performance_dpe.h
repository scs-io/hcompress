/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
 * <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of HCompress
 * 
 * HCompress is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
/*-------------------------------------------------------------------------
*
* Created: hdf5_max_performance_dpe.h
* June 5 2018
* Hariharan Devarajan <hdevarajan@hdfgroup.org>
*
* Purpose: It is the implementation of data_placement_engine on hdf5 file format.
*
*-------------------------------------------------------------------------
*/

#ifndef HERMES_HDF5_MAX_PERFORMANCE_DPE_H
#define HERMES_HDF5_MAX_PERFORMANCE_DPE_H


#include <vector>
#include <memory>
#include <core/interfaces/data_placement_engine.h>
#include <core/interfaces/metadata_manager.h>
#include <core/interfaces/data_organizer.h>
#include <core/hdf5_impl/hdf5_metadata_manager.h>
#include <core/hdf5_impl/hdf5_data_organizer.h>
#include <math.h>
#include <stdint-gcc.h>

class HDF5MaxPerformanceDataPlacementEngine: public DataPlacementEngine<HDF5ReadInput,HDF5WriteInput,HDF5Input,HDF5Output> {
public:
    /**
     * Constructors
     */
    HDF5MaxPerformanceDataPlacementEngine():DataPlacementEngine(){} /* default constructor*/
    HDF5MaxPerformanceDataPlacementEngine(HDF5MaxPerformanceDataPlacementEngine&& other){} /* Move Constructor*/
    HDF5MaxPerformanceDataPlacementEngine(HDF5MaxPerformanceDataPlacementEngine& other){} /* Copy Constructor*/

    /**
     * Methods
     */

    /**
     * Identifies placement based on policy for data to be written.
     *
     * @param input
     * @return vector of placements
     */
    std::vector<Placement<HDF5Input,HDF5Output>> PlaceWriteData(HDF5WriteInput input) override;

    /**
     * Identifies where data is placed in DMSH.
     *
     * @param input
     * @return vector of placements
     */
    std::vector<Placement<HDF5Input,HDF5Output>> PlaceReadData(HDF5ReadInput input) override;

    /**
     * Dynamic Programming recursive method to place data into layers Optimally.
     * Algorithm: minimize time as score will maximize Performance.
     * <ol>
     *      <li>if HasCapacity then calculate_placement_score()</li>
     *      <li>else if Can fit then
     *              min(
     *                  move data to next layer + calculate_placement_score(),
     *                  CalculatePlacementOptimally(input,layer+1)
     *              )
     *      </li>
     *      <li>else
     *              c = get capacity of layer
     *              input_part_1=c
     *              input_part_2=input.size-c
     *              min(
     *                  move data(c) to next layer + calculate_placement_score(input_part_1) +  CalculatePlacementOptimally(input_part_2,layer+1),
     *                  CalculatePlacementOptimally(input,layer+1)
     *              )     *
     *      </li>
     * </ol>
     * @param input
     * @param layer
     * @param buffer_dims
     * @return vector of placements
     */
    std::vector<Placement<HDF5Input,HDF5Output>> CalculatePlacementOptimally(HDF5Input input,Layer layer,
                                                                             Matrix &buffer_dims);

    /**
     * Maps HDF5 dataset type to an integer.
     * */

    int
    get_dataset_type(hid_t type);

    /**
    * Calculates the score for placing compressed input on layer
    *
    * @param input
    * @param layer
    * @return score value in float
    */
    float CalculatePlacementScore(HDF5Input input,Layer layer, CompressionLibrary comp_lib);

    std::pair<float,CompressionLibrary> FindOptimalCompression(HDF5Input input, Layer layer);


    /**
     * Calculates the cost of moving the data to next layer.
     *
     * @param input
     * @param source_layer
     * @param destination_layer
     * @return score value in float
     */
    float CalculateMovingScore(HDF5Input input,Layer source_layer,Layer destination_layer);

    /**
     * Builds Placement Object for given input on a layer.
     *
     * @param input
     * @param layer
     * @param move_required
     * @param comp_lib
     * @param generate_name
     * @param buffer_dims
     * @return Placement Object
     */
    Placement<HDF5Input, HDF5Output> BuildPlacement(HDF5Input input,
                                                    Layer layer,
                                                    bool move_required,
                                                    CompressionLibrary comp_lib,
                                                    bool generate_name,
                                                    Matrix &buffer_dims);

    /**
     * Splits the inputs into multiple chunks based on the capacity provided
     *
     * @param input
     * @param capacity_
     * @return vector of inputs.
     */
    std::vector<HDF5Input> SplitInput(HDF5Input input, uint64_t capacity_);
};


#endif //HERMES_HDF5_MAX_PERFORMANCE_DPE_H
