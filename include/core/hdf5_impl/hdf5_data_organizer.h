/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
 * <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of HCompress
 * 
 * HCompress is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
/*-------------------------------------------------------------------------
*
* Created: hdf5_data_organizer.h
* June 5 2018
* Hariharan Devarajan <hdevarajan@hdfgroup.org>
*
* Purpose: It is the implementation of data_organizer on hdf5 file format.
*
*-------------------------------------------------------------------------
*/

#ifndef HERMES_HDF5DATAORGANIZER_H
#define HERMES_HDF5DATAORGANIZER_H


#include <memory>
#include "../interfaces/data_organizer.h"
#include "../hdf5_impl/hdf5_client.h"
#include "hdf5_metadata_manager.h"
#include <H5public.h>
#include <hdf5.h>
#include "../io_client_factory.h"

class HDF5DataOrganizer: public DataOrganizer<HDF5Input,HDF5Output>,public Singleton<HDF5DataOrganizer> {
private:
    typedef IOClientFactory<HDF5Input,HDF5Output> HDF5IOFactory;
public:
    HDF5DataOrganizer():DataOrganizer(){} /* default constructor*/
    HDF5DataOrganizer(HDF5DataOrganizer&& other){} /* Move Constructor*/
    HDF5DataOrganizer(HDF5DataOrganizer& other){} /* Copy Constructor*/
    /**
     * Methods
     */

    /**
     * Method moves data from source to destination.
     *
     * @param source
     * @param destination
     * @return output
     */
    HDF5Output Move(HDF5Input source, HDF5Input destination) override;

    /**
     * Methods checks if a layer has capacity to accomodate the given source of data.
     *
     * @param source
     * @param layer
     * @return true if layer can fit source else false.
     */
    bool HasCapacity(HDF5Input source, Layer layer) override;

    /**
    * Makes capacity required to fit the source in the layer
    *
    * @param source
    * @param layer
    * @return output
    */
    HDF5Output MakeCapacity(HDF5Input source, Layer layer) override;

    /**
     * Checks if a layer can fit the given data.
     *
     * @param source
     * @param layer
     * @return true if layer can fit source else false.
     */
    bool CanFit(HDF5Input source, Layer layer) override;



    /**
     * Initilizes Buffer
     *
     * @param source
     * @return output
     */
    HDF5Output BufferInit(HDF5Input source) override;

    /**
     * Flushes Buffers for a given source.
     *
     * @param source
     * @return output.
     */
    HDF5Output BufferFlush(HDF5Input source) override;



};


#endif //HERMES_HDF5DATAORGANIZER_H
