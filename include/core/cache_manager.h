/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
 * <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of HCompress
 * 
 * HCompress is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
/*-------------------------------------------------------------------------
*
* Created: cache_manager.h
* June 5 2018
* Hariharan Devarajan <hdevarajan@hdfgroup.org>
*
* Purpose:Defines the Cache Manager Class for Hermer. This class uses a
* cache replacement policy that are defined in constants.h by
* CACHE_REPLACEMENT_POLICY
*
*-------------------------------------------------------------------------
*/

#ifndef HERMES_CACHE_MANAGER_H
#define HERMES_CACHE_MANAGER_H


#include <memory>
#include <iostream>
#include <common/data_structures.h>
#include <core/interfaces/replacement_policy.h>
#include <common/constants.h>
#include <core/replacement_policy/lru_policy.h>
#include <common/singleton.h>
#include <debug.h>

template <typename I,typename D,
        typename std::enable_if<std::is_base_of<Input, I>::value>::type* = nullptr,
        typename std::enable_if<std::is_base_of<Data, D>::value>::type* = nullptr>
class CacheManager {
private:

public:
    /**
     * constructor
     */
    CacheManager(){
    }
    /**
     * Methods
     */
    /**
     * Inserts a cache-line for corresponding data and Input.
     *
     * @param data
     * @param input
     * @return status code 0 if successful < 0 if it fails
     */
    int Insert(D data, I input){
        AutoTrace trace = AutoTrace("CacheManager::Insert",data,input);
        return Singleton<LRUPolicy<I,D>>::GetInstance()->Insert(data, input);
    }
    /**
     * Evicts a cache-line for corresponding data and Input from layer.
     * @param layer
     * @return evicted pair of Data and Input
     */
    std::pair<D,I> Evict(Layer layer){
        AutoTrace trace = AutoTrace("CacheManager::Evict",layer);
        return Singleton<LRUPolicy<I,D>>::GetInstance()->Evict(layer);
    }
    /**
     * deletes a cache-line for Data
     * @param data
     * @return deleted pair of Data and Input
     */
    std::pair<D,I> Delete(D data){
        AutoTrace trace = AutoTrace("CacheManager::Delete",data);
        return Singleton<LRUPolicy<I,D>>::GetInstance()->Delete(data);
    }

};




#endif //HERMES_CACHE_MANAGER_H
