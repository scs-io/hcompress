/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
 * <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of HCompress
 * 
 * HCompress is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
/*-------------------------------------------------------------------------
*
* Created: buffer.h
* June 5 2018
* Hariharan Devarajan <hdevarajan@hdfgroup.org>
*
* Purpose:Defines the Buffer API's for Hermes Platform
*
*-------------------------------------------------------------------------
*/

#ifndef HERMES_BUFFER_API_H
#define HERMES_BUFFER_API_H


#include <memory>
#include <core/hdf5_impl/hdf5_metadata_manager_opt.h>

#ifdef ENABLE_COMPRESSION

#include <ares_filter/hdf5_ares_filter.h>

#endif

#include "common/enumerations.h"
#include "interfaces/data_placement_engine.h"
#include "interfaces/io_client.h"
#include "hdf5_impl/hdf5_data_organizer.h"
#include "hdf5_impl/hdf5_client.h"
#include "hdf5_impl/hdf5_max_performance_dpe.h"
#include "hdf5_impl/hdf5_metadata_manager.h"
#include "common/constants.h"
#include "io_client_factory.h"
#include <debug.h>
#include "prefetch.h"

template<typename I, typename O, typename IW, typename OW, typename IR, typename OR, typename E, typename G,
        typename std::enable_if<std::is_base_of<Input, I>::value>::type * = nullptr,
        typename std::enable_if<std::is_base_of<Output, O>::value>::type * = nullptr,
        typename std::enable_if<std::is_base_of<ReadInput, IR>::value>::type * = nullptr,
        typename std::enable_if<std::is_base_of<ReadOutput, OR>::value>::type * = nullptr,
        typename std::enable_if<std::is_base_of<WriteInput, IW>::value>::type * = nullptr,
        typename std::enable_if<std::is_base_of<WriteOutput, OW>::value>::type * = nullptr,
        typename std::enable_if<std::is_base_of<Event<I>, E>::value>::type * = nullptr,
        typename std::enable_if<std::is_base_of<GraphNode<I>, G>::value>::type * = nullptr>
class BufferAPI {
private:
    typedef IOClientFactory<I, O> IOFactory;
    bool isInitialized;
    /**
     * Attributes
     */
    std::shared_ptr<DataPlacementEngine<IR, IW, I, O>> data_placement_engine; /* instance of data placement engine*/
    std::shared_ptr<DataOrganizer<I, O>> data_organizer;
    std::shared_ptr<MetadataManager<I>> metadataManager;
    std::shared_ptr<IOFactory> io_factory;
    std::shared_ptr<Prefetch<I, O, E, G>> prefetcher;
public:
    /**
     * constructors
     */
    BufferAPI() : isInitialized(false) {}

    /**
     * Methods
     */
    void Init(InterfaceType interface_type) {
        if (!isInitialized) {
            switch (interface_type) {
                /* when interface used is HDF5 */
                case InterfaceType::HDF5: {
                    /* make shared object of HDF5MaxPerformanceDataPlacementEngine */
                    data_placement_engine = Singleton<HDF5MaxPerformanceDataPlacementEngine>::GetInstance();
                    data_organizer = Singleton<HDF5DataOrganizer>::GetInstance(); /* initialise Buffer*/
                    metadataManager = Singleton<MetadataManagerFactory<HDF5Input>>::GetInstance()->GetMDM(MDM_TYPE); /* initialize metadata*/
                    io_factory = Singleton<IOFactory>::GetInstance();/* Initialize Buffers*/
#ifdef ENABLE_PREFETCH
                    prefetcher=Singleton<Prefetch<I,O,E,G>>::GetInstance();
                    prefetcher->Init(interface_type);
                    int rank;
                    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
                    if ((rank + 1) % HERMES_CONF->RANKS_PER_SERVER == 0) {
                        prefetcher->runServer(1);
                    }
#endif
#ifdef ENABLE_COMPRESSION
                    /*Initialize HDF5 compression libraries*/
                    H5_init_ares_filters();
#endif
                }
                    break;
            }
        }
        isInitialized = true;
    }

/**
 * Initializes the Buffer Platform for a given file and dataset.<br>
 * This function should be called on creation/opening of a dataset.
 *
 * @param input
 * @return output
 */
    inline O BufferInit(I input) {
        AutoTrace trace = AutoTrace("BufferAPI::BufferInit", input);
        uint64_t sequenceId;
#ifdef ENABLE_PREFETCH
        sequenceId = prefetcher->prefetch(input,EventType::OPEN_DATASET);
#endif
        data_organizer->BufferInit(input); /* initialise Buffer*/
        metadataManager->UpdateOnInit(input); /* initialize metadata*/
        O output = io_factory->GetClient(input.layer.io_client_type)->Create(input); /* create a placeholder at PFS */
#ifdef ENABLE_PREFETCH
        prefetcher->prefetch(input,EventType::OPEN_DATASET,sequenceId,true);
#endif
        return output;
    }

    /**
     * This method buffers a write operation into DMHS.<br>
     * This function should be called for buffering write of dataset into DMSH.
     * @param input
     * @return write_output
     */
    inline O BufferWrite(IW input) {
        AutoTrace trace = AutoTrace("BufferAPI::BufferWrite", 1, input);
        uint64_t sequenceId;
#ifdef ENABLE_PREFETCH
        sequenceId = prefetcher->prefetch(input,EventType::WRITE_DATASET);
#endif
        /* use DPE to decide where to place write data. */
        std::vector<Placement<I, O>> placements = data_placement_engine->PlaceWriteData(input);
        OW main_output;
        for (Placement<I, O> placement:placements) {
            if (placement.output.CheckSuccess()) {
                if (placement.destination_info.layer == *Layer::LAST) {
                    printf("Data written into %s\n", placement.destination_info.layer.layer_loc.c_str());
                }
                /* make space if we dont have space in current layer. */
                if (placement.move_required_ ||
                    !data_organizer->HasCapacity(placement.destination_info, placement.destination_info.layer)) {
                    data_organizer->MakeCapacity(placement.destination_info, placement.destination_info.layer);
                }
                /* write data into current layer. */
                O output = io_factory->GetClient(placement.destination_info.layer.io_client_type)->Write(
                        placement.source_info, placement.destination_info);
                if (!output.CheckSuccess()) return output;
                /* update metadata after successful write operation. */
                metadataManager->UpdateOnWrite(placement.user_info, placement.destination_info);

            } else main_output = placement.output;
        }
#ifdef ENABLE_PREFETCH
        prefetcher->prefetch(input,EventType::WRITE_DATASET,sequenceId,true);
#endif
        return main_output;
    };

    inline O BufferRead(IR input) {
        AutoTrace trace = AutoTrace("BufferAPI::BufferRead", 1, input);
        uint64_t sequenceId;
#ifdef ENABLE_PREFETCH
        sequenceId = prefetcher->prefetch(input,EventType::READ_DATASET);
#endif

        /* fetch where to read data from DPE. */
        std::vector<Placement<I, O>> placements = data_placement_engine->PlaceReadData(input);
        O main_output;
        if (placements.size() == 0) main_output.error = -1;
        for (Placement<I, O> placement:placements) {
            if (placement.output.CheckSuccess()) {
                if (placement.destination_info.layer == *Layer::LAST) {
                    printf("Data read from %s\n", placement.destination_info.layer.layer_loc.c_str());
                }
                placement.destination_info.HDF5Input::buffer = input.HDF5Input::buffer;
                /* read data from source. */
                O output = io_factory->GetClient(placement.destination_info.layer.io_client_type)->Read(
                        placement.source_info, placement.destination_info);
                /* udpate metadata after successful read. */
                metadataManager->UpdateOnRead(placement.user_info, placement.destination_info);
            } else main_output = placement.output;
        }
#ifdef ENABLE_PREFETCH
        prefetcher->prefetch(input,EventType::READ_DATASET,sequenceId,true);
#endif
        return main_output;
    };

    inline O BufferSync(I input) {
        AutoTrace trace = AutoTrace("BufferAPI::BufferSync", 1, input);
        uint64_t sequenceId;
#ifdef ENABLE_PREFETCH
        sequenceId = prefetcher->prefetch(input,EventType::CLOSE_DATASET);
#endif
        /* Call Buffer flush over dataset described by input. */
        O output;
        //output = data_organizer->BufferFlush(input);
        MPI_Barrier(MPI_COMM_WORLD);
#ifdef ENABLE_PREFETCH
        prefetcher->prefetch(input,EventType::CLOSE_DATASET,sequenceId,true);
#endif
        return output;

    }

    inline void Finalize() {
        AutoTrace trace = AutoTrace("BufferAPI::Finalize", 0);
        MPI_Barrier(MPI_COMM_WORLD);
        /* Call Buffer flush over dataset described by input. */
#ifdef ENABLE_PREFETCH
        prefetcher->stopServer();
#endif
    }
};

#endif //HERMES_BUFFER_API_H
