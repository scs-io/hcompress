/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
 * <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of HCompress
 * 
 * HCompress is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
/*-------------------------------------------------------------------------
*
* Created: io_client_factory.h
* June 5 2018
* Hariharan Devarajan <hdevarajan@hdfgroup.org>
*
* Purpose:Defines the Factory for choosing various IO Clients
*
*-------------------------------------------------------------------------
*/

#ifndef HERMES_PROJECT_IO_CLIENT_FACTORY_H
#define HERMES_PROJECT_IO_CLIENT_FACTORY_H

#include <common/data_structures.h>
#include <core/interfaces/io_client.h>
#include <core/hdf5_impl/hdf5_client.h>
#include <common/singleton.h>
#include <core/hdf5_impl/hdf5_in_memory.h>
#include <debug.h>

template <typename I,typename O,
        typename std::enable_if<std::is_base_of<Input, I>::value>::type* = nullptr,
        typename std::enable_if<std::is_base_of<Output, O>::value>::type* = nullptr>
class IOClientFactory {
public:
    IOClientFactory(){
        Singleton<HDF5Client>::GetInstance();
        Singleton<HDF5InMemory>::GetInstance();
    }
    std::shared_ptr<IOClient<I,O>> GetClient(IOClientType &type){
        /* Initialize all clients */
        AutoTrace trace = AutoTrace("IOClientFactory::GetClient",type);
        switch(type){
            case IOClientType::HDF5_FILE:{
                return Singleton<HDF5Client>::GetInstance();
            }
            case IOClientType::HDF5_MEMORY:{
                return Singleton<HDF5InMemory>::GetInstance();
            }
        }
    }
};
#endif //HERMES_PROJECT_IO_CLIENT_FACTORY_H
