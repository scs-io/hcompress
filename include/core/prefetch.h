/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
 * <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of HCompress
 * 
 * HCompress is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
//
// Created by HariharanDevarajan on 2/7/2019.
//

#ifndef HERMES_PROJECT_PREFETCH_H
#define HERMES_PROJECT_PREFETCH_H

#include <core/interfaces/prefetcher/event_builder.h>
#include <core/interfaces/prefetcher/event_communicator.h>
#include <common/constants.h>
#include <basket/clock/global_clock.h>
#include <core/interfaces/prefetcher/event_manager.h>
#include <thread>
#include <core/hdf5_impl/prefetcher/hdf5_event_builder.h>
#include <common/singleton.h>

template <typename I, typename O, typename E,typename G
        , typename std::enable_if<std::is_base_of<Event<I>, E>::value>::type* = nullptr
        , typename std::enable_if<std::is_base_of<GraphNode<I>, G>::value>::type* = nullptr
        , typename std::enable_if<std::is_base_of<Output, O>::value>::type* = nullptr
        , typename std::enable_if<std::is_base_of<Input, I>::value>::type* = nullptr>
class Prefetch{
private:
    bool isInitialized;
    int num_workers;
    int rank;
    std::thread* eventManagerWorkers;
    std::promise<void>* eventManagerExitSignals;
    std::thread* triggerManagerWorkers;
    std::promise<void>* triggerManagerExitSignals;
    std::shared_ptr<EventBuilder<I,E>> eventBuilder;
    std::shared_ptr<EventCommunicator<I,E>> eventCommunicator;
    std::shared_ptr<EventManager<I,O,E,G>> eventManager;
    std::shared_ptr<TriggerManager<I,O,E,G>> triggerManager;
    PrefetchStatus runEventServerInternal(std::future<void> futureObj){
        AutoTrace trace = AutoTrace("Prefetcher::runEventServerInternal",0);
        std::vector<E> events=std::vector<E>();
        Timer timer;
        timer.startTime();
        while(futureObj.wait_for(std::chrono::milliseconds(1)) == std::future_status::timeout){
            auto newEvents=eventCommunicator->subscribe();
            events.insert( events.end(), newEvents.begin(), newEvents.end() );
            if(events.size() > 0 && (events.size() >= MAX_PREFETCH_EVENTS || timer.endTime() > MAX_PREFETCH_TIME)){
                eventManager->handle(events);
                events.clear();
                timer.startTime();
            }
        }
        timer.startTime();
        return PrefetchStatus::PREFETCH_SUCCESS;
    }

    PrefetchStatus runTriggerServerInternal(std::future<void> futureObj){
        AutoTrace trace = AutoTrace("Prefetcher::runPrefetchServerInternal",0);
        while(futureObj.wait_for(std::chrono::milliseconds(1)) == std::future_status::timeout){
            E event;
            bool trigger=triggerManager->subscribe(event);
            if(trigger){
                triggerManager->handle(event);
            }
        }
        return PrefetchStatus::PREFETCH_SUCCESS;
    }
public:
    Prefetch():num_workers(1),isInitialized(false){
        AutoTrace trace = AutoTrace("Prefetcher::Prefetch",0);
        MPI_Comm_rank(MPI_COMM_WORLD, &rank);
        eventCommunicator=Singleton<EventCommunicator<I,E>>::GetInstance();
        eventManager=Singleton<EventManager<I,O,E,G>>::GetInstance();
        triggerManager=Singleton<TriggerManager<I,O,E,G>>::GetInstance();
    }

    void Init(InterfaceType type){
        if(!isInitialized){
            AutoTrace trace = AutoTrace("Prefetcher::Init",1,type);
            triggerManager->Init(type);
            switch(type){
                /* when interface used is HDF5 */
                case InterfaceType::HDF5:{
                    eventBuilder=Singleton<HDF5EventBuilder>::GetInstance();

                    break;
                }
            }
            isInitialized=true;
        }
    }

    uint64_t prefetch(I input, EventType type, uint64_t sequenceId=0, bool is_end=false){
        AutoTrace trace = AutoTrace("Prefetcher::prefetch",2,input,type);
        std::vector<E> events=eventBuilder->build(input,type,sequenceId,is_end);
        eventCommunicator->publish(events);
        return events[0].sequenceId;
    }

    PrefetchStatus runServer(size_t numWorker=1){
        AutoTrace trace = AutoTrace("Prefetcher::runServer",1,numWorker);
        num_workers=numWorker;
        if(numWorker > 0){
            eventManagerWorkers=new std::thread[numWorker];
            eventManagerExitSignals=new std::promise<void>[numWorker];
            triggerManagerWorkers=new std::thread[numWorker];
            triggerManagerExitSignals=new std::promise<void>[numWorker];
            for(int i=0;i<numWorker;++i) {
                std::future<void> futureObj = eventManagerExitSignals[i].get_future();
                eventManagerWorkers[i]=std::thread (&Prefetch::runEventServerInternal, this, std::move(futureObj));
                std::future<void> futureObj2 = triggerManagerExitSignals[i].get_future();
                triggerManagerWorkers[i]=std::thread (&Prefetch::runTriggerServerInternal, this, std::move(futureObj2));
            }

        }

    }
    ~Prefetch(){
        if(isInitialized) stopServer();
    }

    void stopServer(){
        AutoTrace trace = AutoTrace("Prefetcher::stopServer",0);
        if ((rank + 1) % HERMES_CONF->RANKS_PER_SERVER == 0) {

            while(triggerManager->hasEvents()){}
            eventManager->finalize();
            for (int i = 0; i < num_workers; ++i) {
                eventManagerExitSignals[i].set_value();
                triggerManagerExitSignals[i].set_value();
                eventManagerWorkers[i].join();
                triggerManagerWorkers[i].join();
            }
        }
        isInitialized=false;
    }
};
#endif //HERMES_PROJECT_PREFETCH_H
