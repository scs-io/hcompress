# Introduction

Modern scientific applications read and write massive amounts of data through simulations, observations, andanalysis. These applications spend the majority of their runtime in performing I/O on rotating disks. HPC storage solutions include fast node-local and shared storage resources to elevate applications from this bottleneck. Moreover, several middleware libraries such as Hermes, Univistor, and Proactive Data Contain-ers are proposed to move data between these tiers transparently.Data reduction is another technique that reduces the amount of data produced and, hence, improve I/O performance. These two technologies, if used together, can benefit from each other. The effectiveness of data compression can be enhanced by selecting different compression algorithms according to the characteristics of the different tiers, and the multi-tiered hierarchy can benefit from extra capacity. In this paper, we design and implement HCompress, a hierarchical data compression library that can improve the application’s performance by harmoniously leveraging both multi-tiered storage and data compression.We have developed a novel compression selection algorithm that facilitates the optimal matching of compression libraries to the tiered storage. Our evaluation shows that HCompresscan improve scientific application’s performance by 7x when compared to other state-of-the-art tiered storage solutions.

# Who uses

Ares is used within [Hermes](http://www.cs.iit.edu/~scs/assets/projects/Hermes/Hermes.html) to perform hierarchical compression on buffered data based on various application and system characteristics.

# How it works

-   HCompress contains a library (libhcompress) which exposes API’s (compress and decompress).
    
-   HCompress has a HDF VOL plugin which can be loaded for HDF5 applications
    
-   HCompress has a HDF filter which can be added within HDF5 applications to perform dynamic compression.
    
-   HCompress contains a CLI tool which profiles application and system charectrics for the HCompress library.
    

# Dependencies

-   JRE > 1.8
    
-   Cmake > 3.13,3
    
-   Gcc > 7.3
    
-   MPICH > 3.3
    
-   HDF5 > 10
    
-   H5Part
    
-   Boost > 1.69.0
    
-   RapidJSON
    
-   RPClib
    

# Installation

## Cmake

`mkdir build && cd build`

`cmake ../`

`make -j8 && make install`

# Usage

## Library

Suitable for applications that want to simply link and use the HCompress on compile time directly. Think of a user-facing application which want to compress and decompress data buffers using C++ API.

### C++ API

```cpp
bool compress(const void * source, size_t source_size, void *& destination, size_t &destination_size, Flags &flags);
bool decompress(const void *source, size_t source_size, void *&destination, size_t &destination_size, Flags &flags);
```
### C API
```c
bool compress(void * source, long unsigned int source_size, void * destination, long unsigned int destination_size, uint16_t data_type, uint16_t library,uint16_t format,uint16_t priority);
bool compress(void * source, long unsigned int source_size, void * destination, long unsigned int destination_size, uint16_t data_type, uint16_t library,uint16_t format,uint16_t priority);
```

### HDF5 vol plugin

We have a vol plugin for HDF5v applications to transparently use HCompress.

```
export HDF5_PLUGIN_PATH=<PATH-TO-LIB>
export HDF5_VOL_CONNECTOR=hermes_ares
``` 

### CLI tool

Suitable for user who want to use Ares on the command-line interface and compress/decompress files and folders. From the CLI tool users can directly run Ares engine or even select specific compression libraries to compress/decompression.

```
USAGE:
		-h 	Print out help for this program
		-d 	"/path/to": Intermediate directory for storing data
		-f 	"/path/to/file.5": Run benchmark on existing HDF5 file
		-g 	count type1 type2 ...: Generate a file with datasets 
			type1, type2, ... each with the same number of elements]
		-o 	"/path/to/output.json": Set benchmarking data output file
		-p 	distribution param1 param2
				TYPES:
				1: Generate dataset of characters
				2: Generate dataset of 32-bit signed integers
				3: Generate dataset of sorted 32-bit signed integers
				4: Generate dataset of 32-bit unsigned integers
				5: Generate dataset of sorted 32-bit unsigned integers
				6: Generate dataset of floats
				7: Generate dataset of doubles`
```
# Running Tests

## Ctest

### VPIC:
`ctest -R vpic`

### BD-CATS:

`ctest -R bd-cats`

### All:

`ctest`

# License:

# Acknowledgments: