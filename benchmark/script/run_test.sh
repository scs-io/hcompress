#!/bin/bash
# Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
# Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
# <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
#
# This file is part of HCompress
# 
# HCompress is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
: '
for ((i=0; i<=40; i++))
do
	mpirun -n $i /home/hdevarajan/projects/hfetch/build/hfetch_thread_ben 0
	sleep $((i+5))
done
'
mpirun -n 1 /home/hdevarajan/projects/hfetch/build/hfetch_thread_ben 1

sleep 5
for ((i=0; i<=40; i++))
do
        mpirun -n $((40+i)) /home/hdevarajan/projects/hfetch/build/hfetch_thread_ben ${i}
	sleep $((i+40))
done
