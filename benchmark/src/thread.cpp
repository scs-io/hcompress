/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
 * <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of HCompress
 * 
 * HCompress is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
//
// Created by hariharan on 1/14/19.
//

#include "thread.h"

double
ThreadBenchmark::run_with_prefetch(really_long main_loop_count, really_long child_loop_count, uint prefetch_cores, bool prefetch, bool affinity) {

    unsigned num_cpus = std::thread::hardware_concurrency();
    --num_cpus;
    std::vector<std::thread> threads(num_cpus + prefetch_cores);
    for (unsigned i = 0; i < num_cpus; ++i) {
        if(i < prefetch_cores){
            threads[i] = std::thread(main_function,main_loop_count,elements_,data_,i+1);
            threads[num_cpus + i] = std::thread(prefetch_function,child_loop_count,elements_,data_,i);
        }
        else{
            threads[i] = std::thread(main_function,main_loop_count,elements_,data_,i);
        }
    }
    main_function(main_loop_count,elements_,data_,num_cpus + prefetch_cores + 1);
    for (auto& t : threads) {
        t.join();
    }
    return 0;
}


