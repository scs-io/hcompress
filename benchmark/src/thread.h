/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
 * <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of HCompress
 * 
 * HCompress is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
//
// Created by hariharan on 1/14/19.
//

#ifndef PROJECT_THREAD_H
#define PROJECT_THREAD_H
#include <iostream>
#include <vector>
#include "timer.h"
#include <malloc.h>
#include <thread>
#include <mpi.h>

typedef unsigned long long int really_long;

class ThreadBenchmark {
private:
    int * data_;
    int elements_;
public:
    ThreadBenchmark(int elements){
        data_= static_cast<int *>(malloc(elements * sizeof(int)));
        for(int i=0;i<elements;i++) data_[i]=1;
        elements_=elements;
    }
    double run_with_prefetch(really_long main_loop_count, really_long child_loop_count, uint prefetch_cores,bool prefetch, bool affinity);
    ~ThreadBenchmark(){
        free(data_);
    }
};

void prefetch_function(really_long child_loop_count, int elements, void *data_,int core)
{
    Timer t=Timer();
    t.startTime();
    int * data=(int*)data_;
    for (really_long i = 0;i<child_loop_count; i++){
        std::string filename("file_"+std::to_string(core));
        FILE *fh=std::fopen(filename.c_str(),"w+");
        if(fh == nullptr) std::cout<<"cannot open file\n";
        size_t count=fwrite(data,sizeof(int),elements/2,fh);
        fclose(fh);
        remove(filename.c_str());
        if(i%64==0){
            Timer t2=Timer();
            t2.startTime();
            while(t2.stopTime() < 1){}
        }
    }
    double t1=t.stopTime();
    printf("Prefetch,%d,%f\n",core,t1);

}
void main_function(really_long main_loop_count,int elements,int * data_,int core)
{
    Timer t=Timer();
    t.startTime();
    int * data=(int*)data_;
    int result;
    for (really_long i = 0; i < main_loop_count; ++i){
        really_long array_index=rand()%elements;
        result=data[array_index]*data[elements-array_index]*data[elements-array_index/2]/data[array_index/2];
    }
    double t2=t.stopTime();
    printf("Compute,%d,%f\n",core,t2);
}

int main(int argc, char**argv){

    
    MPI_Init(&argc,&argv);
    int my_rank, num_procs;
    MPI_Comm_rank (MPI_COMM_WORLD, &my_rank);
    MPI_Comm_size (MPI_COMM_WORLD, &num_procs);
    int prefetch_cores = 0;
    if(argc>1) prefetch_cores = atoi(argv[1]);
    int compute_cores = num_procs - prefetch_cores;
    really_long main_loop_count = 1024ULL*1024ULL*512ULL;//*1024ULL*1024ULL*1024ULL;
    if(my_rank==0) printf("\nCase Compute: %d Prefetch: %d\n\n",compute_cores,prefetch_cores);
    MPI_Barrier(MPI_COMM_WORLD);
    really_long child_loop_count = 256ULL;//*1024ULL*1024ULL*1024ULL;
    if(prefetch_cores > 0) child_loop_count = child_loop_count/prefetch_cores;
    int elements = 1024*1024;
    int * data_;
    data_= static_cast<int *>(malloc(elements * sizeof(int)));
    for(int i=0;i<elements;i++) data_[i]=1;
    if(my_rank < compute_cores){
        main_function(main_loop_count,elements,data_,my_rank+1);
    }else{
        prefetch_function(child_loop_count, elements, data_, my_rank+1);
    }
    free(data_);
    MPI_Finalize();
    return 0;
}


#endif //PROJECT_THREAD_H
