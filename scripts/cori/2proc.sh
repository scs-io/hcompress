#!/bin/bash -l
# Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
# Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
# <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
#
# This file is part of HCompress
# 
# HCompress is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

#SBATCH -q debug 
#SBATCH -N 1
#SBATCH -t 00:30:00
#SBATCH -J hermes_2_procs
#SBATCH -C haswell
#DW jobdw capacity=100GB access_mode=striped type=scratch
cd ${SCRATCH}/hermes/hermes/build
rm -rf /global/homes/l/lrknox/bb
ln -s ${DW_JOB_STRIPED} ~/bb
export HDF5_USE_FILE_LOCKING=FALSE
export XTPE_LINK_TYPE=dynamic
export CRAYPE_LINK_TYPE=dynamic
 
export LD_LIBRARY_PATH=/opt/intel/compilers_and_libraries_2018.1.163/linux/mpi/intel64/lib/:/global/common/cori/software/libfabric/1.6.1/gnu/lib:/global/cscratch1/sd/lrknox/hermes/hdf5/hdf5/lib::/opt/intel/compilers_and_libraries_2018.1.163/linux/compiler/lib/intel64:/opt/intel/compilers_and_libraries_2018.1.163/linux/mkl/lib/intel64:/opt/gcc/5.3.0/snos/lib64:/opt/cray/job/2.2.2-6.0.5.0_8.47__g3c644b5.ari/lib64:/usr/syscom/nsg/lib
export PATH=/opt/intel/compilers_and_libraries_2018.1.163/linux/mpi/intel64/include:/opt/intel/compilers_and_libraries_2018.1.163/linux/mpi/intel64/bin:$PATH
export CC=/opt/intel/compilers_and_libraries_2018.1.163/linux/mpi/intel64/bin/mpicc
export CXX=/opt/intel/compilers_and_libraries_2018.1.163/linux/mpi/intel64/bin/mpicxx
export I_MPI_PMI_LIBRARY=/usr/lib64/slurmpmi/libpmi2.so
export  I_MPI_PMI2=yes
ctest -R cori_h5_buffer_write_fpp_mpi_1_1_
