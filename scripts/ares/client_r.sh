#!/bin/bash
# Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
# Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
# <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
#
# This file is part of HCompress
# 
# HCompress is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
NODES=$(cat compute_nodes)
SCRIPT_DIR=`pwd`
for node in $NODES
do
echo "Starting client on $node"
ssh $node /bin/bash << EOF
sudo kill-pvfs2-client
mkdir -p /mnt/nvme/hdevarajan/pfs /mnt/nvme/hdevarajan/bb
sudo insmod ${ORANGEFS_KO}/pvfs2.ko
sudo ${ORANGEFS_PATH}/sbin/pvfs2-client -p ${ORANGEFS_PATH}/sbin/pvfs2-client-core
sudo mount -t pvfs2 tcp://ares-stor-01-40g:3334/orangefs /mnt/nvme/hdevarajan/pfs
sudo mount -t pvfs2 tcp://ares-stor-26-40g:3334/orangefs /mnt/nvme/hdevarajan/bb
mount | grep pvfs2
EOF
done

