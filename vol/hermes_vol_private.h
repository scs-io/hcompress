/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
 * <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of HCompress
 * 
 * HCompress is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
/*-------------------------------------------------------------------------
*
* Created: hermes_vol.h
* June2018
* Hariharan Devarajan <hdevarajan@hdfgroup.org>
*
* Purpose:Defines the Hermes VOL Plugin Private Header file
*
*-------------------------------------------------------------------------
*/

#ifndef HERMES_PROJECT_HERMES_VOL_PRIVATE_H
#define HERMES_PROJECT_HERMES_VOL_PRIVATE_H
#include <hdf5.h>
#include <H5VLpublic.h>
#include <assert.h>
#include <stdlib.h>
#include "../include/hermes_vol.h"
#include "../include/hermes.h"
#include <memory.h>

#define VOL_NAME "hermes"
#if defined(ENABLE_COMPRESSION)
#define COMP "_ares"
#else
#define COMP
#endif
#if defined(DEBUG_TIMER)
#define TIMER "_timer"
#else
#define TIMER
#endif
#if defined(DEBUG_TRACE)
#define TRACE "_trace"
#else
#define TRACE
#endif
#define HERMES_VOL_NAME VOL_NAME COMP TIMER TRACE

#define HERMES_VOL (hermes_register())
#ifdef __cplusplus
extern "C"
{
#endif

H5_DLL hid_t hermes_register(void);

#ifdef __cplusplus
}
#endif
typedef struct H5VL_t {
    const H5VL_class_t *vol_cls;        /* constant driver class info                           */
    /* XXX: Is an integer big enough? */
    int                 nrefs;          /* number of references by objects using this struct    */
    hid_t               vol_id;         /* identifier for the VOL class                         */
} H5VL_t;
typedef struct H5VL_object_t {
    void               *vol_obj;        /* pointer to object created by driver                  */
    H5VL_t             *vol_info;       /* pointer to VOL info struct                           */
} H5VL_object_t;

typedef struct hermes_wrap_ctx_t
{
    hid_t under_vol_id;   /* VOL ID for under VOL */
    void *under_wrap_ctx; /* Object wrapping context for under VOL */
} hermes_wrap_ctx_t;

/* Pass-through VOL connector info */
typedef struct hermes_info_t
{
    hid_t under_vol_id;   /* VOL ID for under VOL */
    void *under_vol_info; /* VOL info for under VOL */
} hermes_info_t;

/**
 * This is the Data structure used within Hermes VOl for maintaining basic data.
 */
typedef struct HermesVol {
    hid_t object_id;
    hid_t vol_id;
    hid_t native_id;
    hid_t native_fapl;
    hid_t hermes_id;
    hid_t hermes_fapl;
    char* file_name;
    char* dataset_name;
    char* group_name;
    hid_t under_vol_id; /* ID for underlying VOL connector */
    void *under_object; /* Info object for underlying VOL connector */
    bool sync;
} hermes_t;

/* Hermes VOL Dataset callbacks */
static void  *hermes_dataset_create(void *_item, const H5VL_loc_params_t *loc_params, const char *name, hid_t lcpl_id, hid_t type_id, hid_t space_id, hid_t dcpl_id, hid_t dapl_id, hid_t dxpl_id, void **req);
static void *hermes_dataset_open(void *_item, const H5VL_loc_params_t *loc_params, const char *name, hid_t dapl_id, hid_t dxpl_id, void **req);
static herr_t hermes_dataset_read(void *dset, hid_t mem_type_id, hid_t mem_space_id, hid_t file_space_id, hid_t dxpl_id, void *buf, void **req);
static herr_t hermes_dataset_write(void *dset, hid_t mem_type_id, hid_t mem_space_id, hid_t file_space_id, hid_t dxpl_id, const void *buf, void **req);
static herr_t  hermes_dataset_get(void *dset, H5VL_dataset_get_t get_type, hid_t dxpl_id, void **req, va_list arguments);
static herr_t hermes_dataset_optional(void *obj, hid_t dxpl_id, void **req, va_list arguments);
static herr_t hermes_dataset_specific(void *obj, H5VL_dataset_specific_t specific_type, hid_t dxpl_id, void **req, va_list arguments);
static herr_t hermes_dataset_close(void *dset, hid_t dxpl_id, void **req);


/* Hermes VOL File callbacks */
static void  *hermes_file_create(const char *name, unsigned flags, hid_t fcpl_id, hid_t fapl_id, hid_t dxpl_id, void **req);
static void  *hermes_file_open(const char *name, unsigned flags, hid_t fapl_id, hid_t dxpl_id, void **req);
static herr_t hermes_file_get(void *file, H5VL_file_get_t get_type, hid_t dxpl_id, void **req, va_list arguments);
static herr_t hermes_file_specific_reissue(void *obj, hid_t connector_id, H5VL_file_specific_t specific_type, hid_t dxpl_id, void **req, ...);
static herr_t hermes_file_specific(void *file, H5VL_file_specific_t specific_type, hid_t dxpl_id, void **req, va_list arguments);
static herr_t hermes_file_optional(void *file, hid_t dxpl_id, void **req, va_list arguments);
static herr_t hermes_file_close(void *file, hid_t dxpl_id, void **req);

/* Hermes VOL attribute callbacks*/
static void * hermes_attribute_create(void *_item, const H5VL_loc_params_t *loc_params, const char *name, hid_t type_id, hid_t space_id, hid_t acpl_id, hid_t aapl_id, hid_t dxpl_id, void **req);
static void * hermes_attribute_open(void *_item, const H5VL_loc_params_t *loc_params, const char *name, hid_t aapl_id, hid_t dxpl_id, void **req);
static herr_t hermes_attribute_read(void *_attr, hid_t mem_type_id, void *buf, hid_t dxpl_id, void **req);
static herr_t hermes_attribute_write(void *_attr, hid_t mem_type_id, const void *buf,  hid_t dxpl_id, void **req);
static herr_t hermes_attribute_get(void *_item, H5VL_attr_get_t get_type, hid_t dxpl_id, void **req, va_list arguments);
static herr_t hermes_attribute_specific(void *_item, const H5VL_loc_params_t *loc_params, H5VL_attr_specific_t specific_type, hid_t dxpl_id, void **req, va_list arguments);
static herr_t hermes_attribute_close(void *_attr, hid_t dxpl_id, void **req);

/* "Management" callbacks */
static herr_t hermes_init(hid_t vipl_id);

static herr_t hermes_term();
void *hermes_info_copy(const void *info);
herr_t hermes_info_cmp(int *cmp_value, const void *info1, const void *info2);
herr_t hermes_info_free(void *info);
herr_t hermes_info_to_str(const void *info, char **str);
herr_t hermes_str_to_info(const char *str, void **info);
void *hermes_get_object(const void *obj);
herr_t hermes_get_wrap_ctx(const void *obj, void **wrap_ctx);
herr_t hermes_free_wrap_ctx(void *obj);
void *hermes_wrap_object(void *obj, H5I_type_t obj_type, void *wrap_ctx);

/* Group callbacks */
void *hermes_group_create(void *obj, const H5VL_loc_params_t *loc_params, const char *name, hid_t lcpl_id, hid_t gcpl_id, hid_t gapl_id, hid_t dxpl_id, void **req);
void *hermes_group_open(void *obj, const H5VL_loc_params_t *loc_params, const char *name, hid_t gapl_id, hid_t dxpl_id, void **req);
herr_t hermes_group_get(void *obj, H5VL_group_get_t get_type, hid_t dxpl_id, void **req, va_list arguments);
herr_t hermes_group_specific(void *obj, H5VL_group_specific_t specific_type, hid_t dxpl_id, void **req, va_list arguments);
herr_t hermes_group_optional(void *obj, hid_t dxpl_id, void **req, va_list arguments);
herr_t hermes_group_close(void *grp, hid_t dxpl_id, void **req);


/* definition of Hermes VOL plugin. */
static const H5VL_class_t hermes_g = {
        1,                 /* Plugin Version number */
        (H5VL_class_value_t)(HERMES),                     /* Plugin Value */
        HERMES_VOL_NAME,                        /* Plugin Name */
        0,                                       /* Plugin capability flags */
        hermes_init,                            /* Plugin initialize */
        hermes_term,                            /* Plugin terminate */
        {
                sizeof(hermes_t),                  /* Plugin Info size */
                hermes_info_copy,                       /* Plugin Info copy */
                hermes_info_cmp,                                    /* Plugin Info compare */
                hermes_info_free,                       /* Plugin Info free */
                hermes_info_to_str,                                    /* Plugin Info To String */
                hermes_str_to_info,                                    /* Plugin String To Info */
        },
        {
                hermes_get_object,                                    /* Plugin Get Object */
                hermes_get_wrap_ctx,                                    /* Plugin Get Wrap Ctx */
                hermes_wrap_object,                                    /* Plugin Wrap Object */
                NULL,                                    /* Plugin Unwrap Object */
                hermes_free_wrap_ctx,                                    /* Plugin Free Wrap Ctx */
        },
        {                                        /* Plugin Attribute cls */
                hermes_attribute_create,            /* Plugin Attribute create */
                hermes_attribute_open,              /* Plugin Attribute open */
                hermes_attribute_read,              /* Plugin Attribute read */
                hermes_attribute_write,             /* Plugin Attribute write */
                hermes_attribute_get,               /* Plugin Attribute get */
                hermes_attribute_specific,          /* Plugin Attribute specific */
                NULL,                                /* Plugin Attribute optional */
                hermes_attribute_close,              /* Plugin Attribute close */
        },
        {                                        /* Plugin Dataset cls */
                hermes_dataset_create,              /* Plugin Dataset create */
                hermes_dataset_open,                /* Plugin Dataset open */
                hermes_dataset_read,                /* Plugin Dataset read */
                hermes_dataset_write,               /* Plugin Dataset write */
                hermes_dataset_get,                 /* Plugin Dataset get */
                hermes_dataset_specific,            /* Plugin Dataset specific */
                hermes_dataset_optional,                                /* Plugin Dataset optional */
                hermes_dataset_close                /* Plugin Dataset close */
        },
        {                                        /* Plugin Datatype cls */
                NULL,             /* Plugin Datatype commit */
                NULL,               /* Plugin Datatype open */
                NULL,                /* Plugin Datatype get */
                NULL,           /* Plugin Datatype specific */
                NULL,                                /* Plugin Datatype optional */
                NULL               /* Plugin Datatype close */
        },
        {                                        /* Plugin File cls */
                hermes_file_create,                 /* Plugin File create */
                hermes_file_open,                   /* Plugin File open */
                hermes_file_get,                    /* Plugin File get */
                hermes_file_specific,               /* Plugin File specific */
                hermes_file_optional,                                /* Plugin File optional */
                hermes_file_close                   /* Plugin File close */
        },
        {                                        /* Plugin Group cls */
                hermes_group_create,                /* Plugin Group create */
                hermes_group_open,                  /* Plugin Group open */
                hermes_group_get,                   /* Plugin Group get */
                hermes_group_specific,              /* Plugin Group specific */
                hermes_group_optional,                                /* Plugin Group optional */
                hermes_group_close                  /* Plugin Group close */
        },
        {                                        /* Plugin Link cls */
                NULL,                 /* Plugin Link create */
                NULL,                   /* Plugin Link copy */
                NULL,                   /* Plugin Link move */
                NULL,                    /* Plugin Link get */
                NULL,               /* Plugin Link specific */
                NULL                                 /* Plugin Link optional */
        },
        {                                        /* Plugin Object cls */
                NULL,                 /* Plugin Object open */
                NULL,                 /* Plugin Object copy */
                NULL,                  /* Plugin Object get */
                NULL,             /* Plugin Object specific */
                NULL              /* Plugin Object optional */
        },
        {
                NULL,                                /* Plugin Request wait */
                NULL,                                /* Plugin Request notify */
                NULL,                                /* Plugin Request cancel */
                NULL,                                /* Plugin Request specific */
                NULL,                                /* Plugin Request optional */
                NULL                     /* Plugin Request free */
        },
        NULL                         /* Plugin optional */
};

#endif //HERMES_PROJECT_HERMES_VOL_H
