/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
 * <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of HCompress
 * 
 * HCompress is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
/*-------------------------------------------------------------------------
*
* Created: hermes_vol_private.c
* June 11 2018
* Hariharan Devarajan <hdevarajan@hdfgroup.org>
*
* Purpose:Defines the Hermes VOL Plugin
*
*-------------------------------------------------------------------------
*/

#include <hdf5.h>
#include <libgen.h>
#include "hermes_vol_private.h"

hid_t HERMES_g = H5I_INVALID_HID;

/* Hermes VOL other callbacks*/
static herr_t hermes_init(hid_t vipl_id) {
    vipl_id = vipl_id;
    return H5_Init();
}

static herr_t hermes_term() {
    HERMES_g = H5I_INVALID_HID;
    return H5_Finalize();
}

hermes_t *
hermes_new_obj(void *under_obj, hid_t under_vol_id,H5I_type_t type) {
    hermes_t *new_obj;

    new_obj = (hermes_t *) calloc(1, sizeof(hermes_t));
    new_obj->under_object = under_obj;
    new_obj->under_vol_id = under_vol_id;
    if(type!=H5I_NTYPES)
        new_obj->object_id = H5VLwrap_register(new_obj,type);
    H5Iinc_ref(new_obj->under_vol_id);

    return (new_obj);
}

herr_t
hermes_free_obj(hermes_t *obj) {
    H5Idec_ref(obj->under_vol_id);
    free(obj);
    return (0);
}

void *
hermes_info_copy(const void *_info) {
    const hermes_info_t *info = (const hermes_info_t *) _info;
    hermes_info_t *new_info;

    /* Allocate new VOL info struct for the pass through connector */
    new_info = (hermes_info_t *) calloc(1, sizeof(hermes_info_t));

    /* Increment reference count on underlying VOL ID, and copy the VOL info */
    new_info->under_vol_id = info->under_vol_id;
    H5Iinc_ref(new_info->under_vol_id);
    if (info->under_vol_info)
        H5VLcopy_connector_info(new_info->under_vol_id, &(new_info->under_vol_info), info->under_vol_info);

    return (new_info);
}

herr_t
hermes_info_cmp(int *cmp_value, const void *_info1, const void *_info2) {
    const hermes_info_t *info1 = (const hermes_info_t *) _info1;
    const hermes_info_t *info2 = (const hermes_info_t *) _info2;

    /* Sanity checks */
    assert(info1);
    assert(info2);

    /* Initialize comparison value */
    *cmp_value = 0;

    /* Compare under VOL connector classes */
    H5VLcmp_connector_cls(cmp_value, info1->under_vol_id, info2->under_vol_id);
    if (*cmp_value != 0)
        return (0);

    /* Compare under VOL connector info objects */
    H5VLcmp_connector_info(cmp_value, info1->under_vol_id, info1->under_vol_info, info2->under_vol_info);
    if (*cmp_value != 0)
        return (0);

    return (0);
}

herr_t hermes_info_free(void *_info) {

    hermes_info_t *info = (hermes_info_t *) _info;

    /* Release underlying VOL ID and info */
    H5Idec_ref(info->under_vol_id);

    /* Free pass through info object itself */
    free(info);

    return (0);
}

herr_t
hermes_info_to_str(const void *_info, char **str) {
    const hermes_info_t *info = (const hermes_info_t *) _info;
    H5VL_class_value_t under_value = (H5VL_class_value_t) -1;
    char *under_vol_string = NULL;
    size_t under_vol_str_len = 0;

    /* Get value and string for underlying VOL connector */
    H5VLget_value(info->under_vol_id, &under_value);
    H5VLconnector_info_to_str(info->under_vol_info, info->under_vol_id, &under_vol_string);

    /* Determine length of underlying VOL info string */
    if (under_vol_string)
        under_vol_str_len = strlen(under_vol_string);

    /* Allocate space for our info */
    *str = (char *) H5allocate_memory(32 + under_vol_str_len, (hbool_t) 0);
    assert(*str);

    /* Encode our info */
    snprintf(*str, 32 + under_vol_str_len, "under_vol=%u;under_info={%s}", (unsigned) under_value,
             (under_vol_string ? under_vol_string : ""));

    return (0);
}

herr_t
hermes_str_to_info(const char *str, void **_info) {
    hermes_info_t *info;
    unsigned under_vol_value;
    const char *under_vol_info_start, *under_vol_info_end;
    hid_t under_vol_id;
    void *under_vol_info = NULL;

    /* Retrieve the underlying VOL connector value and info */
    sscanf(str, "under_vol=%u;", &under_vol_value);
    under_vol_id = H5VLregister_connector_by_value((H5VL_class_value_t) under_vol_value, H5P_DEFAULT);
    under_vol_info_start = strchr(str, '{');
    under_vol_info_end = strrchr(str, '}');
    assert(under_vol_info_end > under_vol_info_start);
    if (under_vol_info_end != (under_vol_info_start + 1)) {
        char *under_vol_info_str;

        under_vol_info_str = (char *) malloc((size_t) (under_vol_info_end - under_vol_info_start));
        memcpy(under_vol_info_str, under_vol_info_start + 1,
               (size_t) ((under_vol_info_end - under_vol_info_start) - 1));
        *(under_vol_info_str + (under_vol_info_end - under_vol_info_start)) = '\0';

        H5VLconnector_str_to_info(under_vol_info_str, under_vol_id, &under_vol_info);

        free(under_vol_info_str);
    } /* end else */

    /* Allocate new pass-through VOL connector info and set its fields */
    info = (hermes_info_t *) calloc(1, sizeof(hermes_info_t));
    info->under_vol_id = under_vol_id;
    info->under_vol_info = under_vol_info;

    /* Set return value */
    *_info = info;

    return (0);
}

void *
hermes_get_object(const void *obj) {
    const hermes_t *o = (const hermes_t *) obj;
    return (H5VLget_object(o->under_object, o->under_vol_id));
}

herr_t
hermes_get_wrap_ctx(const void *obj, void **wrap_ctx) {
    const hermes_t *o = (const hermes_t *) obj;
    hermes_wrap_ctx_t *new_wrap_ctx;
    /* Allocate new VOL object wrapping context for the pass through connector */
    new_wrap_ctx = (hermes_wrap_ctx_t *) calloc(1, sizeof(hermes_wrap_ctx_t));

    /* Increment reference count on underlying VOL ID, and copy the VOL info */
    new_wrap_ctx->under_vol_id = o->under_vol_id;
    H5Iinc_ref(new_wrap_ctx->under_vol_id);
    H5VLget_wrap_ctx(o->under_object, o->under_vol_id, &new_wrap_ctx->under_wrap_ctx);

    /* Set wrap context to return */
    *wrap_ctx = new_wrap_ctx;

    return (0);
}

void *
hermes_wrap_object(void *obj, H5I_type_t obj_type, void *_wrap_ctx) {
    hermes_wrap_ctx_t *wrap_ctx = (hermes_wrap_ctx_t *) _wrap_ctx;
    hermes_t *new_obj;
    void *under;

    /* Wrap the object with the underlying VOL */
    under = H5VLwrap_object(obj, obj_type, wrap_ctx->under_vol_id, wrap_ctx->under_wrap_ctx);
    if (under)
        new_obj = hermes_new_obj(under, wrap_ctx->under_vol_id,H5I_NTYPES);
    else
        new_obj = NULL;

    return (new_obj);
} /* end hermes_wrap_object() */

herr_t
hermes_free_wrap_ctx(void *_wrap_ctx) {
    hermes_wrap_ctx_t *wrap_ctx = (hermes_wrap_ctx_t *) _wrap_ctx;

    /* Release underlying VOL ID and wrap context */
    if (wrap_ctx->under_wrap_ctx)
        H5VLfree_wrap_ctx(wrap_ctx->under_wrap_ctx, wrap_ctx->under_vol_id);
    H5Idec_ref(wrap_ctx->under_vol_id);

    /* Free pass through wrap context object itself */
    free(wrap_ctx);

    return (0);
}

hid_t hermes_register(void) {
    /* Clear the error stack */
    H5Eclear2(H5E_DEFAULT);

    /* Singleton register the pass-through VOL connector ID */
    if (H5I_VOL != H5Iget_type(HERMES_g))
        HERMES_g = H5VLregister_connector(&hermes_g, H5P_DEFAULT);

    return (HERMES_g);
}


/* Hermes VOL Dataset callbacks */
void *hermes_dataset_create(void *obj, const H5VL_loc_params_t *loc_params,
                            const char *name, hid_t lcpl_id, hid_t type_id, hid_t space_id, hid_t dcpl_id,
                            hid_t dapl_id, hid_t dxpl_id, void **req) {
    hermes_t *dset;
    hermes_t *o = (hermes_t *) obj;
    void *under;

#ifdef ENABLE_LOGGING
    printf("------- PASS THROUGH VOL DATASET Create\n");
#endif

    under = H5VLdataset_create(o->under_object, loc_params, o->under_vol_id, name, lcpl_id, type_id, space_id, dcpl_id,
                               dapl_id, dxpl_id, req);
    if (under) {
        dset = hermes_new_obj(under, o->under_vol_id,H5I_DATASET);


        /* Check for async request */
        if (req && *req)
            *req = hermes_new_obj(*req, o->under_vol_id,H5I_DATASET);
        dset->file_name = o->file_name;
        dset->group_name = o->group_name;
        dset->dataset_name = strdup(name);
        hsize_t *dims, *max_dims;
        int rank_ = H5Sget_simple_extent_dims(space_id, NULL, NULL);
        dims = (hsize_t *) malloc(rank_ * sizeof(hsize_t));
        max_dims = (hsize_t *) malloc(rank_ * sizeof(hsize_t));
        H5Sget_simple_extent_dims(space_id, dims, max_dims);
        H5_BufferInit(dset->file_name, dset->dataset_name, rank_, type_id, dims, max_dims, dset,dset->under_vol_id);
        free(dims);
        free(max_dims);
    } /* end if */
    else
        dset = NULL;

    return ((void *) dset);
}

static void *hermes_dataset_open(void *obj, const H5VL_loc_params_t *loc_params,
                                 const char *name, hid_t dapl_id, hid_t dxpl_id, void **req) {
    hermes_t *dset;
    hermes_t *o = (hermes_t *) obj;
    void *under;
    under = H5VLdataset_open(o->under_object, loc_params, o->under_vol_id, name, dapl_id, dxpl_id, req);
    if (under) {
        dset = hermes_new_obj(under, o->under_vol_id,H5I_DATASET);

        /* Check for async request */
        if (req && *req)
            *req = hermes_new_obj(*req, o->under_vol_id,H5I_DATASET);

        //DE Code starts
        dset->file_name = o->file_name;
        dset->group_name = o->group_name;
        dset->dataset_name = strdup(name);
        hid_t file_space_id = H5Dget_space(dset->object_id);
        int rank_ = H5Sget_simple_extent_dims(file_space_id, NULL, NULL);
        hsize_t *dims, *max_dims;
        dims = (hsize_t *) malloc(rank_ * sizeof(hsize_t));
        max_dims = (hsize_t *) malloc(rank_ * sizeof(hsize_t));
        H5Sget_simple_extent_dims(file_space_id, dims, max_dims);
        int64_t type_ = H5Dget_type(dset->object_id);
        H5_BufferInit(dset->file_name, dset->group_name, rank_, type_, dims, max_dims, dset,dset->under_vol_id);
        free(dims);
        free(max_dims);
    } /* end if */
    else
        dset = NULL;
    return ((void *) dset);
}
void dataset_get_wrapper(void *dset, hid_t driver_id, H5VL_dataset_get_t get_type, hid_t dxpl_id, void **req, ...)
{
    va_list args;
    va_start(args, req);
    H5VLdataset_get(dset, driver_id, get_type, dxpl_id, req, args);
}

static herr_t
hermes_dataset_read(void *dset, hid_t mem_type_id, hid_t mem_space_id, hid_t file_space_id, hid_t plist_id, void *buf,
                    void **req) {
    hermes_t *o = (hermes_t *) dset;

    char *filename = basename(o->file_name);//char* filename=o->file_name;
    char *dataset_name = o->dataset_name;
    hsize_t *memory_start, *memory_dim, *file_start, *file_end;
    int rank_;
    if (file_space_id == 0) {
        dataset_get_wrapper(o->under_object, o->under_vol_id, H5VL_DATASET_GET_SPACE, plist_id, req, &file_space_id);
        rank_ = H5Sget_simple_extent_dims(file_space_id, NULL, NULL);
        file_start = (hsize_t *) malloc(rank_ * sizeof(hsize_t));
        file_end = (hsize_t *) malloc(rank_ * sizeof(hsize_t));
        rank_ = H5Sget_simple_extent_dims(file_space_id, file_end, NULL);
        int i;
        for (i = 0; i < rank_; i++) {
            file_start[i] = 0;
            file_end[i] -= 1;
        }
    } else {
        rank_ = H5Sget_simple_extent_dims(file_space_id, NULL, NULL);
        file_start = (hsize_t *) malloc(rank_ * sizeof(hsize_t));
        file_end = (hsize_t *) malloc(rank_ * sizeof(hsize_t));
        H5Sget_select_bounds(file_space_id, file_start, file_end);
    }
    if (mem_space_id == 0) {
        dataset_get_wrapper(o->under_object, o->under_vol_id, H5VL_DATASET_GET_SPACE, plist_id, req, &mem_space_id);
        rank_ = H5Sget_simple_extent_dims(mem_space_id, NULL, NULL);
        memory_start = (hsize_t *) malloc(rank_ * sizeof(hsize_t));
        memory_dim = (hsize_t *) malloc(rank_ * sizeof(hsize_t));
        rank_ = H5Sget_simple_extent_dims(mem_space_id, memory_dim, NULL);
        int i;
        for (i = 0; i < rank_; i++) {
            memory_start[i] = 0;
        }
    } else {
        rank_ = H5Sget_simple_extent_dims(mem_space_id, NULL, NULL);
        memory_start = (hsize_t *) malloc(rank_ * sizeof(hsize_t));
        memory_dim = (hsize_t *) malloc(rank_ * sizeof(hsize_t));
        H5Sget_select_bounds(mem_space_id, memory_start, memory_dim);
        H5Sget_simple_extent_dims(mem_space_id, memory_dim, NULL);
    }
    hid_t type_id;
    dataset_get_wrapper(o->under_object, o->under_vol_id, H5VL_DATASET_GET_TYPE, plist_id, req, &type_id);
    herr_t under = H5_BufferRead(filename, dataset_name, rank_, type_id, file_start, file_end, memory_start, memory_dim,
                                 o->under_object,o->under_vol_id, buf);
    free(file_start);
    free(file_end);
    free(memory_start);
    free(memory_dim);

    return (under);
}

static herr_t
hermes_dataset_write(void *dset, hid_t mem_type_id, hid_t mem_space_id, hid_t file_space_id, hid_t plist_id,
                     const void *buf, void **req) {
    hermes_t *o = (hermes_t *) dset;
    herr_t ret_value;

    char *filename = basename(o->file_name);//char* filename=o->file_name;
    char *dataset_name = o->dataset_name;
    hsize_t *memory_start, *memory_dim, *file_start, *file_end;
    int rank_;
    if (file_space_id == 0) {
        dataset_get_wrapper(o->under_object, o->under_vol_id, H5VL_DATASET_GET_SPACE, plist_id, req, &file_space_id);
        rank_ = H5Sget_simple_extent_dims(file_space_id, NULL, NULL);
        file_start = (hsize_t *) malloc(rank_ * sizeof(hsize_t));
        file_end = (hsize_t *) malloc(rank_ * sizeof(hsize_t));
        rank_ = H5Sget_simple_extent_dims(file_space_id, file_end, NULL);
        int i;
        for (i = 0; i < rank_; i++) {
            file_start[i] = 0;
            file_end[i] -= 1;
        }
    } else {
        rank_ = H5Sget_simple_extent_dims(file_space_id, NULL, NULL);
        file_start = (hsize_t *) malloc(rank_ * sizeof(hsize_t));
        file_end = (hsize_t *) malloc(rank_ * sizeof(hsize_t));
        H5Sget_select_bounds(file_space_id, file_start, file_end);
    }
    if (mem_space_id == 0) {
        dataset_get_wrapper(o->under_object, o->under_vol_id, H5VL_DATASET_GET_SPACE, plist_id, req, &mem_space_id);
        rank_ = H5Sget_simple_extent_dims(mem_space_id, NULL, NULL);
        memory_start = (hsize_t *) malloc(rank_ * sizeof(hsize_t));
        memory_dim = (hsize_t *) malloc(rank_ * sizeof(hsize_t));
        rank_ = H5Sget_simple_extent_dims(mem_space_id, memory_dim, NULL);
        int i;
        for (i = 0; i < rank_; i++) {
            memory_start[i] = 0;
        }
    } else {
        rank_ = H5Sget_simple_extent_dims(mem_space_id, NULL, NULL);
        memory_start = (hsize_t *) malloc(rank_ * sizeof(hsize_t));
        memory_dim = (hsize_t *) malloc(rank_ * sizeof(hsize_t));
        H5Sget_select_bounds(mem_space_id, memory_start, memory_dim);
        H5Sget_simple_extent_dims(mem_space_id, memory_dim, NULL);
    }
    hid_t type_id;
    dataset_get_wrapper(o->under_object, o->under_vol_id, H5VL_DATASET_GET_TYPE, plist_id, req, &type_id);
    ret_value = H5_BufferWrite(filename, dataset_name, rank_, type_id, file_start, file_end, memory_start, memory_dim,
                               o->under_object,o->under_vol_id,
                               (void *) (buf));
    free(file_start);
    free(file_end);
    free(memory_start);
    free(memory_dim);

    return (ret_value);
}

static herr_t
hermes_dataset_get(void *dset, H5VL_dataset_get_t get_type, hid_t dxpl_id, void **req, va_list arguments) {
    hermes_t *o = (hermes_t *) dset;
    herr_t ret_value;
    ret_value = H5VLdataset_get(o->under_object, o->under_vol_id, get_type, dxpl_id, req, arguments);

    /* Check for async request */
    if (req && *req)
        *req = hermes_new_obj(*req, o->under_vol_id,H5I_NTYPES);

    return (ret_value);
}

herr_t
hermes_dataset_specific(void *obj, H5VL_dataset_specific_t specific_type,
                        hid_t dxpl_id, void **req, va_list arguments) {
    hermes_t *o = (hermes_t *) obj;
    herr_t ret_value;
    ret_value = H5VLdataset_specific(o->under_object, o->under_vol_id, specific_type, dxpl_id, req, arguments);

    /* Check for async request */
    if (req && *req)
        *req = hermes_new_obj(*req, o->under_vol_id,H5I_NTYPES);

    return (ret_value);
}

herr_t
hermes_dataset_optional(void *obj, hid_t dxpl_id, void **req,
                        va_list arguments) {
    hermes_t *o = (hermes_t *) obj;
    herr_t ret_value;
    ret_value = H5VLdataset_optional(o->under_object, o->under_vol_id, dxpl_id, req, arguments);

    /* Check for async request */
    if (req && *req)
        *req = hermes_new_obj(*req, o->under_vol_id,H5I_NTYPES);

    return (ret_value);
}

static herr_t hermes_dataset_close(void *dset, hid_t dxpl_id, void **req) {
    hermes_t *o = (hermes_t *) dset;
    if(o->object_id==0)
        return 0;
    herr_t ret_value;

    hsize_t *dims, *max_dims;
    int rank_;
    hid_t file_space_id;
    dataset_get_wrapper(o->under_object, o->under_vol_id, H5VL_DATASET_GET_SPACE, dxpl_id, req, &file_space_id);
    rank_ = H5Sget_simple_extent_dims(file_space_id, NULL, NULL);
    dims = (hsize_t *) malloc(rank_ * sizeof(hsize_t));
    max_dims = (hsize_t *) malloc(rank_ * sizeof(hsize_t));
    rank_ = H5Sget_simple_extent_dims(file_space_id, dims, max_dims);
    char *filename = basename(o->file_name);
    //if(o->sync)
    H5_BufferSync(filename, o->dataset_name, rank_, dims, max_dims, o->under_object,o->under_vol_id);
    ret_value = H5VLdataset_close(o->under_object, o->under_vol_id, dxpl_id, req);
    /* Check for async request */
    if (req && *req)
        *req = hermes_new_obj(*req, o->under_vol_id,H5I_DATASET);
    free(dims);
    free(max_dims);

    free(o->dataset_name);
    o->dataset_name = NULL;
    return (ret_value);
}

/* Hermes VOL File callbacks */
static void *
hermes_file_create(const char *name, unsigned flags, hid_t fcpl_id, hid_t fapl_id, hid_t dxpl_id, void **req) {
    hermes_info_t *info;
    hermes_t *file;
    hid_t under_fapl_id;
    void *under;
    /* Get copy of our VOL info from FAPL */
    H5Pget_vol_info(fapl_id, (void **) &info);
    if(!info){
        info=malloc(sizeof(hermes_info_t));
        info->under_vol_id=H5VLget_connector_id(H5VL_NATIVE_NAME);
    }

    /* Copy the FAPL */
    under_fapl_id = H5Pcopy(fapl_id);

    /* Set the VOL ID and info for the underlying FAPL */
    H5Pset_vol(under_fapl_id, info->under_vol_id, NULL);

    /* Open the file with the underlying VOL connector */
    under = H5VLfile_create(name, flags, fcpl_id, under_fapl_id, dxpl_id, req);
    if (under) {
        file = hermes_new_obj(under, info->under_vol_id,H5I_NTYPES);

        /* Check for async request */
        if (req && *req)
            *req = hermes_new_obj(*req, info->under_vol_id,H5I_NTYPES);
        file->file_name = strdup(name);
    } /* end if */
    else
        file = NULL;



    /* Close underlying FAPL */
    H5Pclose(under_fapl_id);

    /* Release copy of our VOL info */
    hermes_info_free(info);

    return ((void *) file);
}

static void *hermes_file_open(const char *name, unsigned flags, hid_t fapl_id, hid_t dxpl_id, void **req) {
    hermes_info_t *info;
    hermes_t *file;
    hid_t under_fapl_id;
    void *under;

    /* Get copy of our VOL info from FAPL */
    H5Pget_vol_info(fapl_id, (void **) &info);
    if(!info){
        info=malloc(sizeof(hermes_info_t));
        info->under_vol_id=H5VLget_connector_id(H5VL_NATIVE_NAME);
    }

    /* Copy the FAPL */
    under_fapl_id = H5Pcopy(fapl_id);

    /* Set the VOL ID and info for the underlying FAPL */
    H5Pset_vol(under_fapl_id, info->under_vol_id, NULL);

    /* Open the file with the underlying VOL connector */
    under = H5VLfile_open(name, flags, under_fapl_id, dxpl_id, req);
    if (under) {
        file = hermes_new_obj(under, info->under_vol_id,H5I_FILE);
        /* Check for async request */
        if (req && *req)
            *req = hermes_new_obj(*req, info->under_vol_id,H5I_FILE);

        file->file_name = strdup(name);
    } /* end if */
    else {
        file = NULL;
    }

    /* Close underlying FAPL */
    H5Pclose(under_fapl_id);

    /* Release copy of our VOL info */
    hermes_info_free(info);

    return ((void *) file);
}

herr_t
hermes_file_get(void *file, H5VL_file_get_t get_type, hid_t dxpl_id,
                void **req, va_list arguments) {
    hermes_t *o = (hermes_t *) file;
    herr_t ret_value;
    ret_value = H5VLfile_get(o->under_object, o->under_vol_id, get_type, dxpl_id, req, arguments);

    /* Check for async request */
    if (req && *req)
        *req = hermes_new_obj(*req, o->under_vol_id,H5I_FILE);

    return (ret_value);
}

herr_t hermes_file_specific_reissue(void *obj, hid_t connector_id,
                                    H5VL_file_specific_t specific_type, hid_t dxpl_id, void **req, ...) {
    va_list arguments;
    herr_t ret_value;

    va_start(arguments, req);
    ret_value = H5VLfile_specific(obj, connector_id, specific_type, dxpl_id, req, arguments);
    va_end(arguments);

    return (ret_value);
}

herr_t
hermes_file_specific(void *file, H5VL_file_specific_t specific_type,
                     hid_t dxpl_id, void **req, va_list arguments) {
    hermes_t *o = (hermes_t *) file;
    hid_t under_vol_id = -1;
    herr_t ret_value;

    /* Unpack arguments to get at the child file pointer when mounting a file */
    if (specific_type == H5VL_FILE_MOUNT) {
        H5I_type_t loc_type;
        const char *name;
        hermes_t *child_file;
        hid_t plist_id;

        /* Retrieve parameters for 'mount' operation, so we can unwrap the child file */
        loc_type = va_arg(arguments, H5I_type_t);
        name = va_arg(arguments, const char *);
        child_file = (hermes_t *) va_arg(arguments, void *);
        plist_id = va_arg(arguments, hid_t);

        /* Keep the correct underlying VOL ID for possible async request token */
        under_vol_id = o->under_vol_id;

        /* Re-issue 'file specific' call, using the unwrapped pieces */
        ret_value = hermes_file_specific_reissue(o->under_object, o->under_vol_id, specific_type, dxpl_id, req,
                                                 loc_type, name, child_file->under_object, plist_id);
    } /* end if */
    else if (specific_type == H5VL_FILE_IS_ACCESSIBLE) {
        hermes_info_t *info;
        hid_t fapl_id, under_fapl_id;
        const char *name;
        htri_t *ret;

        /* Get the arguments for the 'is accessible' check */
        fapl_id = va_arg(arguments, hid_t);
        name = va_arg(arguments, const char *);
        ret = va_arg(arguments, htri_t *);

        /* Get copy of our VOL info from FAPL */
        H5Pget_vol_info(fapl_id, (void **) &info);

        /* Copy the FAPL */
        under_fapl_id = H5Pcopy(fapl_id);

        /* Set the VOL ID and info for the underlying FAPL */
        H5Pset_vol(under_fapl_id, info->under_vol_id, info->under_vol_info);

        /* Keep the correct underlying VOL ID for possible async request token */
        under_vol_id = info->under_vol_id;

        /* Re-issue 'file specific' call */
        ret_value = hermes_file_specific_reissue(NULL, info->under_vol_id, specific_type, dxpl_id, req, under_fapl_id,
                                                 name, ret);

        /* Close underlying FAPL */
        H5Pclose(under_fapl_id);

        /* Release copy of our VOL info */
        hermes_info_free(info);
    } /* end else-if */
    else {
        va_list my_arguments;

        /* Make a copy of the argument list for later, if reopening */
        if (specific_type == H5VL_FILE_REOPEN)
            va_copy(my_arguments, arguments);

        /* Keep the correct underlying VOL ID for possible async request token */
        under_vol_id = o->under_vol_id;

        ret_value = H5VLfile_specific(o->under_object, o->under_vol_id, specific_type, dxpl_id, req, arguments);

        /* Wrap file struct pointer, if we reopened one */
        if (specific_type == H5VL_FILE_REOPEN) {
            if (ret_value >= 0) {
                void **ret = va_arg(my_arguments, void **);

                if (ret && *ret)
                    *ret = hermes_new_obj(*ret, o->under_vol_id,H5I_NTYPES);
            } /* end if */

            /* Finish use of copied vararg list */
            va_end(my_arguments);
        } /* end if */
    }     /* end else */

    /* Check for async request */
    if (req && *req)
        *req = hermes_new_obj(*req, under_vol_id,H5I_NTYPES);

    return (ret_value);
}

herr_t hermes_file_optional(void *file, hid_t dxpl_id, void **req,
                            va_list arguments) {
    hermes_t *o = (hermes_t *) file;
    herr_t ret_value;

    ret_value = H5VLfile_optional(o->under_object, o->under_vol_id, dxpl_id, req, arguments);

    /* Check for async request */
    if (req && *req)
        *req = hermes_new_obj(*req, o->under_vol_id,H5I_NTYPES);

    return (ret_value);
}

herr_t
hermes_file_close(void *file, hid_t dxpl_id, void **req) {
    hermes_t *o = (hermes_t *) file;
    herr_t ret_value;
    ret_value = H5VLfile_close(o->under_object, o->under_vol_id, dxpl_id, req);

    /* Check for async request */
    if (req && *req) *req = hermes_new_obj(*req, o->under_vol_id,H5I_FILE);

    /* Release our wrapper, if underlying file was closed */
    if (ret_value >= 0) {
        free(o->file_name);
        o->file_name = NULL;
        hermes_free_obj(o);
    }

    return (ret_value);
}


H5PL_type_t H5PLget_plugin_type(void) {
    return H5PL_TYPE_VOL;
}


const void *H5PLget_plugin_info(void) {
    return &hermes_g;
}

void *
hermes_group_create(void *obj, const H5VL_loc_params_t *loc_params,
                    const char *name, hid_t lcpl_id, hid_t gcpl_id, hid_t gapl_id, hid_t dxpl_id, void **req) {
    hermes_t *group;
    hermes_t *o = (hermes_t *) obj;
    void *under;

#ifdef ENABLE_LOGGING
    printf("------- PASS THROUGH VOL GROUP Create\n");
#endif

    under = H5VLgroup_create(o->under_object, loc_params, o->under_vol_id, name, lcpl_id, gcpl_id, gapl_id, dxpl_id,
                             req);
    if (under) {
        group = hermes_new_obj(under, o->under_vol_id,H5I_GROUP);

        /* Check for async request */
        if (req && *req)
            *req = hermes_new_obj(*req, o->under_vol_id,H5I_GROUP);
        group->file_name = o->file_name;
        group->group_name = strdup(name);
    } /* end if */
    else
        group = NULL;

    return ((void *) group);
} /* end hermes_group_create() */

void *
hermes_group_open(void *obj, const H5VL_loc_params_t *loc_params,
                  const char *name, hid_t gapl_id, hid_t dxpl_id, void **req) {
    hermes_t *group;
    hermes_t *o = (hermes_t *) obj;
    void *under;

    under = H5VLgroup_open(o->under_object, loc_params, o->under_vol_id, name, gapl_id, dxpl_id, req);
    if (under) {
        group = hermes_new_obj(under, o->under_vol_id,H5I_GROUP);

        /* Check for async request */
        if (req && *req)
            *req = hermes_new_obj(*req, o->under_vol_id,H5I_GROUP);


        group->file_name = o->file_name;
        group->group_name = strdup(name);


    } /* end if */
    else
        group = NULL;

    return ((void *) group);
} /* end hermes_group_open() */

herr_t
hermes_group_get(void *obj, H5VL_group_get_t get_type, hid_t dxpl_id,
                 void **req, va_list arguments) {
    hermes_t *o = (hermes_t *) (obj);
    herr_t ret_value;

    ret_value = H5VLgroup_get(o->under_object, o->under_vol_id, get_type, dxpl_id, req, arguments);

    /* Check for async request */
    if (req && *req)
        *req = hermes_new_obj(*req, o->under_vol_id,H5I_NTYPES);

    return (ret_value);
} /* end hermes_group_get() */

/*-------------------------------------------------------------------------
 * Function:    hermes_group_specific
 *
 * Purpose: Specific operation on a group
 *
 * Return:  Success:    0
 *      Failure:    -1
 *
 *-------------------------------------------------------------------------
 */
herr_t
hermes_group_specific(void *obj, H5VL_group_specific_t specific_type,
                      hid_t dxpl_id, void **req, va_list arguments) {
    hermes_t *o = (hermes_t *) obj;
    herr_t ret_value;

#ifdef ENABLE_LOGGING
    printf("------- PASS THROUGH VOL GROUP Specific\n");
#endif

    ret_value = H5VLgroup_specific(o->under_object, o->under_vol_id, specific_type, dxpl_id, req, arguments);

    /* Check for async request */
    if (req && *req)
        *req = hermes_new_obj(*req, o->under_vol_id,H5I_NTYPES);

    return (ret_value);
} /* end hermes_group_specific() */

/*-------------------------------------------------------------------------
 * Function:    hermes_group_optional
 *
 * Purpose:     Perform a connector-specific operation on a group
 *
 * Return:  Success:    0
 *      Failure:    -1
 *
 *-------------------------------------------------------------------------
 */
herr_t
hermes_group_optional(void *obj, hid_t dxpl_id, void **req,
                      va_list arguments) {
    hermes_t *o = (hermes_t *) obj;
    herr_t ret_value;

#ifdef ENABLE_LOGGING
    printf("------- PASS THROUGH VOL GROUP Optional\n");
#endif

    ret_value = H5VLgroup_optional(o->under_object, o->under_vol_id, dxpl_id, req, arguments);

    /* Check for async request */
    if (req && *req)
        *req = hermes_new_obj(*req, o->under_vol_id,H5I_NTYPES);

    return (ret_value);
} /* end hermes_group_optional() */

/*-------------------------------------------------------------------------
 * Function:    hermes_group_close
 *
 * Purpose:     Closes a group.
 *
 * Return:  Success:    0
 *      Failure:    -1, group not closed.
 *
 *-------------------------------------------------------------------------
 */
herr_t
hermes_group_close(void *grp, hid_t dxpl_id, void **req) {
    hermes_t *o = (hermes_t *) grp;
    herr_t ret_value;

    if(o->object_id==0)
        return 0;

    ret_value = H5VLgroup_close(o->under_object, o->under_vol_id, dxpl_id, req);

    /* Check for async request */
    if (req && *req)
        *req = hermes_new_obj(*req, o->under_vol_id,H5I_NTYPES);

    free(o->group_name);
    o->group_name = NULL;
    return (ret_value);
}

void *hermes_attribute_create(void *obj, const H5VL_loc_params_t *loc_params, const char *name, hid_t type_id,
                              hid_t space_id, hid_t acpl_id, hid_t aapl_id, hid_t dxpl_id, void **req) {
    hermes_t *attr;
    hermes_t *o = (hermes_t *) obj;
    void *under;

    under = H5VLattr_create(o->under_object, loc_params, o->under_vol_id, name, type_id, space_id, acpl_id, aapl_id, dxpl_id, req);
    if (under) {
        attr = hermes_new_obj(under, o->under_vol_id,H5I_ATTR);

        /* Check for async request */
        if (req && *req)
            *req = hermes_new_obj(*req, o->under_vol_id,H5I_ATTR);
    } /* end if */
    else
        attr = NULL;

    return ((void *) attr);
}

void *
hermes_attribute_open(void *obj, const H5VL_loc_params_t *loc_params, const char *name, hid_t aapl_id, hid_t dxpl_id,
                      void **req) {
    hermes_t *attr;
    hermes_t *o = (hermes_t *) obj;
    void *under;

    under = H5VLattr_open(o->under_object, loc_params, o->under_vol_id, name, aapl_id, dxpl_id, req);
    if (under) {
        attr = hermes_new_obj(under, o->under_vol_id,H5I_ATTR);

        /* Check for async request */
        if (req && *req)
            *req = hermes_new_obj(*req, o->under_vol_id,H5I_ATTR);
    } /* end if */
    else
        attr = NULL;

    return ((void *) attr);
}

herr_t hermes_attribute_read(void *_attr, hid_t mem_type_id, void *buf, hid_t dxpl_id, void **req) {
    hermes_t *attr;
    hermes_t *o = (hermes_t *) _attr;
    herr_t ret_value;
    ret_value = H5VLattr_read(o->under_object,o->under_vol_id, mem_type_id, buf, dxpl_id, req);
    /* Check for async request */
    if (req && *req)
        *req = hermes_new_obj(*req, o->under_vol_id,H5I_NTYPES);

    return ret_value;
}

herr_t hermes_attribute_write(void *_attr, hid_t mem_type_id, const void *buf, hid_t dxpl_id, void **req) {
    hermes_t *attr;
    hermes_t *o = (hermes_t *) _attr;
    herr_t ret_value;
    ret_value = H5VLattr_write(o->under_object,o->under_vol_id, mem_type_id, buf, dxpl_id, req);
    /* Check for async request */
    if (req && *req)
        *req = hermes_new_obj(*req, o->under_vol_id,H5I_NTYPES);

    return ret_value;
}

herr_t hermes_attribute_get(void *_attr, H5VL_attr_get_t get_type, hid_t dxpl_id, void **req, va_list arguments) {
    hermes_t *attr;
    hermes_t *o = (hermes_t *) _attr;
    herr_t ret_value;
    ret_value = H5VLattr_get(o->under_object,o->under_vol_id, get_type, dxpl_id, req,arguments);
    /* Check for async request */
    if (req && *req)
        *req = hermes_new_obj(*req, o->under_vol_id,H5I_NTYPES);

    return ret_value;
}

herr_t hermes_attribute_specific(void *_attr, const H5VL_loc_params_t *loc_params, H5VL_attr_specific_t specific_type,
                                 hid_t dxpl_id, void **req, va_list arguments) {
    hermes_t *attr;
    hermes_t *o = (hermes_t *) _attr;
    herr_t ret_value;
    ret_value = H5VLattr_specific(o->under_object,loc_params, o->under_vol_id,  specific_type,dxpl_id, req, arguments);
    /* Check for async request */
    if (req && *req)
        *req = hermes_new_obj(*req, o->under_vol_id,H5I_NTYPES);

    return ret_value;
}

herr_t hermes_attribute_close(void *_attr, hid_t dxpl_id, void **req) {
    hermes_t *attr;
    hermes_t *o = (hermes_t *) _attr;
    if(o->object_id==0)
        return 0;
    herr_t ret_value;
    ret_value = H5VLattr_close(o->under_object,o->under_vol_id, dxpl_id, req);
    /* Check for async request */
    if (req && *req)
        *req = hermes_new_obj(*req, o->under_vol_id,H5I_NTYPES);

    return ret_value;
}
/* end hermes_group_close() */
