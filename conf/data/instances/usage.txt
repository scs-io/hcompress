#Normal(100,4)
mpirun -n ${mpiprocs} ${exec} -p 0#100#4 ${other_args}
#Gamma(2,25)
mpirun -n ${mpiprocs} ${exec} -p 1#2#25 ${other_args}
#Exponential(2,.1)
mpirun -n ${mpiprocs} ${exec} -p 2#.1 ${other_args}
#Uniform(0,50)
mpirun -n ${mpiprocs} ${exec} -p 3#0#50 ${other_args}

NOTE:
-p [distribution]#[param1]#[param2 (sometimes)]
    For example, exponential distribution only has 1 parameter.
exec is one of h5_buffer_write*
