/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
 * <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of HCompress
 * 
 * HCompress is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
//
// Created by lukemartinlogan on 8/5/19.
//

/**
 * This file is used to generate configuration files
 * for Hermes using template configurations.
 *
 * Usage:
 *  -i [/path/to/template_directory]
 *  -o [/path/to/output_directory]
 * */

#include <iostream>
#include <cmath>
#include <conf/configuration_generator.h>

int main(int argc, char **argv)
{
    ConfigurationGenerator args(argc, argv);
    for(auto conf : args.templates) {
        LayerInfo *layers;
        int num_layers;
        bool is_template;

        if(!LoadTemplate(conf.input, is_template, layers, num_layers))
            continue;
        if(is_template)
            ModifyTemplate(layers, num_layers, conf.net_size);
        SaveInstance(conf.output, layers, num_layers);

        free(layers);
    }
}

bool LoadTemplate(std::string template_path, bool &is_template, LayerInfo *&layers, int &num_layers)
{
    //Open template file
    FILE *template_file = fopen(template_path.c_str(), "r");
    if(template_file == NULL) {
        std::cout << "Invalid template path: " << template_path << std::endl;
        return false;
    }

    //Initialize read stream
    char buf[65536];
    rapidjson::FileReadStream instream(template_file, buf, sizeof(buf));
    rapidjson::Document d;

    //Get JSON object
    d.ParseStream(instream);
    if(!d.IsObject()) {
        std::cout << "Template file is invalid" << std::endl;
        fclose(template_file);
        return false;
    }

    //Check if this file is a template file or if it should be copied
    is_template = false;
    if(d.HasMember("is_template")) {
        if(d["is_template"] == 1)
            is_template = true;
    }

    //Read in layers
    assert(d["layer_info"].IsArray());
    num_layers = d["layer_info"].Size();
    layers = (LayerInfo*)malloc(num_layers * sizeof(LayerInfo));
    int i = 0;
    for (rapidjson::Value::ConstValueIterator itr = d["layer_info"].Begin(); itr != d["layer_info"].End(); ++itr) {
        const rapidjson::Value& attr = (rapidjson::Value&)*itr;
        layers[i].capacity_mb_= attr["capacity_mb"].GetFloat();
        layers[i].bandwidth= attr["bandwidth"].GetFloat();
        layers[i].is_memory= attr["is_memory"].GetInt();
        strcpy(layers[i].mount_point_, attr["mount_point"].GetString());
        layers[i].direct_io= attr["direct_io"].GetInt();
        i++;
    }

    return true;
}

void ModifyTemplate(LayerInfo *layers, int num_layers, size_t net_size)
{
    if(num_layers == 4) {
        layers[0].capacity_mb_ = net_size * .2;
        layers[1].capacity_mb_ = net_size * .3;
        layers[2].capacity_mb_ = net_size;
        return;
    }

    size_t overflow = 0;

    //sum(1 + BW1/BW2 +...+ BW1/BWn)
    double bw_ratio_sum = 1;
    for(int i = 1; i < num_layers - 1; i++)
        bw_ratio_sum += (double)layers[0].bandwidth/layers[i].bandwidth;

    //First layer capacity
    if(num_layers > 0) {
        size_t est_cap = (size_t)std::ceil(net_size/bw_ratio_sum);
        if(est_cap < layers[0].capacity_mb_) {
            layers[0].capacity_mb_ = est_cap;
            overflow = 0;
        }
        else {
            overflow = est_cap - layers[0].capacity_mb_;
        }
    }

    //Set capacity proportional to bandwidth of the previous layer
    for(int i = 1; i < num_layers - 1; i++) {
        size_t est_cap = (size_t)std::ceil(layers[0].capacity_mb_*((double)layers[0].bandwidth/layers[i].bandwidth)) + overflow;
        if(est_cap < layers[i].capacity_mb_) {
            layers[i].capacity_mb_ = est_cap;
            overflow = 0;
        }
        else {
            overflow = est_cap - layers[i].capacity_mb_;
        }
    }
}

void SaveInstance(std::string output_path, LayerInfo *layers, int num_layers)
{
    int i;
    FILE *output_file = fopen(output_path.c_str(), "w");
    if (output_file == NULL) {
        std::cout << "Cannot write instance to this location: " << output_path << std::endl;
        return;
    }

    char buf[65536];

    rapidjson::Document d;
    rapidjson::Value layers_json(rapidjson::kArrayType);
    rapidjson::FileWriteStream outstream(output_file, buf, sizeof(buf));
    rapidjson::Writer <rapidjson::FileWriteStream> writer(outstream);
    rapidjson::Document::AllocatorType& allocator = d.GetAllocator();

    d.SetObject();
    layers_json.SetArray();
    for(int i = 0; i < num_layers; i++) {
        rapidjson::Value layer_json(rapidjson::kObjectType);
        layer_json.AddMember("capacity_mb", layers[i].capacity_mb_, allocator);
        layer_json.AddMember("bandwidth", layers[i].bandwidth, allocator);
        layer_json.AddMember("is_memory", static_cast<int>(layers[i].is_memory), allocator);
        rapidjson::Value mount_point(layers[i].mount_point_, allocator);
        layer_json.AddMember("mount_point", mount_point, allocator);
        layer_json.AddMember("direct_io", static_cast<int>(layers[i].direct_io), allocator);
        layers_json.PushBack(layer_json, allocator);
    }
    d.AddMember("layer_info", layers_json, allocator);

    d.Accept(writer);
    fclose(output_file);
}
