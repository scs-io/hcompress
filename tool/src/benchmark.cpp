/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
 * <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of HCompress
 * 
 * HCompress is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

/*
This tool tests multiple different compression algorithms
on HDF5 files of different formats. For each data type, we 
will create a JSON file containing the following information:

[compress_algo][dataset_type][uncompressed_size][compressed_size][compress_ratio][compress_time][decompress_time]

USAGE:
	-h Print out help for this program
	-d "/path/to": Intermediate directory for storing data
	-f "/path/to/file.5": Run benchmark on existing HDF5 file
	-g count type1 type2 ...: Generate a file with datasets type1, type2, ... each with the same number of elements
	-o "/path/to/output.json": Set benchmarking data output file
	-p distribution param1 param2
	
TYPES:
	1: Generate dataset of characters
	2: Generate dataset of 32-bit signed integers
	3: Generate dataset of sorted 32-bit signed integers
	4: Generate dataset of 32-bit unsigned integers
	5: Generate dataset of sorted 32-bit unsigned integers
	6: Generate dataset of floats
	7: Generate dataset of doubles

 NOTE:
    RICE is not well-implemented. It is dysfunctional for small datasets.
    The implementation of the RICE codec needs updating to account for
    this. We should fix it, skip it, or verify its output.
*/

#include <stdio.h>
#include <iostream>
#include <algorithm>
#include <cstring>
#include <string>
#include <stdlib.h>

#include <hdf5.h>

#include <ares/c++/ares.h>
#include <ares/datasets/include/dataset_generator.h>
#include <ares_filter/compression_metrics_manager.h>
#include "../include/benchmark.h"
#include "../include/time_counter.h"

using namespace std;

int main(int argc, char **argv)
{
    MPI_Init(NULL, NULL);
	uint64_t id = 0;
	putenv("HDF5_PLUGIN_PATH=");
	putenv("HDF5_VOL_CONNECTOR=");

	if(argc <= 1)
	{
		cout << "Error: no inputs\n";
		exit(1);
	}

	//Parse the argument vector
	BenchmarkArgs args(argc, argv);
	
	//Perform benchmarks for existing files
	for(auto path : args.fargs)
	{
        hid_t file_id = H5Fopen(path.c_str(), H5F_ACC_RDONLY, H5P_DEFAULT);
        hid_t group_id = H5Gopen1(file_id, "/");

        benchmark_h5(group_id, args.output_file);

        H5Gclose(group_id);
        H5Fclose(file_id);
    }
	
	//Generate file and benchmark it
	for(auto datasets : args.gargs)
	{
		string path = hdf5_gen(args, datasets, args.intermed_dir, id);
        hid_t file_id = H5Fopen(path.c_str(), H5F_ACC_RDONLY, H5P_DEFAULT);
        hid_t group_id = H5Gopen1(file_id, "/");

		benchmark_h5(group_id, args.output_file);
		remove(path.c_str());

        H5Gclose(group_id);
        H5Fclose(file_id);
		id++;
	}
}

string hdf5_gen(BenchmarkArgs args, GenFileArg datasets, string intermed_dir, uint64_t id)
{
	hid_t file_id, dataset_id, dataspace_id;
	hsize_t dims[1];
	herr_t  status;
	string dataset_base = "/dset";
	
	/* Create a new file using default properties. */
	string filename = intermed_dir + to_string(id) + ".h5";
	file_id = H5Fcreate(filename.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
	
	int i = 0;
	for(auto dataset : datasets)
	{
		/* Create dataset. */
		void* buffer = generate_data(args, dataset);
		
		/* Create the data space for the dataset.*/
		dims[0] = dataset.count;
		dataspace_id = H5Screate_simple(1, dims, NULL);
		
		/* Create the dataset. */
		string dataset_name = dataset_base + to_string(i);
		dataset_id = H5Dcreate2(file_id, dataset_name.c_str(), dataset.type, dataspace_id,
								H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
		i++;
		
		/* Write the dataset. */
		status = H5Dwrite(dataset_id, dataset.type, H5S_ALL, H5S_ALL, H5P_DEFAULT, buffer);
		free(buffer);
		
		/* End access to the dataset and release resources used by it. */
		status = H5Dclose(dataset_id);

		/* Terminate access to the data space. */
		status = H5Sclose(dataspace_id);
	}
	
	/* Close the file. */
	status = H5Fclose(file_id);
	
    return filename;
}

void *generate_data(BenchmarkArgs args, Dataset dataset)
{
    void* data;

    switch (dataset.type_index) {
        case 1: {
            data = (void*)DatasetGenerator<char>::RandomDistribution(args.probability_type, dataset.count, args.probability_args[0], args.probability_args[1]);
            break;
        }
        case 2: {
            data = (void*)DatasetGenerator<int32_t>::RandomDistribution(args.probability_type, dataset.count, args.probability_args[0], args.probability_args[1]);
            break;
        }
        case 3: {
            data = (void*)DatasetGenerator<int32_t>::RandomDistribution(args.probability_type, dataset.count, args.probability_args[0], args.probability_args[1]);
            std::sort((int32_t*)data, (int32_t*)data + dataset.count);
            break;
        }
        case 4:{
            data = (void*)DatasetGenerator<uint32_t>::RandomDistribution(args.probability_type, dataset.count, args.probability_args[0], args.probability_args[1]);
            break;
        }
        case 5: {
            data = (void*)DatasetGenerator<uint32_t>::RandomDistribution(args.probability_type, dataset.count, args.probability_args[0], args.probability_args[1]);
            std::sort((uint32_t*)data, (uint32_t*)data + dataset.count);
            break;
        }
        case 6: {
            data = (void*)DatasetGenerator<float>::RandomDistribution(args.probability_type, dataset.count, args.probability_args[0], args.probability_args[1]);
            break;
        }
        case 7: {
            data = (void*)DatasetGenerator<double>::RandomDistribution(args.probability_type, dataset.count, args.probability_args[0], args.probability_args[1]);
            break;
        }
    }
	
    return data;
}

void benchmark_h5(hid_t group_id, string output_file)
{
    hid_t dataset_id, dataspace_id;
    hsize_t nobj;
    herr_t err;
    char ds_name[1024], memb_name[1024];

    // Process the objects of the group & determine which are datasets
    err = H5Gget_num_objs(group_id, &nobj);
    for (int i = 0; i < nobj; i++) {
        H5Gget_objname_by_idx(group_id, (hsize_t)i, memb_name, (size_t)1024);

        switch(H5Gget_objtype_by_idx(group_id, (size_t)i)) {
            case H5G_DATASET: {
                //Open dataset
                dataset_id = H5Dopen1(group_id, memb_name);
                H5Iget_name(dataset_id, ds_name, 1024);
                printf("Dataset Name: %s\n", ds_name);

                // Read dataset properties
                dataspace_id = H5Dget_space(dataset_id);
                hid_t type = H5Dget_type(dataset_id);
                int64_t type_id = Singleton<CompressionMetricsManager>::GetInstance()->GetDatasetType(type);
                size_t size = H5Dget_storage_size(dataset_id);

                cout << "SIZE OF DS: " << size << endl;

                //Read dataset into memory
                char *buf = (char *) malloc(size);
                H5Dread(dataset_id, type, H5S_ALL, dataspace_id, H5P_DEFAULT, buf);
                H5Dclose(dataset_id);
                H5Tclose(type);

                //Benchmark dataset
                benchmark(type_id, size, buf, output_file);
                free(buf);
            }
            break;

            case H5G_GROUP: {
                hid_t new_group = H5Gopen1(group_id, memb_name);
                benchmark_h5(new_group, output_file);
                H5Gclose(new_group);
            }
            break;
        }
    }
}

void benchmark(int64_t type, size_t ds_size, char *dataset, string output_file)
{
    AresData ares_data;
	size_t uncomp_size[2], comp_size;
	void *comp, *uncomp[2] = {0};

	//Get uncompressed dataset
	uncomp_size[0] = ds_size;
	uncomp[0] = dataset;

	//Benchmark dataset for each compression algorithm
    for(int lib_id = 1; lib_id < NUM_ARES_LIBRARIES; lib_id++)
    {
        cout << "LIBRARY: " << lib_id << endl;

        //Get compression library
        ares_data.setLib(lib_id);

        //Compress dataset
        if(!ares_compress(uncomp[0], uncomp_size[0], comp, comp_size, &ares_data))
            continue;
        fflush(stdout);
        std::flush(std::cerr);

        //Decompress dataset
        if(!ares_decompress(comp, comp_size, uncomp[1], uncomp_size[1], &ares_data))
        {
            free(comp);
            continue;
        }

        //Verify the data is unchanged
        if(uncomp_size[0] != uncomp_size[1]) {
            cout << "Restore failed -- didn't fully decompress" << endl;
            fflush(stdout);
            std::flush(std::cerr);
            continue;
        }
        if(strncmp((char*)uncomp[0], (char*)uncomp[1], uncomp_size[0]) != 0) {
            cout << "Restore failed -- bad data" << endl;
            fflush(stdout);
            std::flush(std::cerr);
            continue;
        }
        else {
            cout << "Restore success" << endl;
            fflush(stdout);
            std::flush(std::cerr);
        }

        //Free temporary buffers
        free(comp);
        if(uncomp[0] != uncomp[1])
            free(uncomp[1]);

        //Save metrics
        Singleton<CompressionMetricsManager>::GetInstance()->AddCompressionMetrics(
            type, lib_id,
            uncomp_size[0], ((double)uncomp_size[0])/comp_size,
            ares_data.comp_time
        );
        Singleton<CompressionMetricsManager>::GetInstance()->AddDecompressionMetrics(
            type, lib_id,
            uncomp_size[0],
            ares_data.decomp_time
        );

        ares_data.resetTimers();
        cout << endl;
    }

    Singleton<CompressionMetricsManager>::GetInstance()->CommitMetrics(output_file);
}
