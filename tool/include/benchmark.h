/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Luke Logan
 * <llogan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of HCompress
 * 
 * HCompress is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef _BENCHMARK_H_
#define _BENCHMARK_H_


using namespace std;

#include <stdio.h>
#include <iostream>
#include <list>
#include <iterator>

struct Dataset;
typedef string PathArg;
typedef list<Dataset> GenFileArg;

struct Dataset
{
	const long int types[8] = {
		0,	//Invalid type
		H5T_NATIVE_CHAR, 
		H5T_NATIVE_INT32, H5T_NATIVE_INT32, 
		H5T_NATIVE_UINT32, H5T_NATIVE_UINT32, 
		H5T_NATIVE_FLOAT, H5T_NATIVE_DOUBLE
	};
	const size_t type_sizes[8] = {
		0,
		1, 
		4, 4, 
		4, 4,
		sizeof(float), sizeof(double)
	};
	
	int type_index;		//The index within the type vector
	uint64_t type;		//The H5 data type
	size_t type_size;	//The size of the H5 data type
	size_t count;		//The number of data units to generate
	size_t size;		//The size of the dataset
	
	Dataset(int type_index, uint64_t count)
	{
		this->type_index = type_index;
		type = types[type_index];
		type_size = type_sizes[type_index];
		this->count = count;
		size = count * type_size;
	}
};

class BenchmarkArgs
{
	int argc = 0;
	char **argv = nullptr;
	
	public:
	
	list<PathArg> fargs;
	list<GenFileArg> gargs;
	string output_file = "output.json";
	string intermed_dir = "";
	DATASET_TYPE probability_type;
	double probability_args[2];

	/*CUNSTROCTORS*/
	
	BenchmarkArgs(int argc, char **argv)
	{
		this->argc = argc;
		this->argv = argv;
		this->probability_type = NORMAL_DISTRIBUTION;
		this->probability_args[0] = 100;
		this->probability_args[1] = 4;
		parse();
	}
	
	
	private:
	
	/*PARSING COMMAND LINE ARGUMENTS*/
	
	void parse(void)
	{
		for(int i = 1; i < argc; i++)
		{
			//----Help flag
			if(strcmp(argv[i], "-h") == 0)
			{
				help_arg();
				continue;
			}
			
			//----Set intermediate directory
			else if(strcmp(argv[i], "-d") == 0)
			{
				dir_arg(i);
				continue;
			}
			
			//----Benchmark existing file
			else if(strcmp(argv[i], "-f") == 0)
			{
				file_arg(i);
				continue;
			}
			
			//----Generate file to benchmark
			else if(strcmp(argv[i], "-g") == 0)
			{
				gen_file_arg(i);
				continue;
			}
			
			//----Set benchmark output file
			else if(strcmp(argv[i], "-o") == 0)
			{
				out_arg(i);
				continue;
			}

			//----Set probability distribution for generator
			else if(strcmp(argv[i], "-p") == 0)
            {
			    probability_arg(i);
			    continue;
            }

			//----Invalid
			else
			{
				invalid_arg(i);
				continue;
			}
		}
	}
	
	
	/*INTERPRETING COMMAND LINE ARGUMENTS*/
	
	//-h
	void help_arg(void)
	{
		cout << "-h: Print out help for this program.\n";
		cout << "-d \"/path/to/dir/\": Intermediate directory for storing data\n";
		cout << "-f \"path/to/file.h5\": Input an HDF5 file to benchmark\n";
		cout << "-g count type1 type2 ...: Generate a file with datasets type1, type2, ... each with count elements\n";
		cout << "-o \"/path/to/output.json\": Set benchmarking data output file\n";
		cout << "-p distribution param1 param2 ...: Set file generator probability distribution.\n\n";
		cout << "TYPES:\n";
		cout << "   1: Generate dataset of characters\n";
		cout << "   2: Generate dataset of 32-bit signed integers\n";
		cout << "   3: Generate dataset of sorted 32-bit signed integers\n";
		cout << "   4: Generate dataset of 32-bit unsigned integers\n";
		cout << "   5: Generate dataset of sorted 32-bit unsigned integers\n";
		cout << "   6: Generate dataset of floats\n";
		cout << "   7: Generate dataset of doubles\n";
	}
	
	//-d <path>
	void dir_arg(int &i)
	{
		i++;
		if(i >= argc)
		{
			cout << "Error: no path was inputted into -d.\n";
			return;
		}
		
		intermed_dir = argv[i];
	}
		
	//-f <path>
	void file_arg(int &i)
	{
		i++;
		if(i >= argc)
		{
			cout << "Error: no path was inputted into -f.\n";
			return;
		}
		
		fargs.push_back(argv[i]);
	}
	
	//-g count type1 type2 ...
	void gen_file_arg(int &i)
	{
		i++;
		GenFileArg gfa;
		uint64_t count = 0;
		
		//Get number of data elements to generate for the datasets
		if(i < argc)
		{
			istringstream iss(argv[i]);
			iss >> count;
			if(iss.fail())
			{
				cout << "Error: Invalid dataset member count: " << count << " is not an integer.\n";
				return;
			}
		}
		else
		{
			cout << "Error: The number of elements to generate was not inputted into -g.\n";
			return;
		}
		
		//Make sure the type vector exists
		i++;
		if(i >= argc)
		{
			cout << "Error: No types were inputted into -g.\n";
			return;
		}
		
		//Convert type vector into an array
		int type_index;
		while(i < argc && argv[i][0] != '-')
		{
			istringstream iss(argv[i]);
			iss >> type_index;
			if(iss.fail())
			{
				cout << "Error: Data type " << argv[i] << " is not an integer. Use -h to see supported types.\n";
				return;
			}
			if(type_index < 1 || type_index > 7)
			{
				cout << "Warning: Data type " << argv[i] << " is not one of the 7 available types. Use -h to see supported types.\n";
				i++;
				continue;
			}
			gfa.push_back(Dataset(type_index, count));
			i++;
		}
		
		gargs.push_back(gfa);
		i--;
	}
	
	//-o <path>
	void out_arg(int &i)
	{
		i++;
		if(i >= argc)
		{
			cout << "Error: no path was inputted into -d.\n";
			return;
		}
		
		output_file = argv[i];
	}

    //-p distribution param1 ...
    void probability_arg(int &i)
    {
        probability_type= static_cast<DATASET_TYPE>(atoi(argv[++i]));
        switch(probability_type)
        {
            case EXPONENTIAL_DISTRIBUTION:
                probability_args[0] = atof(argv[++i]);
                probability_args[1] = 0;
                break;
            case NORMAL_DISTRIBUTION:
            case GAMMA_DISTRIBUTION:
            case UNIFORM_DISTRIBUTION:
                probability_args[0] = atof(argv[++i]);
                probability_args[1] = atof(argv[++i]);
                break;
        }
    }

	//invalid argument
	void invalid_arg(int i)
	{
		cout << "Invalid argument: " << argv[i] << "\n";
	}
	
};

//Prototypes
string hdf5_gen(BenchmarkArgs args, GenFileArg datasets, string intermed_dir, uint64_t id);
void *generate_data(BenchmarkArgs args, Dataset dataset);
int get_dataset_type(hid_t type);
void benchmark_h5(hid_t group_id, string output_file);
void benchmark(int64_t type, size_t ds_size, char *dataset, string output_file);

#endif
